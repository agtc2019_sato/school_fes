
/* :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
/*																	*/
/*		学園祭ゲーム												*/
/*							ゲームタイトル							*/
/*																	*/
/*							Lteam    :	佐藤 松田 山崎				*/
/*							作成期間 :	2020/09/16	〜	2020/10/23	*/
/* :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */

#include "Common.h"

int WINAPI WinMain(HINSTANCE hI,HINSTANCE hP,LPSTR lpC,int nC)
{
int  LightHandle ;
int  secLightHand ;

	// --- ウインドウモードで起動するか確認する
	if ( MessageBox( NULL, "ウインドウモードで起動しますか？", "画面モード確認", MB_YESNO ) == IDYES ) {
		// ---「はい」が選択された場合はウインドウモードで起動
		ChangeWindowMode( TRUE ) ;
	} else {
		// ---「いいえ」が選択された場合はスクリーンモードで起動
		ChangeWindowMode( FALSE ) ;
	}
	// --- ウインドウサイズの変更
	SetGraphMode( WINDOW_W, WINDOW_H, 32 ) ;


	// --- DXライブラリの初期化
	if ( DxLib_Init() == -1 ) {
		return -1 ;
	}


// --- 元のライト設定を消す
SetLightEnable( FALSE ) ;
// --- ディレクショナルライト設定
LightHandle = CreateDirLightHandle( VGet( 0.0f, -1.0f, 0.0f ) ) ;	// --- 上から下
secLightHand = CreateDirLightHandle( VGet( 0.0f, 0.0f, 1.0f ) ) ;	// --- 手前から奥


	// --- モデルなどの初期セット
	if ( InitSet() == -1 ){
		return -1 ;
	}


	// --- 裏画面
	SetDrawScreen(DX_SCREEN_BACK) ;
	// --- カメラの設定 --------------------
	// --- カメラの座標,注視点,上情報
	SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;
	// --- カメラの視野角
	SetupCamera_Perspective( 50.0f * g_Pi / 180.0f ) ;
	// --- Near, Far プレーンの設定	前回は3000.0f
	SetCameraNearFar( 1.0f, 14000.0f ) ;
	/* ---------------------------------------------------------------- */
	/*																	*/
	/*							メインループ							*/
	/*																	*/
	/* ---------------------------------------------------------------- */
	while(ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0)
	{
		// --- シーンループ
		SceneLoop() ;

		// --- 画面の消去
		ClearDrawScreen() ;
		// --- 描画関数
		if ( DrawLoop() != 0 )
			return -1 ;

		// --- 裏画面の内容を表画面に表示
		ScreenFlip() ;

	}

	DxLib_End() ;


	// コメント
	return 0 ;
}


