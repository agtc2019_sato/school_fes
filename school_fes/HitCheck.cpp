
/* ==================================================================================

		ヒットチェック

+ -----------ファイルの概要----------------------------------------------------------

		ヒットチェック処理まとめ

+ =================================================================================== */

#include "Common.h"

/* -----------------------------------------------------------------
|
|			ヒットチェック
|			: オブジェクト → 1,2プレイヤー
|
+----------------------------------------------------------------- */
int OtoPHitCheck(int arg_PlayerNo)
{
	int return_no = 0 ;
	u_char plno = 0 ;


	if ( arg_PlayerNo == e_player1 ) {
		plno = P1 ;
	} else if ( arg_PlayerNo == e_player2 ) {
		plno = P2 ;
	}

	// --- 弾の数分のヒットチェック
	for ( int Bcnt = 0 ; Bcnt < 5 ; Bcnt++ ) {
		if ( g_Dispflg[e_bullet + Bcnt] ) {
			if ( HitCheck_Capsule_Capsule( g_Modelpos[arg_PlayerNo], g_Modelpos[arg_PlayerNo], (PLAYER_H / 2),
											g_Modelpos[e_bullet + Bcnt], g_Modelpos[e_bullet + Bcnt], (100.0f / 2) )
											== TRUE ) {
				// --- ダメージサウンド再生
				PlaySoundMem( g_soundDT[e_Sdamage],  DX_PLAYTYPE_BACK, TRUE ) ;

				go_Effect.EffectSelect( arg_PlayerNo ) ;	// --- ヒットプレイヤーを取得
				g_Dispflg[arg_PlayerNo] = 1 ;				// --- 強制的に表示
				// --- スタンエフェクト非表示
				if ( plno == P1 ) {
					g_Bmpimage[e_stanef].bmp_dispflg = 0 ;
					go_Effect.m_stanflg &= 0xfe ;		// --- フラグ消し
				} else if ( plno == P2 ) {
					g_Bmpimage[e_stanef + 1].bmp_dispflg = 0 ;
					go_Effect.m_stanflg &= 0xfd ;		// --- フラグ消し
				}
				return_no = 1 ;
				// --- 星獲得時一回は無敵
				if ( go_Coin.m_playercoin[plno] ) {
					go_Coin.m_invincible[plno] = 1 ;
					return_no = 0 ;
				}
			}
		}
	}


	return return_no ;
}



/* -----------------------------------------------------------------
|
|			ヒットチェック　　プレイヤー同士は貫通しない
|			: プレイヤー → プレイヤー
|
+----------------------------------------------------------------- */
int P1toP2HitCheck()
{
	int return_no = 0 ;
	
	if ( HitCheck_Capsule_Capsule(VAdd(go_1Player.m_pos, go_1Player.m_move), VAdd(go_1Player.m_pos, go_1Player.m_move), (PLAYER_H / 2),
									VAdd(go_2Player.m_pos, go_2Player.m_move), VAdd(go_2Player.m_pos, go_2Player.m_move), (PLAYER_H / 2))
									== TRUE ) {
		return_no = 1 ;		// --- 当たった
	}

	return return_no ;

}



/* -----------------------------------------------------------------
|
|			ヒットチェック　アタック時
|			: プレイヤー → プレイヤー
|
+----------------------------------------------------------------- */
int AP1toAP2HitCheck()
{
	int return_no = 0 ;
	
	// --- フラグが立っていなければ
	if ( go_Coin.m_hitcheck == 0 ) {
		if ( HitCheck_Capsule_Capsule(g_Modelpos[e_player1], g_Modelpos[e_player1], 70,
									  g_Modelpos[e_player2], g_Modelpos[e_player2], 70) == TRUE ) {
			return_no = 1 ;		// --- 当たった
		}
	}

	return return_no ;

}



/* -----------------------------------------------------------------
|
|			ヒットチェック
|			: パネル → プレイヤー
|
+----------------------------------------------------------------- */
int PtoPHitCheck()
{
	int return_no = 0 ;

	for ( int pcnt = 0 ; pcnt < 2 ; pcnt++ ) {
		if ( HitCheck_Capsule_Capsule(g_Modelpos[e_panel], g_Modelpos[e_panel], (PLAYER_H / 2),
									  g_Modelpos[e_player1 + pcnt], g_Modelpos[e_player1 + pcnt],
									  (PLAYER_H / 2)) == TRUE ) {
			// --- パネルサウンド再生
			ChangeVolumeSoundMem( 155 , g_soundDT[e_Sslow] ) ;
			PlaySoundMem( g_soundDT[e_Sslow],  DX_PLAYTYPE_BACK, TRUE ) ;

			return_no = e_player1 + pcnt ;		// --- 当たった
		}
	}


	return return_no ;

}



/* -----------------------------------------------------------------
|
|			ヒットチェック
|			: プレイヤー  → コイン
|
+----------------------------------------------------------------- */
int CtoPHitCheck()
{
	int return_no = 0 ;

	for ( int pcnt = 0 ; pcnt < 2 ; pcnt++ ) {
		if ( HitCheck_Capsule_Capsule(g_Modelpos[e_coin], g_Modelpos[e_coin], (PLAYER_H / 2),
									  g_Modelpos[e_player1 + pcnt], g_Modelpos[e_player1 + pcnt], (PLAYER_H / 2))
									  == TRUE ) {
			return_no = e_player1 + pcnt ;		// --- 当たった
		}
	}

	return return_no ;

}



/* -----------------------------------------------------------------
|
|			ヒットチェック
|			: 爆発 → プレイヤー
|
+----------------------------------------------------------------- */
int BtoPHitCheck(int arg_PlayerNo)
{
	int return_no = 0 ;
	u_char plno = 0 ;


	if ( arg_PlayerNo == e_player1 ) {
		plno = P1 ;
	} else if ( arg_PlayerNo == e_player2 ) {
		plno = P2 ;
	}

	// --- 表示されている時だけ当たる
	if ( g_Dispflg[e_explo] ) {
		if ( HitCheck_Capsule_Capsule(g_Modelpos[e_explo], g_Modelpos[e_explo], (PLAYER_H / 2),
										g_Modelpos[arg_PlayerNo], g_Modelpos[arg_PlayerNo], (PLAYER_H / 2))
										== TRUE ) {
			return_no = 1 ;					// --- 当たった
			g_Dispflg[arg_PlayerNo] = 1 ;	// --- 強制的に表示
			// --- スタンエフェクト非表示
			if ( plno == P1 ) {
				g_Bmpimage[e_stanef].bmp_dispflg = 0 ;
				go_Effect.m_stanflg &= 0xfe ;		// --- フラグ消し
			} else if ( plno == P2 ) {
				g_Bmpimage[e_stanef + 1].bmp_dispflg = 0 ;
				go_Effect.m_stanflg &= 0xfd ;		// --- フラグ消し
			}
			// --- 星獲得時一回は無敵
			if ( go_Coin.m_playercoin[plno] ) {
				go_Coin.m_invincible[plno] = 1 ;
				return_no = 0 ;
			}
		}
	}

	return return_no ;
}



/* -----------------------------------------------------------------
|
|			ヒットチェック
|			: プレイヤー → 床
|
+----------------------------------------------------------------- */
int PlayerSquare(int arg_PlayerNo) {
	int return_no = 0 ;

	if ( (HitCheck_Capsule_Triangle(g_Modelpos[arg_PlayerNo], g_Modelpos[arg_PlayerNo], (PLAYER_H / 2),
							  VGet(-STG_SQR_X, 0.0f, 400.0f), VGet(STG_SQR_X, 0.0f, 400.0f), VGet(STG_SQR_X, 0.0f, -STG_SQR_Z)) == TRUE)
		 ||
		 (HitCheck_Capsule_Triangle(g_Modelpos[arg_PlayerNo], g_Modelpos[arg_PlayerNo], (PLAYER_H / 2),
							  VGet(-STG_SQR_X, 0.0f, 400.0f), VGet(STG_SQR_X, 0.0f, -STG_SQR_Z), VGet(-STG_SQR_X, 0.0f, -STG_SQR_Z)) == TRUE) ) {
		return_no = 1 ;
	}

	return return_no ;
}





