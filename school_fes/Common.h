
/* ==================================================================================

		すべてのファイルで共通のヘッダーファイル

+ -----------ファイルの概要----------------------------------------------------------
		ヘッダー・define・enum・構造体・プロトタイプ宣言・extern

+ =================================================================================== */


/* -----------------------------------------------------------------
|
|			define宣言
|			: #define
|
+----------------------------------------------------------------- */
// ==ウインドウサイズ== //
#define		WINDOW_W	1920	// --- ウインドウ幅1920
#define		WINDOW_H	1080	// --- ウインドウ高1080

// ==タイマー== //
#define		MAXTIME		45		// --- １ステージでの時間

// == ステージ == //
#define		STG_SQR_X	600.0f	// --- ステージ１のX値
#define		STG_SQR_Z	500.0f	// --- ステージ１のZ値

// ==ポジション== //
#define		STAGE2POS_Y	-50.0f	// --- ステージ２のポジションＹ
#define		DEFANGLE	180		// --- 初期角度

#define		CAMERAPOS_Y	700.0f		// --- カメラポジションY
#define		CAMERAPOS_Z	-1000.0f	// --- カメラポジションZ

// ==プレイヤー== //
#define		PLAYER_W	100		// --- プレイヤー幅
#define		PLAYER_H	100		// --- プレイヤー高
#define		HITCNT		100		// --- ヒットチェックの間
#define		P1			0		// --- プレイヤー１
#define		P2			1		// --- プレイヤー２
#define		BLOW_CNT	5.0f	// --- 吹き飛びスピード
#define		PL_SPD		10.0f	// --- プレイヤーの移動スピード
#define		PL_SPD_DI	7.0f	// --- プレイヤーの移動スピード　斜め
#define		P_PL_SPD	5.0f	// --- 足遅パネルの移動スピード
#define		P_PL_SPD_DI	3.0f	// --- 足遅パネルの移動スピード　斜め
#define		P_ATTIME	40.0f	// --- アタック時のアニメーションタイム
#define		STG2_RADIUS	700.0f	// --- ステージ２のレコード半径

#define		PL_STG2_Y	650.0f	// --- ステージ２スタート初期位置
#define		PL_STG2_SPD	6.0f	// --- ステージ移動のスピード


// ==アイテム== //
#define		P_EFFTIME	150		// --- 足遅の効果時間 

// ==大砲・弾== //
#define		FIRST_POSI_X 700	// --- 大砲の初期位置X
#define		FIRST_POSI_Y 600	// --- 大砲の初期位置Y
#define		CANNON_MAX	 5		// --- 大砲の最大数
#define		BULLET_MAX	 5		// --- 弾の最大数
#define		BULLET_SPD	 30		// --- 弾のスピード

// ==リザルト== //
#define		RESULT_ROT	 3		// --- リザルト時回転数


/* -----------------------------------------------------------------
|
|			型宣言
|			: enum
|
+----------------------------------------------------------------- */

// --- プレイヤーのアニメーション
enum Ply_ActionMode
{
	ep_Wait,		// --- 0 待ち
	ep_Walk,		// --- 1 歩き
	ep_Down,		// --- 3 ダウン状態
	ep_Stan,		// --- 4 スタン状態
	ep_Attack,		// --- 2 攻撃
	ep_Win,			// --- 5 勝利
	ep_Lose,		// --- 6 敗北
	ep_end			// --- アニメーションの最後
} ;

// --- モデルの描画順番
enum Disp_Model
{
	e_recode,		// ---  0 レコード
	e_stage,		// ---  1 ステージ1
	e_stage2 ,		// ---  2 ステージ2
	e_table ,		// ---  3 部屋の机
	e_panel,		// ---  4 デバフパネル
	e_star,			// ---  5 スター
	e_coin,			// ---  6 コイン
	e_player1,		// ---  7 ロボット
	e_player2,		// ---  8 テレビ
	e_enemy ,		// ---  9 パワプロくん	2
	e_bomb   = e_enemy + 2,		// ---  11 爆弾			2
	e_cannon = e_bomb + 2,		// --- 13 大砲			10
	e_bullet = e_cannon + 10,	// --- 23 大砲の弾		10
	e_explo  = e_bullet + 10,	// --- 33 爆弾の爆発	2
	e_end    = e_explo + 2		// --- モデルの最後
} ;

// --- キャラの移動方向
enum Direction
{
	e_Down,			// --- 1 下
	e_Left,			// --- 2 左 
	e_Up,			// --- 3 上
	e_Right,		// --- 4 右
} ;

// --- シーンループ
enum SceneLoop{
	e_SceneBlank,		// ---  0 ブランク
	e_title,			// ---  1 タイトル
	e_SelectInit,		// ---  2 キャラセレクト初期セット
	e_Select,			// ---  3 キャラセレクト画面
	e_Explanation,		// ---  4 操作説明
	e_Stage1Init,		// ---  5 ステージ1初期セット
e_Stage1Ready,
	e_Stage1_Square,	// ---  6 ステージ1 四角
	e_Stage2Init,		// ---  7 ステージ1初期セット
	e_Stage2_Circle,	// ---  8 ステージ2 丸
e_Stage2Fine,
	e_ResultInit,		// ---  9 リザルト初期セット
	e_Result			// --- 10 リザルト中
} ;

// --- プレイヤーアクション
enum PlayerActionName
{
	ep_PlayerBlank,		// --- 初期化
	ep_PlayerWait,		// --- 待機
	ep_PlayerWalk,		// --- 歩き
	ep_PlayerAttack,	// --- 攻撃
	ep_PlayerStan,		// --- スタン
	ep_PlayerDown,		// --- やられ
	ep_PlayerStageDown,	// --- ステージ外
	ep_Player
} ;

// --- エネミーのアクション
enum EnemyActionName{
	ee_Blank,		// --- 0: ブランク
	ee_Wait,		// --- 1: 待機
	ee_Attack,		// --- 2: 攻撃
	ee_End			// --- 3: アクション最後
} ;

// --- ボムのアクション
enum BombAnimAction {
	e_bomwait,		// --- ボム待機
	e_bomanim,		// --- ボム上り中
	e_bomanimend,	// --- ボム上り終了
	e_bomfall,		// --- ボム落ち中
	e_bomexplo,		// --- ボム着地、爆発
	e_bomexploend	// --- ボム爆発終了
} ;

// --- 画像セット
enum BmpImageName {
	e_backGB ,			// --- 0:背景画像
	e_titlogo,			// --- タイトルに描画するロゴ
	e_titstr,			// --- タイトルに描画する文字
	e_selectGB ,		// --- セレクト背景画像
	e_screenGB ,		// --- 1:切替画像
	e_hitef ,			// --- 2:弾のヒット(1P2P)
	e_attackef = e_hitef + 2 ,				// --- 4:攻撃ヒット(1P2P)
	e_stanef   = e_attackef + 2 ,			// --- 6:スタン(1P2P)
	e_cloudef  = e_stanef + CANNON_MAX ,	// --- 大砲爆発時
	e_Eend = e_cloudef + CANNON_MAX,		// --- エフェクト画像最大数
	e_PSele1,			// --- プレイヤー1セレクト用画像
	e_PSele2,			// --- プレイヤー2セレクト用画像
	e_PSele3,			// --- プレイヤー3セレクト用画像
	e_PSele4,			// --- プレイヤー4セレクト用画像
	e_waku1,			// --- セレクト画像１	パネルエフェクト
	e_waku2,			// --- セレクト画像２	パネルエフェクト
	e_kettei,			// --- 決定				レディゴー
	e_waku3,			// --- セレクト画像３
	e_waku4,			// --- セレクト画像４	フィニッシュ
	e_1PWin  ,			// --- 1PWIN画像
	e_2PWin  ,			// --- 2PWIN画像
	e_Lose ,			// --- LOSE画像
	e_PBanner1,			// --- プレイヤー１のバナー
	e_PBanner2,			// --- プレイヤー２のバナー
	e_TBanner,			// --- タイマーのバナー
	e_1P,				// --- １Ｐの表示
	e_2P,				// --- ２Ｐの表示
	e_Nextst,			// --- 次のステージ移動
	e_bomshadow,		// --- ボムの影
	e_BmpEnd			// --- 画像最大数
} ;

// --- リザルト
enum ResultActionName {
	e_ResultBlank ,			// --- 0:アニメーション準備
	e_RtPlyAnim ,			// --- 1:レコード回転+プレイヤー待機
	e_RtWinLosInit ,		// --- 2:勝敗アニメーションセット
	e_RtWinLos ,			// --- 3:勝敗アニメーションプレイ
	e_ResultEnd				// --- 4:終了
} ;

// --- サウンド
enum SoundName {
	e_Btitle  ,			// --- :タイトルBGM
	e_Bselect ,			// --- :セレクトBGM
	e_Bfirst  ,			// --- :第一ステージBGM
	e_Skettei ,			// --- :決定音SE
	e_Scancel ,			// --- :キャンセル音
	e_Sleftright ,		// --- :左右
	e_Ssbutton ,		// --- :スタートボタン
	e_Sready ,			// --- :スタート音1
	e_Sgo ,				// --- :スタート音2
	e_Sattack ,			// --- :攻撃SE
	e_Sdamage ,			// --- :攻撃ヒットSE
	e_Scannon ,			// --- :大砲音
	e_Sbakuha = e_Scannon + CANNON_MAX ,			// --- :爆発
	e_Sslow	= e_Sbakuha + 2, // --- :パネル 遅くなる
	e_Scoin	  ,			// --- :コイン
	e_Swaring ,			// --- :警告音
	e_Bsecound  ,		// --- :第二ステージBGM
	e_Sresult ,			// --- :結果
	e_Scheers = e_Sresult + 2 ,			// --- :勝利歓声
	e_Schange ,			// --- :ステージ変換
	e_Sfinish ,			// --- :終わりの合図
	e_SoundEnd ,		// --- :終了
} ;

/* -----------------------------------------------------------------
|
|			共通ヘッダーファイル
|			: #include
|
+----------------------------------------------------------------- */
#include <DxLib.h>
#include <math.h>				// --- 計算
#include <time.h>				// --- タイム
#include "ConsoleWindow.h"		// --- コンソールウインドウ
#include "BasicObj.h"			// --- 基底クラス
#include "CharacterSelect.h"	// --- タイトルやセレクト
#include "StageAction.h"		// --- ステージ
#include "Player1.h"			// --- プレイヤー１
#include "Player2.h"			// --- プレイヤー２
#include "CannonAction.h"		// --- 大砲
#include "EnemyAction.h"		// --- エネミー
#include "ItemCoinAction.h"		// --- アイテム・コイン
#include "ItemPanelAction.h"	// --- アイテム・パネル
#include "HitCheck.h"			// --- ヒットチェック
#include "EffectAction.h"		// --- エフェクト
#include "ResultAction.h"		// --- リザルト


/* -----------------------------------------------------------------
|
|			構造体
|			: typedef struct
|
+----------------------------------------------------------------- */
// --- 画像描画に必要な要素
typedef struct {
	VECTOR	bmp_pos ;		// --- 表示３Ｄポジション
	float	bmp_xy[2] ;		// --- 画像中心点	0:X 1:Y
	int		bmp_exp ;		// --- サイズ拡大値
	float	bmp_angle ;		// --- 回転値(ラジアン)
	int		bmp_handle ;	// --- 画像ハンドル
	bool	bmp_trans ;		// --- 透過有無
	bool	bmp_dispflg ;	// --- 表示有無
} BMPDT ;

/* -----------------------------------------------------------------
|
|			プロトタイプ宣言
|
+----------------------------------------------------------------- */
int		InitSet( ) ;			// --- 初期セット
int		DrawLoop() ;			// --- 描画ループ
void	SceneLoop() ;			// --- シーンループ
void	ModelInit() ;			// --- モデルの初期セット

void CharSelInit() ;
int CharSel() ;

/* -----------------------------------------------------------------
|
|			外部参照宣言
|			: extern
|
+----------------------------------------------------------------- */
// --- 変数 :g_
extern ConsoleWindow g_cWin ;				// --- コンソールウィンドウ

extern int			g_SceneNo ;				// --- シーンループの番号
extern int			g_Timercnt ;			// --- タイマーカウント
extern SYSTEMTIME	g_LocalTime[] ;			// --- ローカルの時間

extern BMPDT		g_Bmpimage[] ;			// --- 画像系

extern VECTOR		g_Camerapos ;			// --- カメラポジション
extern VECTOR		g_Cameratgt ;			// --- カメラ注視点

extern VECTOR		g_Modelpos[] ;				// --- モデルポジション
extern int			g_Loadmodel[] ;				// --- モデルの読み込み
extern int			g_Animmodel[2][ep_end] ;	// --- アニメーションの読み込み
extern BOOL			g_Dispflg[] ;				// --- 表示フラグ
extern float		g_Pi ;						// --- 円周率

extern int			g_PScoer[] ;				// --- プレイヤーのスコアとコイン保持

extern int			g_stflg ;					// --- ステージフラグ
extern BOOL			g_timeflg ;
extern int			stanimflg ;					// --- ステージアニメフラグ
extern float		g_titsize ;					// --- タイトル大きさ変更用
extern int			g_pmflg ;					// --- タイトル大きさ変更用
extern int			g_titcnt ;					// --- タイトル点滅用
extern int			g_nxtcnt ;					// --- タイトル点滅用

extern int			g_soundDT[] ;				// --- サウンドデータ
extern	bool		g_readygoflg ;				// --- ゴー画像


// --- クラス :go_
extern TitleCharaSet	go_TitChaSet ;		// --- タイトルとキャラセレクト
extern Player1Action	go_1Player ;		// --- プレイヤー１クラス
extern Player2Action	go_2Player ;		// --- プレイヤー２クラス
extern CannonAction		go_Cannon ;			// --- キャノンクラス
extern StageAction		go_Stage ;			// --- ステージクラス
extern ItemCoinAction	go_Coin ;			// --- アイテム・コイン
extern ItemPanelAction	go_Panel ;			// --- アイテム・パネル
extern EnemyAction		go_Enemy ;			// --- エネミークラス
extern EffectAction		go_Effect ;			// --- エフェクトクラス
extern ResultAction		go_Result ;			// --- リザルトクラス


