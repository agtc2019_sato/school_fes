
/* ==================================================================================

		エネミーのアクション

+ -----------ファイルの概要----------------------------------------------------------
		エネミーのクラス

+ =================================================================================== */

class EnemyAction : BasicObj
{
	public :
		EnemyAction() ;
		~EnemyAction() ;

		void InitSet() override ;		// --- 初期セット
		void Action() override ;		// --- アクション
		int  EneAnimInit() ;			// --- アニメーションセット
		void EneAnimNatural() ;			// --- ナチュラルアニメーション
		void EneAnimAction () ;			// --- アクション
		void BombAction() ;				// --- ボムのアクション
		void RandomPos() ;				// --- ボムのランダム落下座標

		// --- エネミー用変数
		int		m_AnimModel[ee_End] ;		// --- アニメーション読み込み
		int		m_rootflm ;					// --- ルートフレーム入れ
		int		m_Attachno[ee_End] ;		// --- エネミーアニメを関連付ける		
		float	m_totalanimtime[ee_End] ;	// --- トータル時間
		float	m_playtime ;				// --- 進行中の時間
		int		m_animno ;					// --- エネミーアニメション格納
		int		m_animtim ;					// --- タイマー変数

		// --- ボム用変数
		int		m_BActionNo ;			// --- ボムのアクションナンバー

		VECTOR	m_bompos ;				// --- ボムのポジション
		VECTOR	m_bommove ;				// --- ボムの落下移動量
		int		m_BombAnim ;			// --- ボム用アニメーション
		int		m_Brootflm ;			// --- ボムのフレーム
		int		m_Battach ;				// --- ボムアニメの関連付け
		float	m_Bplaytime ;			// --- 進行中の時間
		float	m_Btotalanimtime ;		// --- ボムアニメの総時間
		BOOL	m_Bfallflg ;			// --- ボム落下中フラグ

		// --- 爆発用変数
		VECTOR	m_explopos ;			// --- 爆発のポジション
		int		m_ExploAnim ;			// --- 爆発用アニメーション
		int		m_Erootflm ;			// --- 爆発のフレーム
		int		m_Eattach ;				// --- 爆発アニメの関連付け
		float	m_Eplaytime ;			// --- 爆発進行中の時間
		float	m_Etotalanimtime ;		// --- 爆発アニメの総時間


} ;



