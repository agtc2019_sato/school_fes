
/* ==================================================================================

		エネミーのアクション

+ -----------ファイルの概要----------------------------------------------------------
		エネミーのクラスの実装部

+ =================================================================================== */

#include "Common.h"

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
EnemyAction::EnemyAction() {

}

/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
EnemyAction::~EnemyAction() {

}

/* ------------------------------------ */
/*										*/
/*			  初期セット				*/
/*										*/
/* ------------------------------------ */
void EnemyAction::InitSet()
{
	// --- 座標セット
	m_pos		= VGet( -600.0f,  0.0f, 600.0f ) ;
	m_bompos	= VGet( -575.0f, 50.0f, 575.0f ) ;
	m_bommove	= VGet(    0.0f, -10.0f,   0.0f ) ;
	m_explopos  = VGet(    0.0f, 1000.0f,   0.0f ) ;

	m_ActionNo		= ee_Blank ;	// --- アクションナンバー初期化
	m_BActionNo		= e_bomwait ;		// --- アクションナンバー初期化
	m_playtime		= 0.0f ;		// --- アニメーションプレイ時間
	m_Bplaytime		= 0.0f ;		// --- アニメーションプレイ時間
	m_Eplaytime		= 0.0f ;		// --- アニメーションプレイ時間
	m_animtim		= 0 ;			// --- タイマー初期化
	m_Bfallflg		= 0 ;			// --- ボム落下中フラグ

	g_Dispflg[e_bomb]	= 0 ;		// --- ボムの表示フラグ
	g_Dispflg[e_explo]	= 0 ;		// --- 爆発の表示フラグ

	// --- モデルの回転
	MV1SetRotationXYZ( g_Loadmodel[e_enemy]  , VGet(0.0f , 1.0f , 0.0f) ) ;
}

/* ------------------------------------ */
/*										*/
/*			  アクション				*/
/*										*/
/* ------------------------------------ */
void EnemyAction::Action()
{
	switch ( m_ActionNo )
	{
		// --- 初期セット
		case ee_Blank :
			m_animtim++ ;				// --- タイマー進行	
			m_playtime	= 0.0f ;		// --- エネミーアニメーション経過時間初期化
			m_animno	= ee_Wait ;		// --- アニメ番号入れ
			m_animtim	= 0 ;			// --- タイマー初期化

			// --- 待機アニメーション入れ
			m_Attachno[ee_Wait]	= MV1AttachAnim( g_Loadmodel[e_enemy], 0, m_AnimModel[ee_Wait] ) ;
			m_ActionNo = ee_Wait ;

			break ;


		// --- 待機
		case ee_Wait :
			m_animtim++ ;				// --- タイマー進行
			m_animno	= ee_Wait ;		// --- アニメ番号入れ
			m_playtime	+= 1.0f ;		// --- アニメーション進行

			// --- アニメーションループ
			if ( m_playtime > m_totalanimtime[ee_Wait] )
				m_playtime = 0.0f ;

			// --- ボムアクション
			BombAction() ;

			// --- エネミー攻撃へ
			if ( (m_animtim > 250)  && (g_Dispflg[e_bomb]  == 0) 
									&& (g_Dispflg[e_explo] == 0) ){
				m_animtim = 0 ;				// --- タイマーリセット
				MV1DetachAnim( g_Loadmodel[e_enemy], m_Attachno[ee_Wait] ) ;	// アタッチを外す
				// ---攻撃アニメーション入れ
				m_Attachno[ee_Attack]	= MV1AttachAnim( g_Loadmodel[e_enemy], 0, m_AnimModel[ee_Attack] ) ;

				m_ActionNo  = ee_Attack ;		// --- アタックへ
				m_BActionNo = e_bomanim ;		// --- ボムのぼりへ
			}

			break ;


		// --- 攻撃
		case ee_Attack :
			m_animno = ee_Attack ;			// --- アニメ番号入れ
			m_playtime += 0.5f ;			// --- アニメーション進行
			BombAction() ;

			if ( m_playtime > m_totalanimtime[ee_Attack] ){
				m_ActionNo = ee_Blank ;		// --- ブランクへ
				MV1DetachAnim( g_Loadmodel[e_enemy], m_Attachno[ee_Blank] ) ;	// --- アタッチを外す
			}
			break ;

	}

	// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	MV1SetAttachAnimTime( g_Loadmodel[e_enemy], m_Attachno[m_animno], m_playtime ) ;
	MV1SetAttachAnimTime( g_Loadmodel[e_bomb], m_Battach, m_Bplaytime ) ;
	MV1SetAttachAnimTime( g_Loadmodel[e_explo], m_Eattach, m_Eplaytime) ;

	// --- ポジションセット
	g_Modelpos[e_enemy]	= m_pos ;
	g_Modelpos[e_bomb]	= m_bompos ;
	g_Modelpos[e_explo]	= m_explopos ;

}

/* ------------------------------------ */
/*										*/
/*		アニメーションセット			*/
/*										*/
/* ------------------------------------ */
int EnemyAction::EneAnimInit()
{
	m_rootflm	= MV1SearchFrame( g_Loadmodel[e_enemy], "root" ) ;
	m_Brootflm	= MV1SearchFrame( g_Loadmodel[e_bomb], "root" ) ;

	// --- アニメーションの読み込み
	m_AnimModel[ee_Wait]	= MV1LoadModel( "Models/Enemy/BomeneA_natural.mv1" ) ;
	m_AnimModel[ee_Attack]	= MV1LoadModel( "Models/Enemy/BomeneA_action.mv1" ) ;
	m_BombAnim				= MV1LoadModel( "Models/Enemy/Bom_A.mv1" ) ;
	m_ExploAnim				= MV1LoadModel( "Models/Enemy/ExplosionA_action.mv1" ) ;
	for( int i = 0 ; i < ee_End ; i++ ){
		if ( m_AnimModel[i] == -1 )
			return -1 ;
	}
	
	// --- 待機アニメの時間を取得
	m_Attachno[ee_Wait]			= MV1AttachAnim( g_Loadmodel[e_enemy], 0, m_AnimModel[ee_Wait] ) ;
	m_totalanimtime[ee_Wait]	= MV1GetAttachAnimTotalTime( g_Loadmodel[e_enemy], m_Attachno[ee_Wait] ) ;
	MV1DetachAnim( g_Loadmodel[e_enemy], m_Attachno[ee_Wait] ) ;	// アタッチを外す

	// --- アタックアニメの時間を取得
	m_Attachno[ee_Attack] = MV1AttachAnim( g_Loadmodel[e_enemy], 0, m_AnimModel[ee_Attack] ) ;
	m_totalanimtime[ee_Attack] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_enemy], m_Attachno[ee_Attack] ) ;
	MV1DetachAnim( g_Loadmodel[e_enemy], m_Attachno[ee_Attack] ) ;	// アタッチを外す

	// --- ボムアニメの時間を取得
	m_Battach = MV1AttachAnim( g_Loadmodel[e_bomb], 0, m_BombAnim ) ;
	m_Btotalanimtime = MV1GetAttachAnimTotalTime( g_Loadmodel[e_bomb], m_Battach) ;

	// --- 爆発アニメの時間を取得
	m_Eattach = MV1AttachAnim( g_Loadmodel[e_explo], 0, m_ExploAnim ) ;
	m_Etotalanimtime = MV1GetAttachAnimTotalTime( g_Loadmodel[e_explo], m_Eattach ) ;

	// --- ボムの大きさ変更
	MV1SetScale( g_Loadmodel[e_bomb] , VGet(2.0f,2.0f,2.0f) ) ;

	return 0 ;
}

/* ------------------------------------ */
/*										*/
/*			ボムのアクション			*/
/*										*/
/* ------------------------------------ */
void EnemyAction::BombAction(){

	switch ( m_BActionNo ) {

		// --- ボム待機
		case e_bomwait :
			m_bompos	= VGet( -575.0f, 50.0f, 575.0f ) ;
			break ;

		// --- ボム上り中
		case e_bomanim :
			g_Dispflg[e_bomb] = 1 ;
			m_Bplaytime += 0.8f ;	// --- ボムアニメ進行
			if( m_Bplaytime > m_Btotalanimtime )	
			{
				// --- 爆弾ダウン効果音再生
				PlaySoundMem( g_soundDT[e_Sbakuha],  DX_PLAYTYPE_BACK, TRUE ) ;		
				m_BActionNo = e_bomanimend ;			// --- 次へ
			}
			break ;

		// --- ボム上り終了
		case e_bomanimend :
			m_Bplaytime = 0.0f ;	// --- ボムアニメ初期化
			RandomPos() ;			// --- ポジション追加
			MV1DetachAnim( g_Loadmodel[e_bomb], m_Battach ) ;	// アタッチを外す


			m_BActionNo = e_bomfall ;		// --- 次へ
			break ;

		// --- ボム落ち中
		case e_bomfall :
			m_bompos = VAdd( m_bompos, m_bommove ) ;	// --- 落下
			if ( m_bompos.y < 20.0f ){
				g_Dispflg[e_explo] = 1 ;
				m_explopos = VAdd(m_bompos, VGet(0.0f, 0.0f, -50.0f)) ;	// ポジション入れ
	
				// --- 爆発効果音再生
				ChangeVolumeSoundMem( 155 , g_soundDT[e_Sbakuha + 1] ) ;
				PlaySoundMem( g_soundDT[e_Sbakuha + 1],  DX_PLAYTYPE_BACK, TRUE ) ;

				m_BActionNo = e_bomexplo ;		// --- 次へ
			}
			break ;

		// --- ボム着地、爆発
		case e_bomexplo :
			g_Dispflg[e_bomb]  = 0 ;
			g_Dispflg[e_explo] = 1 ;
			m_Eplaytime += 1.0f ;	// --- 爆発アニメ進行
			if ( m_Eplaytime > m_Etotalanimtime )
				m_BActionNo = e_bomexploend ;		// --- 次へ
			break ;

		// --- ボム爆発終了
		case e_bomexploend :
			m_bompos	= VGet( -575.0f, 50.0f, 575.0f ) ;
			g_Dispflg[e_explo] = 0 ;
			m_Eplaytime = 0.0f ;
			// --- ボムアニメセット
			m_Battach = MV1AttachAnim( g_Loadmodel[e_bomb], 0, m_BombAnim ) ;

			m_BActionNo = e_bomwait ;		// --- 次へ
			break ;
	}
}

/* ------------------------------------ */
/*										*/
/*		ランダムポジション入れ			*/
/*										*/
/* ------------------------------------ */
void EnemyAction::RandomPos()
{
	// --- ランダム
	srand( (unsigned) time (NULL) ) ;

	m_bompos.x = (float)(cos(((rand() % 17) + 1) * 100.0f * g_Pi / DEFANGLE) * ((rand() % 4) + 1) * 100.0f) ;
	m_bompos.y = 400.0f ;
	m_bompos.z = (float)(sin(((rand() % 17) + 1) * 100.0f * g_Pi / DEFANGLE) * ((rand() % 4) + 1) * 100.0f) ;

}


