
/* ==================================================================================

		アイテムパネルのアクション

+ -----------ファイルの概要----------------------------------------------------------
		アイテムパネルのアクション実装部
		回転しながらその場にとどまる

+ =================================================================================== */

#include "Common.h"

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
ItemPanelAction::ItemPanelAction() {

}

/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
ItemPanelAction::~ItemPanelAction() {

}

/* ------------------------------------ */
/*										*/
/*			  初期セット				*/
/*										*/
/* ------------------------------------ */
void ItemPanelAction::InitSet()
{
	m_pos = VGet( 0.0f, 50.0f, 0.0f ) ;
	m_move = VGet( 3.0f, 0.0f, 0.0f ) ;

	m_Playtime = 0 ;		// --- アニメーションの経過時間

	// --- フレーム情報の格納
	m_Rootflm			= MV1SearchFrame( g_Loadmodel[e_panel], "root" ) ;

	// --- パネル回転アニメーション読み込み
	m_AnimDt			= MV1LoadModel( "Models/Item/PanelA.mv1" ) ;

	// --- パネルアニメ時間の取得
	m_Attachidx			= MV1AttachAnim( g_Loadmodel[e_panel], 0, m_AnimDt ) ;
	m_Anim_totaltime	= MV1GetAttachAnimTotalTime( g_Loadmodel[e_panel], m_Attachidx ) ;


	m_flgcnt = P_EFFTIME ;	// --- 効果時間
}



/* ------------------------------------ */
/*										*/
/*		ランダムポジション入れ			*/
/*										*/
/* ------------------------------------ */
int ItemPanelAction::RandomPos()
{
	// --- ランダム
	srand( (unsigned) time (NULL) ) ;

	// --- 半径MAX500	角度は180.0f
	m_radius = ((rand() % 4) + 1) * 100.0f ;
	m_angle  = ((rand() % 17) + 1) * 100.0f ; 

	m_rotation.x = (float)(cos(m_angle * g_Pi / DEFANGLE) * m_radius) ;
	m_rotation.y = 50.0f ;
	m_rotation.z = (float)(sin(m_angle * g_Pi / DEFANGLE) * m_radius) ;

	// --- 座標のセット
	m_pos = VAdd(VGet(0.0f, 0.0f, 0.0f), m_rotation) ;

	// --- 配列の初期化
	m_ptopno[P1] = 0 ;
	m_ptopno[P2] = 0 ;
	m_ptopno[2] = 0 ;

	// --- 座標位置入れ
	g_Modelpos[e_panel] = m_pos ;

	return 0 ;
}



/* ------------------------------------ */
/*										*/
/*			  アクション				*/
/*										*/
/* ------------------------------------ */
void ItemPanelAction::Action()
{
	g_Dispflg[e_panel] = 1 ;	// --- 表示

	// --- パネル回転アニメーション進行
	m_Playtime += 0.5f ;
	if ( m_Playtime > m_Anim_totaltime ) {
		m_Playtime = 0.0f ;
	}
	// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	// --- (アニメーションの何番目（何秒後）の動きか同期させる
	MV1SetAttachAnimTime( g_Loadmodel[e_panel], m_Attachidx, m_Playtime ) ;

	// --- プレイヤーとのヒットチェック
	m_ptopno[2] = PtoPHitCheck() ;
	if ( m_ptopno[2] != 0 ) {
		//g_Dispflg[e_panel] = 0 ;	// --- 表示消し
		//m_pos = VGet( 0.0f, -300.0f, 0.0f ) ;	// --- 位置ずらし
		g_Dispflg[e_panel] = 0 ;				// --- 表示消し
		m_pos = VGet( 0.0f, -300.0f, 0.0f ) ;	// --- 位置ずらし

		if ( m_ptopno[2] == e_player1 )
			m_ptopno[P1] = 1 ;
		else if ( m_ptopno[2] == e_player2 )
			m_ptopno[P2] = 1 ;
	}
	// --- 効果時間処理
	if ( (m_ptopno[P1]) || (m_ptopno[P2]) )
		PlayerHit() ;

	// --- ２回目の初期化
	if ( (g_Timercnt == 70) && (m_pos.y <= -300.0f) ) {
		RandomPos() ;
		g_Dispflg[e_panel] = 1 ;	// --- 表示
	}

	// --- 座標位置入れ
	g_Modelpos[e_panel] = m_pos ;
	// --- サイズ変更
	MV1SetScale( g_Loadmodel[e_panel] , VGet(0.7f,0.7f,0.7f) ) ;

}



/* ------------------------------------ */
/*										*/
/*		　プレイヤーと当たった			*/
/*										*/
/* ------------------------------------ */
int ItemPanelAction::PlayerHit()
{
	int return_no = 0 ;

	// --- 効果時間カウント
	m_flgcnt-- ;
	if ( m_flgcnt < 0 ) {
		if ( m_ptopno[P1] )
			m_ptopno[P1] = 0 ;
		else if ( m_ptopno[P2] )
			m_ptopno[P2] = 0 ;
		m_flgcnt = P_EFFTIME ;	// --- カウント初期化
	}



	return return_no ;
}



