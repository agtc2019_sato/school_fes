
/* ==================================================================================

		リザルトのアクション

+ -----------ファイルの概要----------------------------------------------------------
		リザルトのクラスの実装部

+ =================================================================================== */

#include "Common.h"

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
ResultAction::ResultAction() {
	m_playercoin = true ;
	m_Resultflg = FALSE ;
}

/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
ResultAction::~ResultAction() {

}

/* ------------------------------------ */
/*										*/
/*			　初期化処理				*/
/*										*/
/* ------------------------------------ */
void ResultAction::InitSet() 
{
	m_Resultflg = TRUE ;

	// --- エフェクトを非表示にする
	for ( int i = e_hitef ; i < (e_Eend + 1) ; i++ )
	{
		g_Bmpimage[i].bmp_dispflg = 0 ;
	}

	// --- 一度すべてのモデルを非表示にする
	for ( int i = 0 ; i < e_end ; i++ ) 
	{
		g_Dispflg[i] = 0 ;
	}

	g_Dispflg[e_player1] = 1 ;				// --- 1Pのモデル表示有 
	g_Dispflg[e_player2] = 1 ;				// --- 2Pのモデル表示有
	g_Dispflg[e_stage2] = 1 ;				// --- 第二ステージ表示有

	// --- プレイヤーポジション
	m_Rtpos[0] = VGet( 400.0 , 0.0 , -400.0 ) ;
	m_Rtpos[1] = VGet( -400.0 , 0.0 , -400.0 ) ;

	g_Modelpos[e_player1]  = m_Rtpos[0] ;
	g_Modelpos[e_player2]  = m_Rtpos[1] ;

	m_Rtcount = 0 ;							// --- 回転数初期値

	scorememo[P1] = g_PScoer[P1] ;			// --- 1Pのスコア結果を記憶する
	scorememo[P2] = g_PScoer[P2] ;			// --- 2Pのスコア結果を記憶する

}

/* ------------------------------------ */
/*										*/
/*			  アクション				*/
/*										*/
/* ------------------------------------ */
void ResultAction::Action() 
{
	switch ( m_ActionNo )
	{
		// --- アニメーション準備
		case e_ResultBlank :

			// --- 1Pの勝利
			if ( g_PScoer[P1]  >  g_PScoer[P2] )
			{
				m_winBmp = e_1PWin ;
			}
			// --- 2Pの勝利
			else if ( g_PScoer[P1]  <  g_PScoer[P2] )
			{			
				m_winBmp = e_2PWin ;
			}
			else
			{
				// --- コイン判定
				if ( m_playercoin )
				{		
					// --- 1Pが勝ち
					m_winBmp = e_1PWin ;
				} else {
					// --- 2Pが勝ち
					m_winBmp = e_2PWin ;
				}				
			}

			AnimInit() ;							// --- アニメーションセット

			g_Bmpimage[m_winBmp].bmp_exp = 800 ;	// --- WIN画像のサイズ初期値

			m_ActionNo = e_RtPlyAnim ;
			break ;

		// --- レコード回転+プレイヤー待機
		case e_RtPlyAnim :

			// --- 回転フラグ
			if  ( m_Resultflg == TRUE )
			{
				AnimPlay() ;			// --- アニメーション
				PlayerRotation() ;		// --- 1P2Pの回転移動
			}
			else
			{
				// --- スコアを格納
				g_PScoer[P1] = scorememo[P1] ;
				g_PScoer[P2] = scorememo[P2] ;
			
				MV1DetachAnim( g_Loadmodel[e_player1], go_1Player.m_attachno[ep_Wait] ) ;	// --- アタッチを外す

				MV1DetachAnim( g_Loadmodel[e_player2], go_2Player.m_attachno[ep_Wait] ) ;	// --- アタッチを外す

				m_ActionNo = e_RtWinLosInit ;
			}
			break ;

		// --- 勝敗アニメーションセット
		case e_RtWinLosInit :
			AnimInit() ;
			
			// --- 勝利と歓声
			ChangeVolumeSoundMem( 175 , g_soundDT[e_Scheers] ) ;
			PlaySoundMem( g_soundDT[e_Scheers] , DX_PLAYTYPE_BACK , TRUE ) ;

			m_ActionNo = e_RtWinLos ;
			break ;

		// --- 勝敗アニメーションプレイ
		case e_RtWinLos :
			g_Dispflg[e_coin] = 1 ;		// --- コイン表示
			ResultWinAnim() ;
			ResultLoseAnim() ;
			BmpInit() ;
			break ;
	}
}

/* ------------------------------------ */
/*										*/
/*		アニメーションセット			*/
/*										*/
/* ------------------------------------ */
void ResultAction::BmpInit()
{
	//m_winBmp = e_2PWin ;

	//					WIN画像設定					//
	g_Bmpimage[m_winBmp].bmp_dispflg = 1 ;		// --- 表示有
	g_Bmpimage[m_winBmp].bmp_trans = 1 ;		// --- 透過有
	g_Bmpimage[m_winBmp].bmp_xy[0] = 0.5f ;		// --- x座標
	g_Bmpimage[m_winBmp].bmp_xy[1] = 0.5f ;		// --- y座標を調整
	g_Bmpimage[m_winBmp].bmp_angle = 0 ;		// --- 角度
	
	// ---エフェクト座標
	g_Bmpimage[m_winBmp].bmp_pos = VGet( 0.0f , 300.0f , 0.0f ) ;

	//					LOSE画像設定					//
	g_Bmpimage[e_Lose].bmp_dispflg = 1 ;		// --- 表示有
	g_Bmpimage[e_Lose].bmp_trans = 1 ;			// --- 透過有
	g_Bmpimage[e_Lose].bmp_xy[0] = 0.5f ;		// --- x座標
	g_Bmpimage[e_Lose].bmp_xy[1] = 0.5f ;		// --- y座標を調整
	g_Bmpimage[e_Lose].bmp_angle = 0 ;			// --- 角度
	g_Bmpimage[e_Lose].bmp_exp = 300 ;			// --- サイズ

	// ---エフェクト座標
	g_Bmpimage[e_Lose].bmp_pos = g_Modelpos[Lplyer] ;

	// --- 画像サイズ加算・減算を選択
	if ( g_Bmpimage[m_winBmp].bmp_exp >= 950 )
	{
		m_Imagesize = 1 ;			
	}
	else if ( g_Bmpimage[m_winBmp].bmp_exp <= 800 )
	{
		m_Imagesize = 2 ;
	}

	switch ( m_Imagesize )
	{
		case 1 :
			g_Bmpimage[m_winBmp].bmp_exp -= 2 ;			// --- サイズ加算
			break ;

		case 2 :
			g_Bmpimage[m_winBmp].bmp_exp += 2 ;			// --- サイズ減算
			break ;
	}
}

/* ------------------------------------ */
/*										*/
/*		アニメーションセット			*/
/*										*/
/* ------------------------------------ */
void ResultAction::AnimInit()
{
	//				勝利アニメーショセット			//
	go_Stage.m_Playtime	= 0 ;		// --- アニメーションの経過時間
	
	if ( m_winBmp == e_1PWin )
	{
		Wplyer = e_player1 ;
		Lplyer = e_player2 ;
	}
	else
	{
		Wplyer = e_player2 ;
		Lplyer = e_player1 ;
	}

	if ( m_ActionNo == e_ResultBlank )
	{
		// --- 待機アニメの時間を取得
		go_1Player.m_attachno[ep_Wait] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Wait] ) ;
		go_1Player.m_totalanimtime[ep_Wait] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player1], go_1Player.m_attachno[ep_Wait] ) ;

		go_2Player.m_attachno[ep_Wait] = MV1AttachAnim( g_Loadmodel[e_player2], 0, g_Animmodel[P2][ep_Wait] ) ;
		go_2Player.m_totalanimtime[ep_Wait] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player2], go_2Player.m_attachno[ep_Wait] ) ;
	}

	// --- アクションナンバーが勝敗アニメーションセットのとき
	if ( m_ActionNo == e_RtWinLosInit )
	{
		// --- 勝利アニメーションデータ
		m_Rt_AnimData[0] = MV1LoadModel( "Models/Player/RobotA_WIN.mv1" ) ;
	
		// --- 勝利アニメ時間の取得
		m_Rt_Attach[0]		= MV1AttachAnim( g_Loadmodel[Wplyer], 0, m_Rt_AnimData[0] ) ;
		m_Rt_Totaltime[0]	= MV1GetAttachAnimTotalTime( g_Loadmodel[Wplyer], m_Rt_Attach[0] ) ;
		
		// --- 敗北アニメーションデータ
		m_Rt_AnimData[1] = MV1LoadModel( "Models/Player/RobotA_LOSE.mv1" ) ;
		
		// --- 敗北アニメ時間の取得
		m_Rt_Attach[1]		= MV1AttachAnim( g_Loadmodel[Lplyer], 0, m_Rt_AnimData[1] ) ;
		m_Rt_Totaltime[1]	= MV1GetAttachAnimTotalTime( g_Loadmodel[Lplyer], m_Rt_Attach[1] ) ;
	}
}

/* ------------------------------------ */
/*										*/
/*			アニメーションプレイ		*/
/*										*/
/* ------------------------------------ */
void ResultAction::AnimPlay()
{
	//		レコード回転アニメーション		//
	// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	MV1SetAttachAnimTime( g_Loadmodel[e_stage2], go_Stage.m_Attachidx, go_Stage.m_Playtime ) ;

	go_Stage.m_Playtime += 0.5f ;

	if ( go_Stage.m_Playtime > go_Stage.m_Anim_totaltime )
	{
		go_Stage.m_Playtime = 0.0f ;
	}

	//		1Pプレイヤーの待機アニメーション	//

	MV1SetAttachAnimTime( g_Loadmodel[e_player1], go_1Player.m_attachno[ep_Wait]
						, go_1Player.m_playtime ) ;

	go_1Player.m_playtime += 1.0f ;
	
	if ( go_1Player.m_playtime > go_1Player.m_totalanimtime[ep_Wait] )
	{
		go_1Player.m_playtime = 0.0f ;
	}

	//		2Pプレイヤーの待機アニメーション	//
	
	MV1SetAttachAnimTime( g_Loadmodel[e_player2], go_2Player.m_attachno[ep_Wait]
						, go_2Player.m_playtime ) ;

	go_2Player.m_playtime += 1.0f ;
	
	if ( go_2Player.m_playtime > go_2Player.m_totalanimtime[ep_Wait] )
	{
		go_2Player.m_playtime = 0.0f ;
	}

	// --- スコア表示を高速で回す
	g_PScoer[P1]+= 3 ;
	g_PScoer[P2]+= 3 ;

	if ( g_PScoer[P1] > 55 )
	{
		g_PScoer[P1] = 0 ;
		g_PScoer[P2] = 0 ;
	}
}

/* ------------------------------------ */
/*										*/
/*		1P2Pプレイヤーの回転			*/
/*										*/
/* ------------------------------------ */
void ResultAction::PlayerRotation() 
{
	for ( int i = 0 ; i < 2 ; i++ )
	{
		//半径処理//
		m_Square[i][0] = pow((double)m_Rtpos[i].x, 2) ;		// --- ２乗
		m_Square[i][1] = pow((double)m_Rtpos[i].z, 2) ;		// --- ２乗
		m_Square[i][2] = m_Square[i][0] + m_Square[i][1] ;	// --- 足し算
		m_radius[i] = sqrt( m_Square[i][2] ) ;			// --- √計算

		//角度処理//
		m_angle[i][0] = atan2(m_Rtpos[i].z, m_Rtpos[i].x) ;
		m_angle[i][1] = m_angle[i][0] / (g_Pi / DEFANGLE) ;

		//外への移動//
		m_radius[i] = 500.0f ;
		m_angle[i][1] -= 2 ;

		// 代入 //
		if ( m_radius[i] < 650.0f ) {	// --- radiusが600以下の場合は代入
			m_Rtrotation[i].x = (float)(cos(m_angle[i][1] * g_Pi / DEFANGLE) * m_radius[i]) ;
			m_Rtrotation[i].y = 0.0f ;
			m_Rtrotation[i].z = (float)(sin(m_angle[i][1] * g_Pi / DEFANGLE) * m_radius[i]) ;
		}

		m_Rtpos[i] = VAdd( VGet(0.0f, 0.0f, 0.0f), m_Rtrotation[i] ) ;

		if ( i == 0 )
		{
			g_Modelpos[e_player1] = m_Rtpos[i] ;
		}
		else
		{
			g_Modelpos[e_player2] = m_Rtpos[i] ;
		}
	}


	// --- 回転を止める座標範囲
	if ( (g_Modelpos[Wplyer].x >= -7) && (g_Modelpos[Wplyer].x <= 9) && (g_Modelpos[Wplyer].z <= -480) )
	{
		m_Rtcount++ ;

		if ( m_Rtcount == RESULT_ROT )
		{
			// --- 第一ステージBGM音を止める
			StopSoundMem( g_soundDT[e_Sresult] ) ;
			
			// --- 結果発表ドラム音
			PlaySoundMem( g_soundDT[e_Sresult + 1],  DX_PLAYTYPE_BACK, TRUE ) ;

			m_Resultflg = FALSE ;
		}		
	}
}

/* ------------------------------------ */
/*										*/
/*			 勝利アニメーション			*/
/*										*/
/* ------------------------------------ */
void ResultAction::ResultWinAnim()
{
	// --- キャラモデルにアニメ進行時間を与える
	MV1SetAttachAnimTime( g_Loadmodel[Wplyer], m_Rt_Attach[0], m_Rt_Playtime[0] ) ;

	m_Rt_Playtime[0] += 0.5f ;			// --- アニメション進行

	if ( m_Rt_Playtime[0] > m_Rt_Totaltime[0] ) 
	{
		m_Rt_Playtime[0] = 0.0f ;
	}
}

/* ------------------------------------ */
/*										*/
/*			 敗北アニメーション			*/
/*										*/
/* ------------------------------------ */
void ResultAction::ResultLoseAnim()
{
	// --- キャラモデルにアニメ進行時間を与える
	MV1SetAttachAnimTime( g_Loadmodel[Lplyer], m_Rt_Attach[1], m_Rt_Playtime[1] ) ;

	m_Rt_Playtime[1] += 0.5f ;						// --- アニメション進行

	if ( m_Rt_Playtime[1] > m_Rt_Totaltime[1] ) 
	{
		m_Rt_Playtime[1] = 0.0f ;
	}
}

