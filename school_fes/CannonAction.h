
/* ==================================================================================

		大砲のアクション

+ -----------ファイルの概要----------------------------------------------------------
		大砲のクラス		継承元 → BasicObjクラス

+ =================================================================================== */

class CannonAction : BasicObj
{
	public :
		CannonAction() ;								// --- コンストラクタ
		~CannonAction() ;								// --- デストラクタ

		void InitSet() ;								// --- 初期セット
		
		void Action() ;									// --- 関数まとめ
		void CannonAnim() ;								// --- アニメーション

		void FirStagePosi() ;							// --- 第一ステージポジション
		void SecoStagePosi() ;							// --- 第二ステージポジション		

		float m_CA_xposi[20] , m_CA_yposi[20] ;			// --- 大砲のポジション

		BOOL m_CA_flg[5] ;								// --- アニメーションフラグ
		BOOL m_CA_bulletflg[5] ;						// --- 弾のフラグ


		BOOL m_CA_randflg ;								// --- ランダムフラグ

		int m_cannon_put ;								// --- 表示する大砲の数

		int m_blank_can[5] ;							// --- 空きの要素数

		time_t timer ;									// --- 時間
		struct tm *local ;

		int m_CActionNo ;

	//private :
		void BulletMove()	;							// ---　弾丸の動き

		float BulletRadian( float , float , float , float ) ;	// --- 弾の角度を取得
		
		void CannonRadian( int ) ;						// --- 大砲の角度を取得

		void CannonPosition() ;							// --- 大砲が置かれる座標を取得

		void CannonShaful() ;							// --- 大砲の位置を取得

		void CannonRest(int arg_xcnt) ;					// --- 大砲と設定リセット
	

		BOOL m_ShafulFlg ;

		int m_CA_atack ;								// --- 大砲アニメーション格納		
		int m_CA_attach ;								// --- 大砲アニメを関連付ける		
		float m_CA_playtime[5] ;						// --- 進行中の時間
		float m_CA_totaltime[5] ;						// --- トータル時間
		float m_CA_radian[5] ;							// --- 角度格納
		float m_CA_can_ragian	;						// --- 大砲角度格納
		
		int m_xcnt , m_ycnt ;							// --- コピーモデルの要素数
		int m_blankcnt ;								// --- 空きの大砲数
		int m_CA_DispFlg[2][5] ;						// --- 大砲・弾のモデル表示フラグ
		int m_smemory ;									// --- 秒数を記憶

		VECTOR m_CenterPosi ;							// --- 中心座標
		VECTOR m_PosiVec ;								// --- 大砲と中心座標までのベクトル

		typedef struct
		{
			int kosu ;									// --- 大砲の個数
			float CA_angle ;							// --- 円の角度
			float CA_xp ;								// --- 大砲の配置x座標
			float CA_yp ;								// --- 大砲の配置y座標
			float CA_radius ;							// --- 円の半径

		} CannonsetPosi ;

		CannonsetPosi m_CA_Setposi ;
		
		// --- 大砲サイズ
		float m_CAsizeX[CANNON_MAX] ;
		float m_CAsizeY[CANNON_MAX] ;
		float m_CAsizeZ[CANNON_MAX] ;

} ;


