
/* ==================================================================================

		描画ループ

+ -----------ファイルの概要----------------------------------------------------------
		モデルや画像の描画

+ =================================================================================== */

#include "Common.h"

int DrawLoop()
{
	int return_no = 0 ;		// 返値入れ

	// --- 背景画像
	if ( DrawGraph( 0 , 0 , g_Bmpimage[e_backGB].bmp_handle , FALSE) != 0 )
		return_no = -1 ;	// エラー終了値

	if ( g_SceneNo <= e_SelectInit ) {
		// --- タイトル文字
		if ( g_Bmpimage[e_titstr].bmp_dispflg == 1 )
			DrawGraph( 600 , 780 , g_Bmpimage[e_titstr].bmp_handle , TRUE ) ;
		// --- タイトルロゴ
		if ( g_Bmpimage[e_titlogo].bmp_dispflg == 1 )
			DrawRotaGraph( WINDOW_W / 2 + 50 , 300, g_titsize, 0.0f,
							g_Bmpimage[e_titlogo].bmp_handle , TRUE ) ;
	}

	// --- セレクト時の背景
	if ( ( e_Select <= g_SceneNo) && (g_SceneNo < e_Stage1Init) )
		DrawGraph( -115 , 0 , g_Bmpimage[e_selectGB].bmp_handle , TRUE ) ;

	// モデルポジション
	//for ( int i = 0 ; i < 2 ; i++ ) {
	//	MV1SetPosition(g_Loadmodel[i], g_Modelpos[i]) ;

	//}
	MV1SetPosition(g_Loadmodel[e_stage], g_Modelpos[e_stage]) ;
	MV1SetPosition(g_Loadmodel[e_player1], g_Modelpos[e_player1]) ;
	MV1SetPosition(g_Loadmodel[e_player2], g_Modelpos[e_player2]) ;
	MV1SetPosition( g_Loadmodel[e_coin], g_Modelpos[e_coin] ) ;
	MV1SetPosition( g_Loadmodel[e_panel], g_Modelpos[e_panel] ) ;
	MV1SetPosition( g_Loadmodel[e_enemy], g_Modelpos[e_enemy] ) ;
	MV1SetPosition( g_Loadmodel[e_bomb], g_Modelpos[e_bomb] ) ;
	MV1SetPosition( g_Loadmodel[e_explo], g_Modelpos[e_explo] ) ;
	MV1SetPosition( g_Loadmodel[e_table], g_Modelpos[e_table] ) ;
	MV1SetPosition( g_Loadmodel[e_stage2], g_Modelpos[e_stage2] ) ;
	MV1SetPosition( g_Loadmodel[e_recode], g_Modelpos[e_recode] ) ;

	for ( int first_cnt = 0 ; first_cnt < 5 ; first_cnt++ )
	{
		//---  大砲を位置をセットする
		MV1SetPosition( g_Loadmodel[e_cannon + go_Cannon.m_blank_can[first_cnt]] , g_Modelpos[e_cannon + go_Cannon.m_blank_can[first_cnt]] ) ;
		// --- 座標セット
		MV1SetPosition( g_Loadmodel[e_bullet + first_cnt] , g_Modelpos[e_bullet + first_cnt] ) ;
	}

	// --- モデル描画
	for( int i = 0 ; i < e_end ; i++ ) {
		if ( (i != e_player1) || (i != e_player2) || (i != e_coin) ) {
			if ( (g_Loadmodel[i] != 0) && (g_Dispflg[i] != 0) )
				MV1DrawModel( g_Loadmodel[i] ) ;
		}
	}

	// --- エフェクト画像描画
	go_Effect.EffectDrawP( ) ;	// --- プレイヤー前エフェクト
	// --- タイマー文字表示
	if ( g_SceneNo >= e_Stage1Init ) {
		SetFontSize( 150 ) ;
		ChangeFont( "851マカポップ", DX_CHARSET_DEFAULT ) ;
		// --- タイマー表示
		if ( g_Bmpimage[e_TBanner].bmp_dispflg )
			DrawFormatString( ((WINDOW_W / 2) - 100), 50, RGB(00,00,255), "%02d" , g_Timercnt ) ;
	}
	// エフェクト前に置くモデル
	if ( (g_Loadmodel[e_player1] != 0) && (g_Dispflg[e_player1] != 0) )
		MV1DrawModel( g_Loadmodel[e_player1] ) ;
	if ( (g_Loadmodel[e_player2] != 0) && (g_Dispflg[e_player2] != 0) )
		MV1DrawModel( g_Loadmodel[e_player2] ) ;
	if ( (g_Loadmodel[e_coin] != 0) && (g_Dispflg[e_coin] != 0) )
		MV1DrawModel( g_Loadmodel[e_coin] ) ;
	go_Effect.EffectDraw( ) ;	// --- エフェクト

	// --- セレクト用キャラ画像
	if ( g_SceneNo == e_Select ) {
		for ( int i = e_PSele1 ; i < e_kettei ; i++ ) {
			if ( g_Bmpimage[i].bmp_dispflg ) {
				DrawGraph( go_TitChaSet.m_imagepos[i - e_PSele1].x,go_TitChaSet.m_imagepos[i - e_PSele1].y,
						   g_Bmpimage[i].bmp_handle,TRUE ) ;
			}
		}
	}
	// --- 確認画面
	if ( go_TitChaSet.m_sceneno == 4 ) {
		// --- 確認画面
		if ( g_Bmpimage[e_kettei].bmp_dispflg )
			DrawGraph(  427, 140, g_Bmpimage[e_kettei].bmp_handle, TRUE ) ;
		// --- 選択枠
		for ( int i = e_waku3 ; i < e_1PWin ; i++ ){
			if ( g_Bmpimage[i].bmp_dispflg ){
				DrawGraph(  go_TitChaSet.m_imagepos[i - e_PSele2].x,
							go_TitChaSet.m_imagepos[i - e_PSele2].y,
							g_Bmpimage[i].bmp_handle,
							TRUE ) ;
			}
		}
	} else {
		// --- リザルトの描画
		for ( int i = e_waku4 ; i < e_PBanner1 ; i++ ) {
			if ( g_Bmpimage[i].bmp_dispflg ) {
 				DrawBillboard3D( g_Bmpimage[i].bmp_pos, g_Bmpimage[i].bmp_xy[0], g_Bmpimage[i].bmp_xy[1],
									(float)g_Bmpimage[i].bmp_exp, g_Bmpimage[i].bmp_angle,
									g_Bmpimage[i].bmp_handle, TRUE ) ;
			}
		}
	}



	// ==文字表示== //
	if ( g_SceneNo >= e_Stage1Init ) {
		ChangeFont( "851マカポップ", DX_CHARSET_DEFAULT ) ;
		SetFontSize( 100 ) ;
		// --- スコア表示
		if ( (g_SceneNo <= e_ResultInit) && (g_Timercnt <= 30) ) {
			DrawFormatString( 80, 65, RGB(00,00,255), "   ??" ) ;
			DrawFormatString( (WINDOW_W - 500), 65, RGB(00,00,255), "   ??" ) ;
		} else {
			DrawFormatString( 80, 65, RGB(00,00,255), "   %d" , g_PScoer[P1] ) ;
			DrawFormatString( (WINDOW_W - 500), 65, RGB(255,00,00), "   %d", g_PScoer[P2] ) ;
		}
		SetFontSize( 60 ) ;
		DrawFormatString( 200, 10, RGB(00,00,200), "Player1" ) ;
		DrawFormatString( (WINDOW_W - 400), 10, RGB(200,00,00), "Player2" ) ;
	}

	//SetFontSize( 30 ) ;	
	//ChangeFont( "ＭＳ　ゴシック", DX_CHARSET_DEFAULT ) ;
	//DrawFormatString( 50, 350, RGB(200,00,00), "1Pcnt = %d" , go_1Player.m_hitcnt ) ;
	//DrawFormatString( 50, 400, RGB(00,00,200), "2Pcnt = %d" , go_2Player.m_hitcnt ) ;
	//DrawFormatString( 50, 500, RGB(255,00,00), "playtime = %0.2f" , go_Cannon.m_CA_playtime[0] ) ;
	//DrawFormatString( 50, 550, RGB(00,255,255), "coin.x = %0.2f" , g_Modelpos[e_coin].x ) ;
	//DrawFormatString( 50, 600, RGB(00,255,255), "coin.y = %0.2f" , g_Modelpos[e_coin].y ) ;

	// --- NEXT STAGE　の画像表示
	if ( (CheckSoundMem( g_soundDT[e_Swaring] ) == 1) && (g_Timercnt == MAXTIME) )
		DrawGraph(  500 , 200 ,	g_Bmpimage[e_Nextst].bmp_handle , TRUE ) ;



// --- DX_BLENDMODE_ADD	: 明るくする
// --- DX_BLENDMODE_SUB	: 暗くする
// --- DX_BLENDMODE_NOBLEND : 通常
//SetDrawBlendMode( DX_BLENDMODE_ADD, 255 ) ;		// --- 加算描画
// --- 3D空間に画像を表示する
//DrawBillboard3D( g_Modelpos[e_player1], 0.5f, 0.5f, 64, 0.0f, g_effectBmp[1], TRUE ) ;
//SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;	// --- 通常描画

	// --- 切替画像
	if ( g_Bmpimage[e_screenGB].bmp_dispflg != 0 ) {
		if ( g_SceneNo >= e_ResultInit ) {
			SetDrawBlendMode( DX_BLENDMODE_ADD, go_TitChaSet.m_fadecnt ) ;		// --- 加算描画
			DrawBillboard3D( VGet(0.0f,0.0f,0.0f), 0.5f, 0.5f, 5000.0f, 0.0f, g_Bmpimage[e_screenGB].bmp_handle, TRUE ) ;
		} else if ( g_SceneNo >= e_Explanation ) {
			if ( DrawGraph( (int)g_Bmpimage[e_screenGB].bmp_xy[0] , (int)g_Bmpimage[e_screenGB].bmp_xy[1] , g_Bmpimage[e_screenGB].bmp_handle , TRUE) != 0 )
				return_no = -1 ;	// エラー終了値
		} else {
			SetDrawBlendMode( DX_BLENDMODE_SUB, go_TitChaSet.m_fadecnt ) ;		// --- 加算描画
			DrawBillboard3D( VGet(0.0f,0.0f,0.0f), 0.5f, 0.5f, 3000.0f, 0.0f, g_Bmpimage[e_screenGB].bmp_handle, TRUE ) ;
		}
	}
	SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;	// --- 通常描画


	return return_no ;

}

