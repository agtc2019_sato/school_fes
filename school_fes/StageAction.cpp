
/* ==================================================================================

		ステージのアクション

+ -----------ファイルの概要----------------------------------------------------------
		ステージのクラスの実装部

+ =================================================================================== */

#include "Common.h"

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
StageAction::StageAction() {
	g_stflg = 0 ;
}

/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
StageAction::~StageAction() {

}


/* ------------------------------------ */
/*										*/
/*			  初期セット				*/
/*										*/
/* ------------------------------------ */
void StageAction::InitSet()
{
	// --- セレクト画面
	if ( g_stflg == 0 )
	{
		m_keihoFlg = 0 ;				// --- 警報音フラグ初期値

		printf( "レコードアニメーション" ) ;
		//				レコード回転のアニメーション			//
		m_Playtime = 0 ;		// --- アニメーションの経過時間

		// --- フレーム情報の格納
		m_Rootflm			= MV1SearchFrame( g_Loadmodel[e_recode], "root" ) ;

		// --- レコード回転アニメーション読み込み
		m_AnimDt			= MV1LoadModel( "Models/Stage/Stage02_05A.mv1" ) ;
		// --- レコード回転アニメ時間の取得
		m_Attachidx			= MV1AttachAnim( g_Loadmodel[e_recode], 0, m_AnimDt ) ;
		m_Anim_totaltime	= MV1GetAttachAnimTotalTime( g_Loadmodel[e_recode], m_Attachidx ) ;	

				//				レコードオープンのアニメーション			//
		m_Rc_Playtime[1] = 0 ;
		// --- レコードのオープンアニメ読み込み
		m_Rc_AnimDt[1]  = MV1LoadModel( "Models/Stage/OpenA.mv1" ) ; 
		
		// --- レコードのオープンアニメ時間の取得
		m_Rc_Attach[1]	= MV1AttachAnim( g_Loadmodel[e_stage2], 0, m_Rc_AnimDt[1] ) ;
		m_Rc_Totaltime[1]	= MV1GetAttachAnimTotalTime( g_Loadmodel[e_stage2], m_Rc_Attach[1] ) ;
	}
	// --- 第一ステージ
	else if ( g_stflg == 1 )
	{
		printf( "第一アニメーション" ) ;
		m_Rc_Playtime[0] = 0 ;
		m_Rc_AnimDt[0]  = MV1LoadModel( "Models/Stage/Stage01_04A.mv1" ) ; 
		m_Rc_Attach[0]	= MV1AttachAnim( g_Loadmodel[e_stage], 0, m_Rc_AnimDt[0] ) ;
		m_Rc_Totaltime[0]	= MV1GetAttachAnimTotalTime( g_Loadmodel[e_stage], m_Rc_Attach[0] ) ;
		
	}
	// --- 第二ステージ
	else if ( g_stflg == 2 )
	{
		//				レコード落下のアニメーション			//
		m_Rc_Playtime[2] = 0 ;

		// --- レコードの上から落ちるアニメ読み込み
		m_Rc_AnimDt[2]  = MV1LoadModel( "Models/Stage/RecodeA03.mv1" ) ; 
		
		// --- レコードの上から落ちるアニメ時間の取得
		m_Rc_Attach[2]	= MV1AttachAnim( g_Loadmodel[e_stage2], 0, m_Rc_AnimDt[2] ) ;
		m_Rc_Totaltime[2] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_stage2], m_Rc_Attach[2] ) ;

		//				レコード盤ハリのアニメーション			//
		m_Rc_Playtime[3] = 0 ;
		// --- ハリアニメ読み込み
		m_Rc_AnimDt[3] = MV1LoadModel( "Models/Stage/hariA.mv1" ) ; 

		// --- ハリアニメ時間の取得
		m_Rc_Attach[3] = MV1AttachAnim( g_Loadmodel[e_stage2], 0, m_Rc_AnimDt[3] ) ;
		m_Rc_Totaltime[3] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_stage2], m_Rc_Attach[3] ) ;		

		//				レコード回転のアニメーション			//
		m_Playtime = 0 ;		// --- アニメーションの経過時間

		// --- フレーム情報の格納
		m_Rootflm			= MV1SearchFrame( g_Loadmodel[e_stage2], "root" ) ;

		// --- レコード回転アニメーション読み込み
		m_AnimDt			= MV1LoadModel( "Models/Stage/Stage02_05A.mv1" ) ;
		// --- レコード回転アニメ時間の取得
		m_Attachidx			= MV1AttachAnim( g_Loadmodel[e_stage2], 0, m_AnimDt ) ;
		m_Anim_totaltime	= MV1GetAttachAnimTotalTime( g_Loadmodel[e_stage2], m_Attachidx ) ;	

		stanimflg = 0 ;			// --- ステージアニメーションフラグ初期値

		// == 第２ステージのカメラ初期位置== //
		g_Camerapos	= VGet( 0.0f, 700.0f, -1000.0f ) ;		// --- カメラポジション
		g_Cameratgt	= VGet( 0.0f, 0.0f, 0.0f ) ;			// --- カメラ注視点
		SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;

	}

}

/* ------------------------------------ */
/*										*/
/*			  アクション				*/
/*										*/
/* ------------------------------------ */
void StageAction::Action()
{
	switch ( g_stflg )
	{

		case 0 :
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			// --- (アニメーションの何番目（何秒後）の動きか同期させる
			MV1SetAttachAnimTime( g_Loadmodel[e_recode], m_Attachidx, m_Playtime ) ;

			m_Playtime += 0.5f ;
			if ( m_Playtime > m_Anim_totaltime ){
				m_Playtime = 0.0f ;
			}
			break ;

		// --- 第一ステージ処理
		case 1 :
			MV1SetAttachAnimTime( g_Loadmodel[e_stage],m_Rc_Attach[0], m_Rc_Playtime[0] ) ;

			m_Rc_Playtime[0] += 0.28f ;		// --- アニメーション進行

			if ( m_Rc_Playtime[0] > m_Rc_Totaltime[0] ) {
				m_Rc_Playtime[0] = 0.0f ;
				stanimflg = 0 ;
				MV1DetachAnim( g_Loadmodel[e_stage] ,m_Rc_Attach[0] ) ;
			}
			// == カメラポジション設定 == //
			if ( m_stgcmrpos < 1000.0f ) {
				SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;
				g_Camerapos	= VGet( m_stgcmrpos, 700.0f, -1000.0f ) ;		// --- カメラポジション
				g_Cameratgt	= VGet( m_stgcmrpos, 0.0f, 0.0f ) ;				// --- カメラ注視点
				m_stgcmrpos += 7.0f ;
				if ( m_stgcmrpos > 500.0f )
					g_Bmpimage[e_screenGB].bmp_dispflg = 1 ;
			}
			// == プレイヤーの動き == //
			go_1Player.Stage1move() ;		// --- プレイヤー移動アニメーション
			go_2Player.Stage1move() ;		// --- プレイヤー移動アニメーション
			break ;

		// --- 第二ステージ処理
		case 2 :
			// --- オープンアニメーション
			if ( stanimflg == 0 )
			{
				MV1SetAttachAnimTime( g_Loadmodel[e_stage2],m_Rc_Attach[1], m_Rc_Playtime[1] ) ;

				m_Rc_Playtime[1] += 0.5f ;			// --- アニメーション進行

				if ( m_Rc_Playtime[1] > m_Rc_Totaltime[1] ){
					printf( "ERRR\n" ) ;
					m_Rc_Playtime[1] = 0.0f ;
					MV1DetachAnim( g_Loadmodel[e_stage2] ,m_Rc_Attach[1] ) ;
					stanimflg = 1 ;
				}
			}
			// --- レコード落下アニメーション
			if ( stanimflg == 1 )
			{
				MV1SetAttachAnimTime( g_Loadmodel[e_stage2],m_Rc_Attach[2], m_Rc_Playtime[2] ) ;
				
				m_Rc_Playtime[2] += 0.25f ;			// --- アニメーション進行

				if ( m_Rc_Playtime[2] > m_Rc_Totaltime[2] ){
					m_Rc_Playtime[2] = 0.0f ;
					MV1DetachAnim( g_Loadmodel[e_stage2] ,m_Rc_Attach[2] ) ;
					stanimflg = 2 ;
				}
				// == プレイヤーの動き == //
				go_1Player.Stage2move() ;
				go_2Player.Stage2move() ;
			}

			// --- ハリのアニメーション
			if ( stanimflg == 2 )
			{
				MV1SetAttachAnimTime( g_Loadmodel[e_stage2],m_Rc_Attach[3], m_Rc_Playtime[3] ) ;
				
				m_Rc_Playtime[3] += 0.5f ;			// --- アニメーション進行

				if ( m_Rc_Playtime[3] > m_Rc_Totaltime[3] ){
					m_Rc_Playtime[3] = 0.0f ;
					MV1DetachAnim( g_Loadmodel[e_stage2] ,m_Rc_Attach[3] ) ;
					// == 表示系の初期化 == //
					g_Dispflg[e_coin] = 1 ;		// --- コイン表示
					// --- 第二ステージタイマー作動中
					g_timeflg = TRUE ;

					// --- 第一ステージBGM音を止める
					StopSoundMem( g_soundDT[e_Bfirst] ) ;

					// --- 第二ステージBGM再生
					ChangeVolumeSoundMem( 155 , g_soundDT[e_Bsecound] ) ;
					PlaySoundMem( g_soundDT[e_Bsecound], DX_PLAYTYPE_LOOP, TRUE ) ;

					stanimflg = 3 ;	// --- フラグ消し
				}
				// == プレイヤーの動き == //
				go_1Player.Stage2move() ;
				go_2Player.Stage2move() ;
			}

			// --- レコード回転アニメーション進行
			// --- タイマーが作動中
			if ( g_timeflg == TRUE ){
				// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
				// --- (アニメーションの何番目（何秒後）の動きか同期させる
				MV1SetAttachAnimTime( g_Loadmodel[e_stage2], m_Attachidx, m_Playtime ) ;

				m_Playtime += 0.5f ;
				if ( m_Playtime > m_Anim_totaltime ){
				m_Playtime = 0.0f ;
			}
		}
		break ;
	}
}

