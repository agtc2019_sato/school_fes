
/* ==================================================================================

		エフェクトの処理を行う関数

+ -----------ファイルの概要----------------------------------------------------------

		エフェクトの処理を行う

+ =================================================================================== */
#include "Common.h"

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
EffectAction::EffectAction() {

}

/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
EffectAction::~EffectAction() {

}

/* ------------------------------------ */
/*										*/
/*				初期セット				*/
/*										*/
/* ------------------------------------ */
void EffectAction::InitSet( )
{	
	m_E_plytime[0] = 0.0f ;			// --- プレイタイム				
	m_efflg = 0 ;					// --- 描画フラグの初期値

	// --- 表示フラグ初期化
	for ( int ecnt = e_hitef ; ecnt < e_Eend ; ecnt++ )
		g_Bmpimage[ecnt].bmp_dispflg = 0 ;

	// --- スタン時のエフェクト表示フラグ
	m_stanflg = 0 ;
	
	for ( int i = 0 ; i < CANNON_MAX ; i++ )
	{
		m_cloudFlg[i] = FALSE ;			// --- 煙フラグ
	}

	// --- 初期化
	g_Bmpimage[e_PBanner1].bmp_dispflg = 0 ;
	g_Bmpimage[e_PBanner2].bmp_dispflg = 0 ;
	g_Bmpimage[e_TBanner].bmp_dispflg = 0 ;
}



/* ------------------------------------ */
/*										*/
/*		エフェクトアクション			*/
/*										*/
/* ------------------------------------ */
void EffectAction::Action( )
{
	// --- ヒットエフェクト
	if ( m_efflg != 0 )
	{
		g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_dispflg = 1 ;		// --- 表示有
		g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_trans = 1 ;			// --- 透過有
		g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_xy[0] = 0.5f ;		// --- x座標
		g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_xy[1] = 0.0f ;		// --- y座標を調整
		g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_exp += 15 ;			// --- サイズ加算
		g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_angle = 0 ;			// --- 角度
		// ---エフェクト座標
		g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_pos = g_Modelpos[e_player1 + (m_efflg - 1)] ;
		// --- 限界加算値
		if ( g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_exp > 150 ) {
			g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_exp = 0 ;
			g_Bmpimage[e_hitef + (m_efflg - 1)].bmp_dispflg = 0 ;	// --- 表示無
			m_efflg = 0 ;
		}
	}

	// --- 煙エフェクト表示
	for ( int icnt = 0 ; icnt < CANNON_MAX ; icnt++ )
	{
		// --- 大砲が出たら
		if ( m_cloudFlg[icnt] == TRUE )
		{
			// --- 表示有無
			g_Bmpimage[e_cloudef + icnt].bmp_dispflg = 1 ;
			// --- 中心座標
			g_Bmpimage[e_cloudef + icnt].bmp_xy[0] = 0.5f  ;
			g_Bmpimage[e_cloudef + icnt].bmp_xy[1] = 0.5f  ;
			// --- エフェクトポジション
			g_Bmpimage[e_cloudef + icnt].bmp_pos = g_Modelpos[e_cannon + icnt] ;
			// --- 加算の限界値
			if ( g_Bmpimage[e_cloudef + icnt].bmp_exp < 200 ) {
				g_Bmpimage[e_cloudef + icnt].bmp_exp += 7 ;
			}
			else
			{
				m_cloudFlg[icnt] = FALSE ;
				// --- 表示無
				g_Bmpimage[e_cloudef + icnt].bmp_dispflg = 0 ;
			}
		} else {
			g_Bmpimage[e_cloudef + icnt].bmp_exp = 0 ;
			g_Bmpimage[e_cloudef + icnt].bmp_dispflg = 0 ;
		}
	}
	

	// --- スタン
	if ( m_stanflg & 0x01 ) {
		g_Bmpimage[e_stanef].bmp_dispflg = 1 ;
		g_Bmpimage[e_stanef].bmp_pos = g_Modelpos[e_player1] ;	// --- 1P スタンエフェクト座標値
		g_Bmpimage[e_stanef].bmp_pos.y += 250 ;					// --- 1P y座標値調整
		g_Bmpimage[e_stanef].bmp_angle = 0 ;					// --- 1P 角度
		// --- 中心座標
		g_Bmpimage[e_stanef].bmp_xy[0] = 0.5f  ;
		g_Bmpimage[e_stanef].bmp_xy[1] = 0.5f  ;
		// --- アニメーション終了で初期化
		if ( go_1Player.m_playtime >= go_1Player.m_totalanimtime[ep_Stan] ) {
			g_Bmpimage[e_stanef].bmp_dispflg = 0 ;
			g_Bmpimage[e_stanef].bmp_exp = 0 ;
			m_stanflg &= 0xfe ;		// --- フラグ消し
		} else {
			// --- 加算の限界値
			if ( g_Bmpimage[e_stanef].bmp_exp < 100 )
				g_Bmpimage[e_stanef].bmp_exp += 5 ;					// --- 1P サイズ加算
		}
	} else if ( m_stanflg & 0x02 ) {
		g_Bmpimage[e_stanef + 1].bmp_dispflg = 1 ;
		g_Bmpimage[e_stanef + 1].bmp_pos = g_Modelpos[e_player2] ;	// --- 2P スタンエフェクト座標値
		g_Bmpimage[e_stanef + 1].bmp_pos.y += 250 ;					// --- 2P y座標値調整
		g_Bmpimage[e_stanef + 1].bmp_angle = 0 ;					// --- 2P 角度
		// --- 中心座標
		g_Bmpimage[e_stanef + 1].bmp_xy[0] = 0.5f  ;
		g_Bmpimage[e_stanef + 1].bmp_xy[1] = 0.5f  ;
		// --- アニメーション終了で初期化
		if ( go_2Player.m_playtime >= go_2Player.m_totalanimtime[ep_Stan] ) {
			g_Bmpimage[e_stanef + 1].bmp_dispflg = 0 ;
			g_Bmpimage[e_stanef + 1].bmp_exp = 0 ;
			m_stanflg &= 0xfd ;		// --- フラグ消し
		} else {
			if ( g_Bmpimage[e_stanef + 1].bmp_exp < 100 )
				g_Bmpimage[e_stanef + 1].bmp_exp += 5 ;				// --- 2P サイズ加算
		}
	} else {
		// --- 初期化
		g_Bmpimage[e_stanef].bmp_dispflg = 0 ;
		g_Bmpimage[e_stanef + 1].bmp_dispflg = 0 ;
		g_Bmpimage[e_stanef].bmp_exp = 0 ;
		g_Bmpimage[e_stanef + 1].bmp_exp = 0 ;
	}


	// --- パネル獲得時のエフェクト
	if ( go_Panel.m_ptopno[P1] ) {
		g_Bmpimage[e_waku1].bmp_dispflg = 1 ;
		g_Bmpimage[e_waku1].bmp_pos = g_Modelpos[e_player1] ;	// --- 1P 座標値
		g_Bmpimage[e_waku1].bmp_pos.y += 50 ;					// --- 1P y座標値調整
		g_Bmpimage[e_waku1].bmp_angle = 0 ;						// --- 1P 角度
		g_Bmpimage[e_waku1].bmp_exp = 100 ;						// --- 1P 大きさ
		// --- 中心座標
		g_Bmpimage[e_waku1].bmp_xy[0] = 0.5f  ;
		g_Bmpimage[e_waku1].bmp_xy[1] = 0.5f  ;
	} else {
		// --- 初期化
		g_Bmpimage[e_waku1].bmp_dispflg = 0 ;
		g_Bmpimage[e_waku1].bmp_exp = 0 ;
	}
	if ( go_Panel.m_ptopno[P2] ) {
		g_Bmpimage[e_waku2].bmp_dispflg = 1 ;
		g_Bmpimage[e_waku2].bmp_pos = g_Modelpos[e_player2] ;	// --- 2P 座標値
		g_Bmpimage[e_waku2].bmp_pos.y += 50 ;					// --- 2P y座標値調整
		g_Bmpimage[e_waku2].bmp_angle = 0 ;						// --- 2P 角度
		g_Bmpimage[e_waku2].bmp_exp = 100 ;						// --- 2P 大きさ
		// --- 中心座標
		g_Bmpimage[e_waku2].bmp_xy[0] = 0.5f  ;
		g_Bmpimage[e_waku2].bmp_xy[1] = 0.5f  ;
	} else {
		// --- 初期化
		g_Bmpimage[e_waku2].bmp_dispflg = 0 ;
		g_Bmpimage[e_waku2].bmp_exp = 0 ;
	}
	if ( g_SceneNo != e_Stage1_Square ) {
		// --- 初期化
		g_Bmpimage[e_waku1].bmp_dispflg = 0 ;
		g_Bmpimage[e_waku1].bmp_exp = 0 ;
		g_Bmpimage[e_waku2].bmp_dispflg = 0 ;
		g_Bmpimage[e_waku2].bmp_exp = 0 ;
	}

	// --- ボムの影表示
	if ( go_Enemy.m_BActionNo == e_bomfall ) {
		g_Bmpimage[e_bomshadow].bmp_dispflg = 1 ;		// --- 表示フラグ
		g_Bmpimage[e_bomshadow].bmp_pos = VGet(g_Modelpos[e_bomb].x, 0.0f, g_Modelpos[e_bomb].z) ;
		g_Bmpimage[e_bomshadow].bmp_angle = 0 ;			// --- 角度
		g_Bmpimage[e_bomshadow].bmp_exp = 70 ;			// --- 大きさ
		g_Bmpimage[e_bomshadow].bmp_xy[0] = 0.5f  ;
		g_Bmpimage[e_bomshadow].bmp_xy[1] = 0.5f  ;
	} else {
		g_Bmpimage[e_bomshadow].bmp_dispflg = 0 ;		// --- 非表示フラグ
	}

}



/* ------------------------------------ */
/*										*/
/*			選択アクション				*/
/*										*/
/* ------------------------------------ */
void  EffectAction::EffectSelect( char arg_PlayerNo )
{
	m_efflg = 0 ;

	// --- 表示の初期化
	g_Bmpimage[e_hitef].bmp_dispflg = 0 ;		// --- 表示無
	g_Bmpimage[e_hitef + 1].bmp_dispflg = 0 ;	// --- 表示無


	// --- どちらに当たったか
	if ( arg_PlayerNo == e_player1 ) {
		// --- 1Pの場合
		m_efflg = 1 ;
	} else if ( arg_PlayerNo == e_player2 ) {
		// --- 2Pの場合
		m_efflg = 2 ;
	}

}



/* ------------------------------------ */
/*										*/
/*			プレイヤー前描画			*/
/*										*/
/* ------------------------------------ */
void EffectAction::EffectDrawP( )
{
	// --- 大砲雲エフェクト
	for( int i = e_cloudef ; i < e_Eend ; i++ ) {
		if ( g_Bmpimage[i].bmp_dispflg ) {
 			DrawBillboard3D( g_Bmpimage[i].bmp_pos, g_Bmpimage[i].bmp_xy[0], g_Bmpimage[i].bmp_xy[1],
								(float)g_Bmpimage[i].bmp_exp, g_Bmpimage[i].bmp_angle,
								g_Bmpimage[e_cloudef].bmp_handle, TRUE ) ;
		}
	}

	// --- バナー表示
	if ( g_Bmpimage[e_PBanner1].bmp_dispflg == 0 ) {
		g_Bmpimage[e_PBanner1].bmp_dispflg = 1 ;		// --- 表示有
		g_Bmpimage[e_PBanner1].bmp_pos.x = 10.0f ;		// --- X値
		g_Bmpimage[e_PBanner1].bmp_pos.y = 10.0f ;		// --- Y値
		g_Bmpimage[e_PBanner2].bmp_dispflg = 1 ;		// --- 表示有
		g_Bmpimage[e_PBanner2].bmp_pos.x = (float)(WINDOW_W - 460) ;		// --- X値
		g_Bmpimage[e_PBanner2].bmp_pos.y = 10.0f ;		// --- Y値
		g_Bmpimage[e_TBanner].bmp_dispflg = 1 ;			// --- 表示有
		g_Bmpimage[e_TBanner].bmp_pos.x = (float)((WINDOW_W / 2) - 200) ;	// --- X値
		g_Bmpimage[e_TBanner].bmp_pos.y = 10.0f ;		// --- Y値
		g_Bmpimage[e_1P].bmp_dispflg = 1 ;
		g_Bmpimage[e_1P].bmp_angle = 0 ;
		g_Bmpimage[e_1P].bmp_exp = 100 ;
		g_Bmpimage[e_1P].bmp_xy[0] = 0.5f ;
		g_Bmpimage[e_1P].bmp_xy[1] = 0.5f ;
		g_Bmpimage[e_2P].bmp_dispflg = 1 ;
		g_Bmpimage[e_2P].bmp_angle = 0 ;
		g_Bmpimage[e_2P].bmp_exp = 100 ;
		g_Bmpimage[e_2P].bmp_xy[0] = 0.5f ;
		g_Bmpimage[e_2P].bmp_xy[1] = 0.5f ;
	}
	// --- バナー表示
	if ( g_SceneNo >= e_Stage1Init ) {
		for ( int bncnt = e_PBanner1 ; bncnt <= e_TBanner ; bncnt++ ) {
			if ( g_Bmpimage[bncnt].bmp_dispflg )
				 DrawGraph( (int)g_Bmpimage[bncnt].bmp_pos.x, (int)g_Bmpimage[bncnt].bmp_pos.y, 
				 g_Bmpimage[bncnt].bmp_handle, TRUE ) ;
		}
	}

	// --- 爆弾の影表示
	if ( g_Bmpimage[e_bomshadow].bmp_dispflg != 0 )
 		DrawBillboard3D( g_Bmpimage[e_bomshadow].bmp_pos, g_Bmpimage[e_bomshadow].bmp_xy[0], g_Bmpimage[e_bomshadow].bmp_xy[1],
							(float)g_Bmpimage[e_bomshadow].bmp_exp, g_Bmpimage[e_bomshadow].bmp_angle,
							g_Bmpimage[e_bomshadow].bmp_handle, TRUE ) ;

}



/* ------------------------------------ */
/*										*/
/*			  描画						*/
/*										*/
/* ------------------------------------ */
void EffectAction::EffectDraw( )
{
	// --- 画像ハンドル番号格納
	int hbmp ;
	int panelecnt = 0 ;


	for ( int ecnt = e_hitef ; ecnt < e_stanef ; ecnt++ ) {
		if ( ecnt < e_attackef )
			hbmp = e_hitef ;
		else if ( ecnt < e_stanef )
			hbmp = e_attackef ;
		// --- 画像描画
		if ( g_Bmpimage[ecnt].bmp_dispflg ) {
 			DrawBillboard3D( g_Bmpimage[ecnt].bmp_pos, g_Bmpimage[ecnt].bmp_xy[0], g_Bmpimage[ecnt].bmp_xy[1],
							 (float)g_Bmpimage[ecnt].bmp_exp, g_Bmpimage[ecnt].bmp_angle,
							 g_Bmpimage[hbmp].bmp_handle, TRUE ) ;
		}
	}

	// --- バナーキャラとプレイヤー表示
	g_Bmpimage[e_1P].bmp_pos = VAdd( g_Modelpos[e_player1], VGet(5.0f,250.0f,0.0f) ) ;
	g_Bmpimage[e_2P].bmp_pos = VAdd( g_Modelpos[e_player2], VGet(5.0f,250.0f,0.0f) ) ;
	if ( g_SceneNo >= e_Stage1Init ) {
		DrawExtendGraph(  20, 10, 150, 150, g_Bmpimage[m_playerbmp[P1]].bmp_handle, TRUE ) ;
		DrawExtendGraph(  1750, 10, 1900, 150, g_Bmpimage[m_playerbmp[P2]].bmp_handle, TRUE ) ;
 		DrawBillboard3D( g_Bmpimage[e_1P].bmp_pos, g_Bmpimage[e_1P].bmp_xy[0], g_Bmpimage[e_1P].bmp_xy[1],
							(float)g_Bmpimage[e_1P].bmp_exp, g_Bmpimage[e_1P].bmp_angle,
							g_Bmpimage[e_1P].bmp_handle, TRUE ) ;
 		DrawBillboard3D( g_Bmpimage[e_2P].bmp_pos, g_Bmpimage[e_2P].bmp_xy[0], g_Bmpimage[e_2P].bmp_xy[1],
							(float)g_Bmpimage[e_2P].bmp_exp, g_Bmpimage[e_2P].bmp_angle,
							g_Bmpimage[e_2P].bmp_handle, TRUE ) ;
	}

	// --- スタンエフェクト表示
	for ( int i = e_stanef ; i < e_stanef + 2 ; i++ ) {
		if ( g_Bmpimage[i].bmp_dispflg != 0 )
 			DrawBillboard3D( g_Bmpimage[i].bmp_pos, g_Bmpimage[i].bmp_xy[0], g_Bmpimage[i].bmp_xy[1],
								(float)g_Bmpimage[i].bmp_exp, g_Bmpimage[i].bmp_angle,
								g_Bmpimage[e_stanef].bmp_handle, TRUE ) ;
	}
	// --- パネルの影表示
	if ( g_Bmpimage[e_waku1].bmp_dispflg != 0 )
 		DrawBillboard3D( g_Bmpimage[e_waku1].bmp_pos, g_Bmpimage[e_waku1].bmp_xy[0], g_Bmpimage[e_waku1].bmp_xy[1],
							(float)g_Bmpimage[e_waku1].bmp_exp, g_Bmpimage[e_waku1].bmp_angle,
							g_Bmpimage[e_waku1].bmp_handle, TRUE ) ;
	if ( g_Bmpimage[e_waku2].bmp_dispflg != 0 )
 		DrawBillboard3D( g_Bmpimage[e_waku2].bmp_pos, g_Bmpimage[e_waku2].bmp_xy[0], g_Bmpimage[e_waku2].bmp_xy[1],
							(float)g_Bmpimage[e_waku2].bmp_exp, g_Bmpimage[e_waku2].bmp_angle,
							g_Bmpimage[e_waku1].bmp_handle, TRUE ) ;



	// --- レディゴーの表示
	if ( (g_SceneNo == e_Stage1Ready) && (g_Bmpimage[e_kettei].bmp_dispflg) ) {
		DrawBillboard3D( g_Bmpimage[e_kettei].bmp_pos, g_Bmpimage[e_kettei].bmp_xy[0], g_Bmpimage[e_kettei].bmp_xy[1],
							(float)g_Bmpimage[e_kettei].bmp_exp, g_Bmpimage[e_kettei].bmp_angle,
							g_Bmpimage[e_kettei].bmp_handle, TRUE ) ;
	}

}


