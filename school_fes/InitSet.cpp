
/* ==================================================================================

		初期セット

+ -----------ファイルの概要----------------------------------------------------------
		モデルの読み込みや変数の初期化

+ =================================================================================== */

#include "Common.h"

int InitSet()
{
	int return_no = 0 ;		// 返値入れ

	// ==初期化== //
	for ( int i = 0 ; i < e_end ; i++ ){
		g_Loadmodel[i] = 0 ;		// --- モデルデータ	
		g_Dispflg[i] = 0 ;			// --- 表示フラグ
	}
	for ( int i = 0 ; i < ep_end ; i++ ) {
		g_Animmodel[P1][i] = 0 ;		// --- アニメーション
		g_Animmodel[P2][i] = 0 ;		// --- アニメーション
	}

	// ==フォント読み込み== //
	AddFontResourceEx( "Images/851MkPOP_002.ttf",FR_PRIVATE, NULL ) ;

	// ==画像読み込み== //
	g_Bmpimage[e_backGB].bmp_handle		= LoadGraph( "Images/Title_Image.bmp" ) ;		// --- タイトル背景画像
	g_Bmpimage[e_titlogo].bmp_handle	= LoadGraph( "Images/Title_Logo.png" ) ;		// --- タイトルロゴ画像
	g_Bmpimage[e_titstr].bmp_handle		= LoadGraph( "Images/moji.png" ) ;				// --- タイトル文字画像
	g_Bmpimage[e_selectGB].bmp_handle	= LoadGraph( "Images/SelectGazou.bmp" ) ;		// --- 背景画像
	g_Bmpimage[e_screenGB].bmp_handle	= LoadGraph( "Images/WhiteColor.png" ) ;		// --- ステージ切り替え画像	
	g_Bmpimage[e_1PWin].bmp_handle		= LoadGraph( "Images/1PWin.png" ) ;				// --- 1PWIN画像
	g_Bmpimage[e_2PWin].bmp_handle		= LoadGraph( "Images/2PWIN.png" ) ;				// --- 2PWIN画像
	g_Bmpimage[e_Lose].bmp_handle		= LoadGraph( "Images/Lose.png" ) ;				// --- LOSE画像
	g_Bmpimage[e_hitef].bmp_handle		= LoadGraph( "Images/Hit.bmp" ) ;				// --- ヒットエフェクト	
	g_Bmpimage[e_cloudef].bmp_handle	= LoadGraph( "Images/Smoke.bmp" ) ;				// --- 煙エフェクト
	g_Bmpimage[e_stanef].bmp_handle		= LoadGraph( "Images/stan.bmp" ) ;				// --- スタンエフェクト
	g_Bmpimage[e_PSele1].bmp_handle		= LoadGraph( "Images/Icon_Robot1.bmp" ) ;		// --- セレクト用キャラ１画像
	g_Bmpimage[e_PSele2].bmp_handle		= LoadGraph( "Images/Icon_Robot2.bmp" ) ;		// --- セレクト用キャラ２画像
	g_Bmpimage[e_PSele3].bmp_handle		= LoadGraph( "Images/Icon_Robot3.bmp" ) ;		// --- セレクト用キャラ３画像
	g_Bmpimage[e_PSele4].bmp_handle		= LoadGraph( "Images/Icon_Robot4.bmp" ) ;		// --- セレクト用キャラ４画像
	g_Bmpimage[e_waku1].bmp_handle		= LoadGraph( "Images/waku1.png" ) ;				// --- キャラセレクトの枠	
	g_Bmpimage[e_waku2].bmp_handle		= LoadGraph( "Images/waku2.png" ) ;				// --- キャラセレクトの枠
	g_Bmpimage[e_kettei].bmp_handle		= LoadGraph( "Images/kettei.png" ) ;			// --- 確認画面
	g_Bmpimage[e_waku3].bmp_handle		= LoadGraph( "Images/waku3.png" ) ;				// --- キャラセレクトの枠	
	g_Bmpimage[e_waku4].bmp_handle		= LoadGraph( "Images/waku4.png" ) ;				// --- キャラセレクトの枠
	g_Bmpimage[e_PBanner1].bmp_handle	= LoadGraph( "Images/banner1.png" ) ;			// --- プレイヤーバナー１
	g_Bmpimage[e_PBanner2].bmp_handle	= LoadGraph( "Images/banner2.png" ) ;			// --- プレイヤーバナー２
	g_Bmpimage[e_TBanner].bmp_handle	= LoadGraph( "Images/timerbox.png" ) ;			// --- タイマーバナー
	g_Bmpimage[e_1P].bmp_handle	= LoadGraph( "Images/1P.png" ) ;						// --- プレイヤー１Ｐ
	g_Bmpimage[e_2P].bmp_handle	= LoadGraph( "Images/2P.png" ) ;						// --- プレイヤー２Ｐ
	g_Bmpimage[e_Nextst].bmp_handle		= LoadGraph( "Images/Nextstage.png" ) ;			// --- ステージ移動
	g_Bmpimage[e_bomshadow].bmp_handle	= LoadGraph( "Images/shadow.png" ) ;			// --- 爆弾の影


	// ==モデル読み込み== //
	g_Loadmodel[e_recode]	= MV1LoadModel( "Models/Stage/Recode.mv1" ) ;		// --- レコード

	// == サウンド == //
	g_soundDT[e_Btitle]			= LoadSoundMem( "Sound/retropark.mp3" ) ;		// --- タイトルBGM
	g_soundDT[e_Bselect]		= LoadSoundMem( "Sound/sanjinooyatsu.mp3" ) ;	// --- セレクトBGM
	g_soundDT[e_Bfirst]			= LoadSoundMem( "Sound/happytime.mp3" ) ;		// --- 第一ステージのBGM
	g_soundDT[e_Bsecound]		= LoadSoundMem( "Sound/wintercarnival.mp3" ) ;	// --- 第二ステージのBGM
	g_soundDT[e_Sready]			= LoadSoundMem( "Sound/labo/ready.mp3" ) ;		// --- スタートSE
	g_soundDT[e_Sgo]			= LoadSoundMem( "Sound/labo/go.mp3" ) ;			// --- スタートSE2
	g_soundDT[e_Scheers]		= LoadSoundMem( "Sound/labo/cheers.mp3" ) ;		// --- 歓声SE

	g_soundDT[e_Sattack]		= LoadSoundMem( "Sound/labo/Attack.mp3" ) ;		// --- 攻撃SE
	g_soundDT[e_Sdamage]		= LoadSoundMem( "Sound/damage.wav" ) ;			// --- ダメージSE
	g_soundDT[e_Skettei]		= LoadSoundMem( "Sound/kettei.wav" ) ;			// --- 決定SE
	g_soundDT[e_Scancel]		= LoadSoundMem( "Sound/cancel.wav" ) ;			// --- キャンセルSE
	g_soundDT[e_Sleftright]		= LoadSoundMem( "Sound/leftright.wav" ) ;		// --- 左右キーSE
	g_soundDT[e_Sslow]			= LoadSoundMem( "Sound/slow.wav" ) ;			// --- パネルSE
	g_soundDT[e_Swaring]		= LoadSoundMem( "Sound/onjin/keihou.mp3" ) ;	// --- 警報音SE
	g_soundDT[e_Schange]		= LoadSoundMem( "Sound/onjin/stagechange.mp3" ) ;	// --- ステージ切り替えSE
	g_soundDT[e_Sresult]		= LoadSoundMem( "Sound/Kdoramu.wav" ) ;			// --- 結果発表SE
	g_soundDT[e_Sresult + 1]	= LoadSoundMem( "Sound/Kdoramu02.wav" ) ;		// --- 結果発表SE2
	g_soundDT[e_Scoin]			= LoadSoundMem( "Sound/coin.wav" ) ;			// --- コインSE
	g_soundDT[e_Sbakuha]		= LoadSoundMem( "Sound/Kdown.wav" ) ;			// --- ダウンSE
	g_soundDT[e_Sbakuha + 1]	= LoadSoundMem( "Sound/bakuha.wav" ) ;			// --- 爆発SE
	g_soundDT[e_Scannon]		= LoadSoundMem( "Sound/cannon.wav" ) ;			// --- 大砲砲撃音SE
	g_soundDT[e_Ssbutton]		= LoadSoundMem( "Sound/start.wav" ) ;			// --- スタートボタンSE
	g_soundDT[e_Sfinish]		= LoadSoundMem( "Sound/whistle.wav" ) ;			// --- 終了SE
	

	// --- モデルが入っているか確認
	for ( int i = 0 ; i < e_end ; i++ ) {
		if ( g_Loadmodel[i] == -1)
			return_no = -1 ;	// --- エラー終了値
	}

	// ==アニメーション読み込み== //
	go_Effect.InitSet() ;	// --- エフェクト
	return return_no ;

}

/* ------------------------------------ */
/*										*/
/*			  モデル読み込み			*/
/*										*/
/* ------------------------------------ */
void ModelInit()
{
	switch ( g_SceneNo ) {
		// --- 初期セット
		case e_SceneBlank :
			// --- キャラ読み込み
			go_TitChaSet.m_Pmdldt[0] = MV1LoadModel( "Models/Player/Robot1_1.mv1" ) ;	// --- ロボット１
			go_TitChaSet.m_Pmdldt[1] = MV1LoadModel( "Models/Player/Robot2_1.mv1" ) ;	// --- ロボット２
			go_TitChaSet.m_Pmdldt[2] = MV1LoadModel( "Models/Player/Robot1_2.mv1" ) ;	// --- ロボット３
			go_TitChaSet.m_Pmdldt[3] = MV1LoadModel( "Models/Player/Robot2_2.mv1" ) ;	// --- ロボット４
			break ;

		// --- セレクト画面初期セット
		case e_SelectInit :
			// --- ステージ読み込み
			g_Loadmodel[e_stage]	= MV1LoadModel( "Models/Stage/Stage01.mv1" ) ;	// --- ステージ１
			g_Loadmodel[e_stage2]	= MV1LoadModel( "Models/Stage/Stage02_7.mv1" ) ;	// --- ステージ2
			g_Loadmodel[e_table]	= MV1LoadModel( "Models/Stage/RoomTukue.mv1" ) ;	// --- 机
			go_Stage.InitSet() ;			// --- ステージ
			break ;

		// ---  操作説明
		case e_Explanation :
			// --- エネミー・アイテム読み込み
			g_Loadmodel[e_enemy]	= MV1LoadModel( "Models/Enemy/BomEnemy.mv1" ) ;		// --- エネミー
			g_Loadmodel[e_bomb]		= MV1LoadModel( "Models/Enemy/Bom.mv1" ) ;			// --- ボム
			g_Loadmodel[e_explo]	= MV1LoadModel( "Models/Enemy/Explotion.mv1" ) ;	// --- 爆発
			// --- アイテム読み込み
			g_Loadmodel[e_cannon]	= MV1LoadModel( "Models/Cannon/Cannon.mv1" ) ;		// --- 大砲
			g_Loadmodel[e_bullet]	= MV1LoadModel( "Models/Cannon/Bullet.mv1" ) ;		// --- 弾
			g_Loadmodel[e_panel]	= MV1LoadModel( "Models/Item/Panel.mv1" ) ;			// --- パネル
			MV1SetScale( g_Loadmodel[e_cannon], VGet(0.5f , 0.5f , 0.5f) ) ;			// --- 大砲大きさ 
			
			go_Enemy.InitSet() ;			// --- エネミー
			go_Enemy.EneAnimInit() ;		// --- エネミーアニメ
			go_Cannon.InitSet( ) ;	// --- 大砲
			go_Cannon.CannonAnim() ;		// --- 大砲
			go_Panel.InitSet() ;			// --- パネル
			break ;

		// --- ステージ２初期セット 
		case e_Stage2Init :
			// --- コイン読み込み
			g_Loadmodel[e_coin]		= MV1LoadModel( "Models/Item/coin1.mv1" ) ;			// --- コイン
			go_Coin.InitSet() ;		// --- コイン
			break ;

	}

}
