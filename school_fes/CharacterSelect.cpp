
/* ==================================================================================

		タイトル・セレクトのアクション

+ -----------ファイルの概要----------------------------------------------------------

		タイトル時の表示やセレクト画面での処理

+ =================================================================================== */

#include "Common.h"

// --- シーン用enum
enum SceneSele {
	CharInit,		// --- 0:セレクト初期セット
	CharSele,		// --- 1:セレクト中
	CharEnd,		// --- 2:セレクトエンド
	VariInit,		// --- 3:確認初期セット
	VariSele,		// --- 4:確認中
	VariEnd			// --- 5:確認エンド
} ;

// --- 画像用enum
enum selenum{
	pre1,		// --- プレイヤー
	pre2,
	pre3,
	pre4,
	wak1,		// --- 枠
	wak2,
	wak3,
	wak4
} ;

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
TitleCharaSet::TitleCharaSet() {

}



/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
TitleCharaSet::~TitleCharaSet() {

}



/* ------------------------------------ */
/*										*/
/*		　セレクト時の初期セット		*/
/*										*/
/* ------------------------------------ */
void TitleCharaSet::CharSelInit() {

	m_imagenum = 0 ;		// --- 画像用の配列ナンバー
	m_image_W = 300 ;		// --- 画像の横幅
	m_image_H = 300 ;		// --- 画像の縦幅

	m_seltrg = 0 ;

	// --- 選択したかのフラグ
	m_selectcnt[P1] = 0 ;
	m_selectcnt[P2] = 0 ;
	m_Decisionflg[P1] = 0 ;
	m_Decisionflg[P2] = 0 ;
	m_Finalflg[P1] = 0 ;
	m_Finalflg[P2] = 0 ;

	// --- タイマー
	m_stagetime = 500 ;

	// --- キャラセレクト用画像セット
	for ( int i = e_PSele1 ; i < e_waku1 ; i++ )
	{
		g_Bmpimage[i].bmp_dispflg = 1 ;
		m_imagepos[m_imagenum].x = 90 + (480 * (m_imagenum)) ;
		m_imagepos[m_imagenum].y = 670 ;
		m_imagenum++ ;
	}

	// --- キャラ選択枠
	for ( int i = 0 ; i < 2 ; i++ ){
		g_Bmpimage[e_waku1 + i].bmp_dispflg = 1 ;
		m_imagepos[m_imagenum].x = 90 ;
		m_imagepos[m_imagenum].y = 670 ;
		m_imagenum++ ;
	}

	// --- 確認画面
	g_Bmpimage[e_kettei].bmp_dispflg = 0 ;

	printf( "%d\n", m_imagenum ) ;
	// --- 確認選択枠
	for ( int i = 0 ; i < 2 ; i++ ){
		g_Bmpimage[e_waku3 + i].bmp_dispflg = 0 ;
		m_imagepos[m_imagenum].x = 970 ;
		m_imagepos[m_imagenum].y = 610 ;
		m_imagenum++ ;
	}
	printf( "%d\n", m_imagenum ) ;

	// --- 最終決定前のトリガー
	m_endtrg = 0 ;
	// --- アニメーション用
	m_playtime[P1] = -1 ;

	// --- セレクト操作の音量
	ChangeVolumeSoundMem( 155 , g_soundDT[e_Sleftright] ) ;
	ChangeVolumeSoundMem( 155 , g_soundDT[e_Skettei] ) ;

	// --- シーンセット
	m_sceneno  = CharInit ;
}



/* ------------------------------------ */
/*										*/
/*		　	セレクト時の処理			*/
/*										*/
/* ------------------------------------ */
int TitleCharaSet::CharSel() {
	int return_no = 0 ;

	switch ( m_sceneno ){
		// --- キャラ選択初期
		case CharInit :
			// --- 次のシーンへ
			g_Dispflg[e_player1] = 0 ;		// --- 表示フラグ
			g_Dispflg[e_player2] = 0 ;		// --- 表示フラグ

			// --- モデル、カメラの座標
			g_Modelpos[e_player1].x = -200.0f ;
			g_Modelpos[e_player1].z = -500.0f ;
			g_Modelpos[e_player2].x =  200.0f ;
			g_Modelpos[e_player2].z = -500.0f ;
			g_Camerapos.y = 300 ;
			g_Cameratgt.y = 250 ;
			SetCameraPositionAndTargetAndUpVec( g_Camerapos, g_Cameratgt, VGet(0.0f,0.0f,1.0f) ) ;

			g_Dispflg[e_recode]  = 1 ;		// --- 表示有

			m_keyflg[P1] = GetJoypadInputState(DX_INPUT_KEY_PAD1) ;
			m_keyflg[P2] = GetJoypadInputState(DX_INPUT_PAD2) ;

			m_imagenum = 0 ;

			// --- 何も押されていなければ次へ
			if ( (m_keyflg[P1] == 0) && (m_keyflg[P2] == 0) )
				m_sceneno = CharSele ;		// --- 次のシーンへ
			break ;

		// --- キャラ選択中
		case CharSele :
			//// --- スペースキーでキャラセレクトスキップ
			//if ( GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_10 ) {
			//	g_Loadmodel[e_player1] = MV1DuplicateModel( m_Pmdldt[pre2] ) ;
			//	g_Loadmodel[e_player2] = MV1DuplicateModel( m_Pmdldt[pre4] ) ;
			//	m_sceneno = VariEnd ;
			//}

			// --- 1Playerの選択 ---------------------------------------------------------
			if ( m_Decisionflg[P1] == 0 )
			{
				// --- カーソル左移動
				if ( (GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_LEFT) && (m_selectcnt[P1] != 0) ) {				
					// --- キー押下かつ一番左でない
					if ( (m_seltrg & 0x01) == 0x00 ){
						m_seltrg |= 0x01 ;
						// --- 左右十字キー音
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;
						// --- カーソル移動
						m_selectcnt[P1]	-= 1  ;
					}
				}
				else{
					m_seltrg &= 0xfe ;
				}

				// --- カーソル右移動
				if ( (GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_RIGHT) && (m_selectcnt[P1] != 3) ) {
					// --- 一番右にいたとき
					if ( (m_seltrg & 0x02) == 0x00 ){
						m_seltrg |= 0x02 ;
						// --- 左右十字キー音
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;
						// --- カーソル移動
						m_selectcnt[P1]	+= 1 ;
					}
				}
				else{
					m_seltrg &= 0xfd ;
				}

				// --- キャラの決定(プレイヤー１)
				// 5：S　→　B：○
				if ( GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_B ) {
					// --- 押下時のみ
					if ( (m_seltrg & 0x04) == 0x00 ){
						m_seltrg |= 0x04 ;

						switch ( m_selectcnt[P1] )
						{
							// --- ふつうロボット
							case 0 :
								g_Loadmodel[e_player1] = MV1DuplicateModel( m_Pmdldt[pre1] ) ;
								go_Effect.m_playerbmp[P1] = e_PSele1 ;
						// -- 決定音
						PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;	
								break ;

							// --- かわいいロボット
							case 1 :
								g_Loadmodel[e_player1] = MV1DuplicateModel( m_Pmdldt[pre2] ) ;
								go_Effect.m_playerbmp[P1] = e_PSele2 ;
						// -- 決定音
						PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;	
								break ;

							case 2 :
								g_Loadmodel[e_player1] = MV1DuplicateModel( m_Pmdldt[pre3] ) ;
								go_Effect.m_playerbmp[P1] = e_PSele3 ;
						// -- 決定音
						PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;	
								break ;

							case 3 :
								g_Loadmodel[e_player1] = MV1DuplicateModel( m_Pmdldt[pre4] ) ;
								go_Effect.m_playerbmp[P1] = e_PSele4 ;
						// -- 決定音
						PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;	
								break ;

							default :
								m_selectcnt[P1] = 0 ;
								break ;
						}
						g_Dispflg[e_player1] = 1 ;		// --- 表示フラグ
						m_Decisionflg[P1] = 1 ;			// --- 決定されたかどうか
					}

				}
				else{
					m_seltrg &= 0xfb ;
				}
			}

			// --- 2Playerの選択 ----------------------------------------------------
			if ( m_Decisionflg[P2] == 0 )
			{
				// --- カーソル左移動
				if ( (GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_LEFT) && (m_selectcnt[P2] != 0) ) {
					// --- キー押下かつ一番左でない
					if ( (m_seltrg & 0x10) == 0x00 ){
						m_seltrg |= 0x10 ;
						// --- 左右十字キー音
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;
						// --- カーソル移動
						m_selectcnt[P2]	-= 1  ;
					}
				}
				else{
					m_seltrg &= 0xef ;
				}

				// --- カーソル右移動
				if ( (GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_RIGHT) && (m_selectcnt[P2] != 3) ) {
					// --- キー押下かつ一番右でない
					if ( (m_seltrg & 0x20) == 0x00 ){
						m_seltrg |= 0x20 ;
						// --- 左右十字キー音
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;
						// --- カーソル移動
						m_selectcnt[P2]	+= 1 ;
					}
				}
				else{
					m_seltrg &= 0xdf ;
				}

				// --- キャラの決定(プレイヤー２)
				// 6：D　→　B：○
				if ( GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_B ) {
					// --- 押下時のみ
					if ( (m_seltrg & 0x40) == 0x00 ){
						m_seltrg |= 0x40 ;

						// -- 決定音
						PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;

						switch ( m_selectcnt[P2] )
						{
							// --- ふつうロボット
							case 0 :
								g_Loadmodel[e_player2] = MV1DuplicateModel( m_Pmdldt[pre1] ) ;
								go_Effect.m_playerbmp[P2] = e_PSele1 ;
								break ;

							// --- かわいいロボット
							case 1 :
								g_Loadmodel[e_player2] = MV1DuplicateModel( m_Pmdldt[pre2] ) ;
								go_Effect.m_playerbmp[P2] = e_PSele2 ;
								break ;

							case 2 :
								g_Loadmodel[e_player2] = MV1DuplicateModel( m_Pmdldt[pre3] ) ;
								go_Effect.m_playerbmp[P2] = e_PSele3 ;
								break ;

							case 3 :
								g_Loadmodel[e_player2] = MV1DuplicateModel( m_Pmdldt[pre4] ) ;
								go_Effect.m_playerbmp[P2] = e_PSele4 ;
								break ;

							default :
								m_selectcnt[P1] = 0 ;
								break ;
						}
						g_Dispflg[e_player2] = 1 ;		// --- 表示フラグ
						m_Decisionflg[P2] = 1 ;
					}
				}
				else {
					m_seltrg &= 0xbf ;
				}
			}

			// --- キャラ選択解除 -----------------------------------------------------------------
			if ( m_Decisionflg[P1] == 1 )
			{
				if ( GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_C ) {
					// --- 押下時のみ
					if ( (m_seltrg & 0x08) == 0x00 ){
						m_seltrg |= 0x08 ;
						// -- キャンセル音
						ChangeVolumeSoundMem( 155, g_soundDT[e_Scancel] ) ;
						PlaySoundMem( g_soundDT[e_Scancel],  DX_PLAYTYPE_BACK, TRUE ) ;

						m_Decisionflg[P1] = 0 ;
						g_Dispflg[e_player1] = 0 ;		// --- 表示フラグ
					}

				}
				else{
					m_seltrg &= 0xf7 ;
				}
			}
			if ( m_Decisionflg[P2] == 1 )
			{
				if ( GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_C ) {
					// --- 押下時のみ
					if ( (m_seltrg & 0x80) == 0x00 ){
						m_seltrg |= 0x80 ;
						// -- キャンセル音
						PlaySoundMem( g_soundDT[e_Scancel],  DX_PLAYTYPE_BACK, TRUE ) ;
					
						m_Decisionflg[P2] = 0 ;
						g_Dispflg[e_player2] = 0 ;		// --- 表示フラグ
					}
				}
				else {
					m_seltrg &= 0x7f ;
				}
			}

			// --- 表示位置の代入
			m_imagepos[wak1] = m_imagepos[m_selectcnt[P1]] ;
			m_imagepos[wak2] = m_imagepos[m_selectcnt[P2]] ;

			// --- 次のシーンへ
			if ( (m_Decisionflg[P1]) && (m_Decisionflg[P2]) ) {
				m_stagetime = 500 ;
				m_endtrg = 1 ;	// --- トリガー立て
				m_sceneno = CharEnd ;
			}
			break ;

		// --- キャラ選択後
		case CharEnd :
			m_seltrg = 0x00 ;	// --- せれくとトリガー
			m_stagetime -= 30 ;

			// --- 次のシーンへ
			if ( m_stagetime < 0 )
				m_sceneno = VariInit ;
			break ;

		// --- 確認初期
		case VariInit :
			g_Bmpimage[e_kettei].bmp_dispflg = 1 ;
			g_Bmpimage[e_waku3].bmp_dispflg = 1 ;
			g_Bmpimage[e_waku4].bmp_dispflg = 1 ;
			m_Decisionflg[P1] = 0 ;
			m_Decisionflg[P2] = 0 ;
			m_Finalflg[P1] = 0 ;
			m_Finalflg[P2] = 0 ;

			m_keyflg[P1] = GetJoypadInputState(DX_INPUT_KEY_PAD1) ;
			m_keyflg[P2] = GetJoypadInputState(DX_INPUT_PAD2) ;

			// --- 何も押されていなければ次へ
			if ( (m_keyflg[P1] == 0) && (m_keyflg[P2] == 0) )
				m_sceneno = VariSele ;			// --- 次のシーンへ
			break ;

		// --- 確認中
		case VariSele :
			// --- 1Playerの選択 --------------------------------------------------------------
			if ( !(m_Decisionflg[P1]) ) {
				if ( GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_LEFT ) {
					// --- キー押下かつ一番左でない
					if ( (m_seltrg & 0x01) == 0x00 ){
						m_seltrg |= 0x01 ;
						// --- 左右十字キー音	
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;

						m_imagepos[wak3].x = 540 ;
						m_Finalflg[P1] = 1 ;		// --- はい
					}
				}
				else{
					m_seltrg &= 0xfe ;
				}
				if ( GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_RIGHT ) {
					// --- 一番右にいたとき
					if ( (m_seltrg & 0x02) == 0x00 ) {
						m_seltrg |= 0x02 ;
						// --- 左右十字キー音	
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;

						m_imagepos[wak3].x = 970 ;
						m_Finalflg[P1] = 0 ;		// --- いいえ
					}
				}
				else{
					m_seltrg &= 0xfd ;
				}
			}

			// --- 2Playerの選択 -----------------------------------------------------------
			if ( !(m_Decisionflg[P2]) ) {
				if ( (GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_LEFT) ) {
					// --- キー押下かつ一番左でない
					if ( (m_seltrg & 0x10) == 0x00 ){
						m_seltrg |= 0x10 ;
						// --- 左右十字キー音	
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;

						m_imagepos[wak4].x = 540 ;
						m_Finalflg[P2] = 1 ;		// --- はい
					}
				}
				else {
					m_seltrg &= 0xef ;
				}
				if ( GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_RIGHT ) {
					// --- キー押下かつ一番右でない
					if ( (m_seltrg & 0x20) == 0x00 ){
						m_seltrg |= 0x20 ;
						// --- 左右十字キー音	
						PlaySoundMem( g_soundDT[e_Sleftright],  DX_PLAYTYPE_BACK, TRUE ) ;

						m_imagepos[wak4].x = 970 ;
						m_Finalflg[P2] = 0 ;		// --- いいえ
					}
				}
				else {
					m_seltrg &= 0xdf ;
				}
			}

			// --- プレイヤー１〇押下時 ------------------------------------------------
			if ( GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_B ){
				// --- 押下時のみ
				if ( (m_seltrg & 0x04) == 0x00 ){
					m_seltrg |= 0x04 ;

					// --- 決定音
					ChangeVolumeSoundMem( 155 , g_soundDT[e_Skettei] ) ;
					PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;
					// --- はいの時
					if ( m_Finalflg[P1] == 1 ){
						m_Decisionflg[P1] = 1 ;			// --- はい
					}
					// --- いいえの時
					if ( m_Finalflg[P1] == 0 ){ 
						CharSelInit() ;
					}
				}
			}
			else {
				m_seltrg &= 0xfb ;
			}


			// --- プレイヤー2〇押下時 ------------------------------------------------
			if ( GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_B ) {
				// --- 押下時のみ
				if ( (m_seltrg & 0x40) == 0x00 ){
					m_seltrg |= 0x40 ;
					// --- 決定音
					PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;

					// --- 決定音
					PlaySoundMem( g_soundDT[e_Skettei],  DX_PLAYTYPE_BACK, TRUE ) ;
					// --- はいの時
					if ( m_Finalflg[P2] == 1 ){
						m_Decisionflg[P2] = 1 ;			// --- はい
					}
					// --- いいえの時
					if ( m_Finalflg[P2] == 0 ){ 
						CharSelInit() ;
					}
				}
			}
			else {
				m_seltrg &= 0xbf ;
			}

			// --- どちらかがxを押下
			if ( ( (GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_C) && (m_Decisionflg[P1] == 2) ) 
			  || ( (GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_C)     && (m_Decisionflg[P2] == 2) ) ){
				// --- キャンセル音
				PlaySoundMem( g_soundDT[e_Scancel],  DX_PLAYTYPE_BACK, TRUE ) ;

				CharSelInit() ;
			}

			// --- 次のシーンへ
			if ( (m_Decisionflg[P1] == 1) && (m_Decisionflg[P2] == 1) )
				m_sceneno = VariEnd ;

			break ;

		// --- 確認後
		case VariEnd :
			return_no = 1 ;
			// --- キャラセレ終わり!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			break ;
	}

	CharAnim() ;

	return return_no ;
}



/* ------------------------------------ */
/*										*/
/*		セレクト時のアニメーション		*/
/*										*/
/* ------------------------------------ */
void TitleCharaSet::CharAnim() {
	if ( g_SceneNo < e_Stage1Init ) {
		// --- アニメーションセット
		if ( (g_Dispflg[e_player1]) && (m_playtime[P1] == -1) ) {
			m_animmodel = MV1LoadModel( "Models/Player/RobotA_Wait.mv1" ) ;
			m_attachno[P1] = MV1AttachAnim( g_Loadmodel[e_player1], 0, m_animmodel ) ;
			m_totalanimtime[P1] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player1], m_attachno[P1] ) ;
			m_playtime[P1] = 0.0f ;
			go_1Player.m_animno = ep_Wait ;
		}
		if ( (g_Dispflg[e_player2]) && (m_playtime[P2] == -1) ) {
			m_animmodel = MV1LoadModel( "Models/Player/RobotA_Wait.mv1" ) ;
			m_attachno[P2] = MV1AttachAnim( g_Loadmodel[e_player2], 0, m_animmodel ) ;
			m_totalanimtime[P2] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player2], m_attachno[P2] ) ;
			m_playtime[P2] = 0.0f ;
			go_2Player.m_animno = ep_Wait ;
		}

		// --- アニメーション進行
		if ( g_Dispflg[e_player1] ) {
			m_playtime[P1] += 1.0f ;
			if ( m_playtime[P1] > m_totalanimtime[P1] )
				m_playtime[P1] = 0.0f ;
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[P1], m_playtime[P1] ) ;
		} else {
			MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[P1] ) ;
			m_playtime[P1] = -1 ;
		}
		if ( g_Dispflg[e_player2] ) {
			m_playtime[P2] += 1.0f ;
			if ( m_playtime[P2] > m_totalanimtime[P2] )
				m_playtime[P2] = 0.0f ;
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player2], m_attachno[P2], m_playtime[P2] ) ;
		} else {
			MV1DetachAnim( g_Loadmodel[e_player2], m_attachno[P2] ) ;
			m_playtime[P2] = -1 ;
		}
	} else {
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[P1] ) ;
		MV1DetachAnim( g_Loadmodel[e_player2], m_attachno[P2] ) ;
	}
	
	
}


