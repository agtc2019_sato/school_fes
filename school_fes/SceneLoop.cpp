
/* ==================================================================================

		シーンループ

+ -----------ファイルの概要----------------------------------------------------------
		シーンの遷移を行う

+ =================================================================================== */

#include "Common.h"

void SceneLoop()
{
	switch ( g_SceneNo )
	{
		// --- ブランク -------------------------------------
		case e_SceneBlank :
			// --- モデルの読み込み
			ModelInit() ;

			// -- タイトルBGM再生
			ChangeVolumeSoundMem( 175 , g_soundDT[e_Btitle] ) ;
			PlaySoundMem( g_soundDT[e_Btitle],  DX_PLAYTYPE_LOOP, TRUE ) ;	// --- バッググラウンド再生

			g_Bmpimage[e_backGB].bmp_dispflg   = 1 ;	// --- 切替画像
			g_Bmpimage[e_titstr].bmp_dispflg   = 1 ;	// --- 切替画像
			g_Bmpimage[e_titlogo].bmp_dispflg  = 1 ;	// --- タイトルに描画する文字
			g_titsize	= 1.0f ;

			g_SceneNo = e_title ;		// --- 次のシーンへ
			break ;


		// --- タイトル -------------------------------------
		case e_title :
			// --- タイトル拡縮
			if ( g_pmflg == 0 ){
				g_titsize += 0.005f ;
				if ( g_titsize >= 1.3f )
					g_pmflg = 1 ;
			}
			else{
				g_titsize -= 0.005f ;
				if ( g_titsize <= 1.0f )
					g_pmflg = 0 ;
			}

			// --- スペースキーで移動	STARTボタンは10→Mで
			if ( ((GetJoypadInputState(DX_INPUT_KEY_PAD1) & PAD_INPUT_10) || (GetJoypadInputState(DX_INPUT_PAD2) & PAD_INPUT_10))&& ( g_nxtcnt == 0 ) ){
				// --- スタートボタン音量
				ChangeVolumeSoundMem( 155 , g_soundDT[e_Ssbutton] ) ;
		
				// --- スタートボタン効果音再生
				PlaySoundMem( g_soundDT[e_Ssbutton],  DX_PLAYTYPE_BACK, TRUE ) ;
				g_titcnt = 300 ;
				g_nxtcnt = 1 ;
			}

			if ( (g_titcnt <= 0) && (g_nxtcnt != 0) ){
				g_titcnt = 300 ;
				g_nxtcnt += 1 ;
				g_Bmpimage[e_titstr].bmp_dispflg ^= 1 ;
			}
			else{
				g_titcnt -= 30 ;
			}

			if ( g_nxtcnt >= 7 ){
				g_Bmpimage[e_screenGB].bmp_dispflg = 1 ;

				go_TitChaSet.m_fadecnt = 0 ;	// --- フェードのカウント
				g_SceneNo = e_SelectInit ;		// --- 次のシーンへ
			}
			break ;


		// --- キャラセレクト初期セット ---------------------
		case e_SelectInit :
			// --- レコードモデルの設定
			g_Modelpos[e_recode] = VGet( 0.0f , 200.0f , 100.0f ) ;	
			// --- 角度　向き
			MV1SetRotationXYZ(g_Loadmodel[e_recode], VGet(1.3f, 1.57f * 2 , 0.0f)) ;


			// --- 画像とモデルの表示設定
			go_TitChaSet.m_fadecnt += 5 ;	// --- フェードカウントの開始
			if ( go_TitChaSet.m_fadecnt >= 255 ) {
				// --- タイトルBGMを停止
				StopSoundMem( g_soundDT[e_Btitle] ) ;

				// --- モデルの読み込み
				ModelInit() ;

				g_Bmpimage[e_backGB].bmp_dispflg   = 0 ;	// --- 切替画像
				g_Bmpimage[e_titstr].bmp_dispflg   = 0 ;	// --- タイトルに描画するロゴ
				g_Bmpimage[e_titlogo].bmp_dispflg  = 0 ;	// --- タイトルに描画する文字
				g_Bmpimage[e_screenGB].bmp_dispflg = 1 ;	// --- 切替画像
				go_TitChaSet.CharSelInit() ;
				g_Dispflg[e_player1] = 1 ;
				g_Dispflg[e_player2] = 1 ;

				// --- セレクトBGM再生
				ChangeVolumeSoundMem( 175 , g_soundDT[e_Bselect] ) ;
				PlaySoundMem( g_soundDT[e_Bselect], DX_PLAYTYPE_LOOP, TRUE ) ;
				g_SceneNo = e_Select ;		// --- 次のシーンへ
			}

			break ;


		// --- キャラセレクト画面 ---------------------------
		case e_Select :
			if ( go_TitChaSet.m_fadecnt <= 0 ) {
				// --- キャラクターを選択したら進む
				if ( go_TitChaSet.CharSel() ) {
					// --- 切替画像用意
					g_Bmpimage[e_screenGB].bmp_handle = LoadGraph( "Images/switch_screen.png" ) ;
					g_Bmpimage[e_screenGB].bmp_xy[0] = 0.0f ;
					g_Bmpimage[e_screenGB].bmp_xy[1] = 1500.0f ;
					g_Bmpimage[e_screenGB].bmp_dispflg = 1 ;
					g_SceneNo = e_Explanation ;		// --- 次のシーンへ
				}
			} else {
				go_TitChaSet.m_fadecnt -= 5 ;		// --- フェードインカウント
			}

			go_Stage.Action() ;						// --- レコード回転アクション
			break ;


		// --- 操作説明 -------------------------------------
		case e_Explanation :
			g_Camerapos.y = 0 ;
			g_Cameratgt.y = 0 ;

			// --- シーン切替のアニメーション
			if ( g_Bmpimage[e_screenGB].bmp_dispflg != 0 )
			{
				if (g_Bmpimage[e_screenGB].bmp_xy[1] > -400.0f) {
					g_Bmpimage[e_screenGB].bmp_xy[1] -= 10.0f ;
					go_Stage.Action() ;				// --- レコード回転アクション
					go_TitChaSet.CharAnim() ;		// --- セレクト時にアニメーション
				} else {
					// --- モデルの読み込み
					ModelInit() ;
					g_Dispflg[e_player1] = 0 ;
					g_Dispflg[e_player2] = 0 ;
					g_Bmpimage[e_waku1].bmp_dispflg = 0 ;
					g_Bmpimage[e_waku2].bmp_dispflg = 0 ;
					g_Bmpimage[e_kettei].bmp_dispflg = 0 ;
					MV1SetScale( g_Loadmodel[e_player1] , VGet(0.5f,0.5f,0.5f) ) ;
					MV1SetScale( g_Loadmodel[e_player2] , VGet(0.5f,0.5f,0.5f) ) ;
					// ==プレイヤーアニメ== //
					go_1Player.PAnimInit() ;
					go_2Player.PAnimInit() ;
					// --ここにキャラのポジションをセット-- //
					go_1Player.m_pos.x = -400.0f ;
					go_2Player.m_pos.x = 400.0f ;
					g_SceneNo = e_Stage1Init ;		// --- 次のシーンへ
				}
			}
			break ;


		// --- ステージ1初期セット --------------------------
		case e_Stage1Init :
			// --- セレクトBGM音を止める
			StopSoundMem( g_soundDT[e_Bselect] ) ;

			// --- レコード回転終了
			MV1DetachAnim( g_Loadmodel[e_recode] ,go_Stage.m_Attachidx ) ;
			g_Dispflg[e_recode]  = 0 ;

			// --- 表示フラグ立て
			g_Dispflg[e_stage] = 1 ;
			g_Dispflg[e_stage2] = 1 ;
			g_Dispflg[e_player1] = 1 ;
			g_Dispflg[e_player2] = 1 ;
			g_Dispflg[e_enemy]	= 1 ;
			g_Dispflg[e_table] = 1 ;
			go_TitChaSet.CharAnim() ;		// --- アニメーション止め

			// --- ステージポジション
			g_Modelpos[e_stage] = VGet(0.0f, STAGE2POS_Y, 0.0f) ;
			g_Modelpos[e_stage2] = VGet(3000.0f, 200.0f , 0.0f) ;
			g_Modelpos[e_table] = VGet(3000.0f, -50.0f , 1000.0f) ;
			g_Modelpos[e_player1] = go_1Player.m_pos ;
			g_Modelpos[e_player2] = go_2Player.m_pos ;

			// --- 時間セット
			g_Timercnt = MAXTIME * 2 ;
			GetLocalTime(&g_LocalTime[0]) ;
			g_LocalTime[1].wSecond = g_LocalTime[0].wSecond ;

			// --- 切り替え画像上げ
			if ( g_Bmpimage[e_screenGB].bmp_xy[1] > -2100 ) {
				g_Bmpimage[e_screenGB].bmp_xy[1] -= 10 ;
			}
			else{
				g_stflg = 1 ;
				go_Stage.InitSet() ;

				// --- 新しい画像設定
				g_Bmpimage[e_waku1].bmp_handle = LoadGraph( "Images/panel.png" ) ;
				g_Bmpimage[e_kettei].bmp_handle = LoadGraph( "Images/ready.png" ) ;
				g_Bmpimage[e_kettei].bmp_dispflg = 1 ;		// --- 表示有
				g_Bmpimage[e_kettei].bmp_angle = 0 ;
				g_Bmpimage[e_kettei].bmp_exp = 1000 ;
				g_Bmpimage[e_kettei].bmp_pos = VGet(0.0f,0.0f,0.0f) ;
				g_Bmpimage[e_kettei].bmp_xy[0] = 0.5f ;
				g_Bmpimage[e_kettei].bmp_xy[1] = 0.5f ;
				// --- 切替画像用意
				g_Bmpimage[e_screenGB].bmp_handle = LoadGraph( "Images/switch_screen1.png" ) ;
				g_Bmpimage[e_screenGB].bmp_xy[0] = -3100 ;
				g_Bmpimage[e_screenGB].bmp_xy[1] = 0 ;
				g_Bmpimage[e_screenGB].bmp_dispflg = 0 ;
				go_Panel.RandomPos() ;	// --- パネルランダム位置
				// -- 第一ステージBGM再生
				ChangeVolumeSoundMem( 155 , g_soundDT[e_Bfirst] ) ;
				PlaySoundMem( g_soundDT[e_Bfirst],  DX_PLAYTYPE_LOOP, TRUE ) ;
				// --- 次のシーンへ
				g_SceneNo = e_Stage1Ready ;
				// --- スタートの効果音
				ChangeVolumeSoundMem( 175  , g_soundDT[e_Sready] ) ;
				PlaySoundMem( g_soundDT[e_Sready],  DX_PLAYTYPE_BACK, TRUE ) ;
			}

			g_Camerapos = VGet( 0.0f, 1000.0f, -3500.0f ) ;			// --- カメラポジション
			SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;
			break ;

			
		// --- ステージ１レディゴー画面 ---------------------------
		case e_Stage1Ready :
			if ( (g_readygoflg == true) && (g_Camerapos.y <= CAMERAPOS_Y) && (g_Camerapos.z >= CAMERAPOS_Z) )	{
				// --- ゴーの効果音
				ChangeVolumeSoundMem( 175  , g_soundDT[e_Sgo] ) ;
				PlaySoundMem( g_soundDT[e_Sgo],  DX_PLAYTYPE_NORMAL, TRUE ) ;
				g_Bmpimage[e_kettei].bmp_dispflg = 0 ;		// --- 表示無
				// --- 次のシーンへ
				g_SceneNo = e_Stage1_Square ;
			} else {
				// --- カメラポジション
				if ( g_Camerapos.y > CAMERAPOS_Y )
					g_Camerapos.y -= 12.0f ;
				if ( g_Camerapos.z < CAMERAPOS_Z )
					g_Camerapos.z += 29.0f ;
				SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;

				// --- 再生中か
				if ( !CheckSoundMem(g_soundDT[e_Sready]) ) {
					if ( (g_Camerapos.y <= CAMERAPOS_Y) && (g_Camerapos.z >= CAMERAPOS_Z) ) {
						g_Bmpimage[e_kettei].bmp_handle = LoadGraph( "Images/GO.png" ) ;
						g_readygoflg = true ;
					}
				} else {
					g_Bmpimage[e_kettei].bmp_exp -= 2 ;
				}
			}
			break ;


		// --- ステージ1 四角 -------------------------------
		case e_Stage1_Square :
			DeleteSoundMem( g_soundDT[e_Sready] ) ;
			DeleteSoundMem( g_soundDT[e_Sgo] ) ;

			GetLocalTime(&g_LocalTime[0]) ;
			// --- タイマーセット
			if ( (g_LocalTime[0].wSecond != g_LocalTime[1].wSecond) && (CheckSoundMem( g_soundDT[e_Swaring] ) == 0) )
			{
				g_LocalTime[1].wSecond = g_LocalTime[0].wSecond ;
				if ( g_Timercnt > MAXTIME ) {
					g_Timercnt-- ;			// --- ４５から引いていく
				}
			}

			// --- ステージ１処理
			if ( g_Timercnt > MAXTIME ) {
				go_1Player.GetKeydata(GetJoypadInputState(DX_INPUT_KEY_PAD1)) ;
				go_1Player.Action() ;
				go_2Player.GetKeydata(GetJoypadInputState(DX_INPUT_PAD2)) ;
				go_2Player.Action() ;
				go_Cannon.Action() ;
				go_Panel.Action( ) ;
				go_Enemy.Action() ;
				go_Effect.Action( ) ;
			} else if ( g_Timercnt == MAXTIME ) {
				// --- 第二ステージへの変更時間

				// --- 警報音がなっているかチェック
				if ( CheckSoundMem( g_soundDT[e_Swaring] ) == 0 )
				{
					if ( go_Stage.m_keihoFlg == 0 )
					{
						// --- 警報音
						ChangeVolumeSoundMem( 155  , g_soundDT[e_Swaring] ) ;		
						PlaySoundMem( g_soundDT[e_Swaring],  DX_PLAYTYPE_BACK, TRUE ) ;
					}
					go_Stage.m_keihoFlg = 1 ;
				}
				// --- 警報音が終了
				if ( CheckSoundMem( g_soundDT[e_Swaring] ) == 0 )
				{
					// --- 表示物の初期化
					for ( int i = 0 ; i < go_Cannon.m_xcnt ; i++ )
						go_Cannon.CannonRest(i) ;						// --- 大砲の初期化
					for ( int icnt = 0 ; icnt < CANNON_MAX ; icnt++ )
						g_Bmpimage[e_cloudef + icnt].bmp_dispflg = 0 ;	// --- エフェクト消し
					// --- エフェクト消し
					g_Bmpimage[e_hitef].bmp_dispflg = 0 ;
					g_Bmpimage[e_hitef + 1].bmp_dispflg = 0 ;
					g_Bmpimage[e_waku1].bmp_dispflg = 0 ;
					g_Bmpimage[e_waku2].bmp_dispflg = 0 ;
					g_Bmpimage[e_stanef].bmp_dispflg = 0 ;
					g_Bmpimage[e_stanef + 1].bmp_dispflg = 0 ;
					g_Dispflg[e_panel]	 = 0 ;		// --- パネル非表示
					g_Dispflg[e_enemy]	 = 0 ;		// --- エネミーの表示フラグ
					g_Dispflg[e_bomb]	 = 0 ;		// --- ボムの表示フラグ
					g_Dispflg[e_explo]	 = 0 ;		// --- 爆発の表示フラグ
					g_Dispflg[e_player1] = 1 ;		// --- プレイヤー１表示
					g_Dispflg[e_player2] = 1 ;		// --- プレイヤー２表示
					// --- 移動のアニメーションセット
					go_Stage.Action( ) ;			// --- レコード横スライド
				}
			}

			// --- ステージ移動横画像
			if ( g_Bmpimage[e_screenGB].bmp_dispflg != 0 )
			{
				if (g_Bmpimage[e_screenGB].bmp_xy[0] < -600.0) {
					g_Bmpimage[e_screenGB].bmp_xy[0] += 30.0 ;
				}
				else{
					g_stflg = 2 ;					// --- 第二ステージフラグ
					g_timeflg = FALSE ;				
					go_Stage.InitSet() ;
						
					// --- 場面切り替え効果音
					PlaySoundMem( g_soundDT[e_Schange],  DX_PLAYTYPE_BACK, TRUE ) ;
					g_SceneNo = e_Stage2Init ;		// --- 次のシーンへ
				}
			}
			break ;

		// --- ステージ2初期セット --------------------------
		case e_Stage2Init :
			ChangeVolumeSoundMem( 155 , g_soundDT[e_Bfirst] ) ;

			MV1DetachAnim( g_Loadmodel[e_stage] ,go_Stage.m_Rc_Attach[0] ) ;
			// --- 第二ステージ位置調整
			g_Modelpos[e_stage] = VGet(-3000.0f , STAGE2POS_Y - 50 , 0.0f) ;
			g_Modelpos[e_stage2] = VGet(0.0f, STAGE2POS_Y - 50.0f , 0.0f) ;
			g_Modelpos[e_table] = VGet(0.0f , -200.0f , 1000.0f) ;
			go_1Player.Stage2Init() ;	// --- プレイヤーたちの初期位置セット

			g_Timercnt = MAXTIME ;		// --- ４５から引いていく

			// --- モデルの読み込み
			ModelInit() ;

			// --- 切り替え画像横
			if ( g_Bmpimage[e_screenGB].bmp_xy[0] < WINDOW_W ) {
				g_Bmpimage[e_screenGB].bmp_xy[0] += 20 ;
			} else {
				g_Bmpimage[e_screenGB].bmp_dispflg = 0 ;
				// --- 次のシーンへ
				g_SceneNo = e_Stage2_Circle ;
			}

			g_Camerapos = VGet( 0.0f, 1000.0f, -4000.0f ) ;			// --- カメラポジション
			SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;
			break ;

		// --- ステージ2 丸 ---------------------------------
		case e_Stage2_Circle :

			go_Stage.Action( ) ;					// --- レコード落下
			
			// --- カメラ接近
			if ( g_Camerapos.y > CAMERAPOS_Y  )
				g_Camerapos.y -= 5.0f ;
			if ( g_Camerapos.z < CAMERAPOS_Z - 200.0f )
				g_Camerapos.z += 17.0f ;
			g_Cameratgt.z = -120.0f ;
			SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;

			if ( (g_Camerapos.y <= CAMERAPOS_Y ) && (g_Camerapos.z >= CAMERAPOS_Z - 200.0f) )
			{
				if ( g_timeflg == TRUE )
				{
					g_Dispflg[e_enemy] = 1 ;
					go_1Player.GetKeydata(GetJoypadInputState(DX_INPUT_KEY_PAD1)) ;
					go_1Player.Action() ;
					go_2Player.GetKeydata(GetJoypadInputState(DX_INPUT_PAD2)) ;
					go_2Player.Action() ;
					go_Cannon.Action() ;
					go_Coin.Action( ) ;
					go_Enemy.Action() ;
					go_Effect.Action( ) ;
					GetLocalTime(&g_LocalTime[0]) ;

					// --- タイマーセット
					if ( g_LocalTime[0].wSecond != g_LocalTime[1].wSecond ) {
						g_LocalTime[1].wSecond = g_LocalTime[0].wSecond ;

						// --- 45秒カウントが0だったら
						if ( g_Timercnt == 0 ) {
							// --- ステージ切り替え画像	
							g_Bmpimage[e_screenGB].bmp_handle = LoadGraph( "Images/WhiteColor.png" ) ;
							g_Bmpimage[e_screenGB].bmp_dispflg = 1 ;
							go_TitChaSet.m_fadecnt = 0 ;	// --- フェードのカウント
							// --- フィニッシュ画像表示
							g_Bmpimage[e_waku4].bmp_dispflg = 1 ;
							g_Bmpimage[e_waku4].bmp_handle = LoadGraph( "Images/finish.png" ) ;
							g_Bmpimage[e_waku4].bmp_angle = 0 ;
							g_Bmpimage[e_waku4].bmp_exp = 1000 ;
							g_Bmpimage[e_waku4].bmp_pos = VGet(0.0f,0.0f,0.0f) ;
							g_Bmpimage[e_waku4].bmp_xy[0] = 0.5f ;
							g_Bmpimage[e_waku4].bmp_xy[1] = 0.5f ;
							// --- 次のシーンへ
							g_SceneNo = e_Stage2Fine ;
						} else {
							g_Timercnt-- ;			// --- ４５から引いていく
						}
					}
				}
			}
			break ;

		case e_Stage2Fine :
			// --- 終了合図効果音
			ChangeVolumeSoundMem( 155 , g_soundDT[e_Sfinish] ) ;
			PlaySoundMem( g_soundDT[e_Sfinish] , DX_PLAYTYPE_NORMAL , TRUE ) ;
			// --- 次のシーンへ
			g_SceneNo = e_ResultInit ;
			break ;


		// --- リザルト初期セット ---------------------------
		case e_ResultInit :
			// --- 第二ステージBGM音を下げる
			ChangeVolumeSoundMem( 125 , g_soundDT[e_Bsecound] ) ;

			// --- 第二ステージの位置調整
			g_Modelpos[e_stage2] = VGet(0.0f, STAGE2POS_Y - 50 , 0.0f) ;

			go_TitChaSet.m_fadecnt += 5 ;
			if ( go_TitChaSet.m_fadecnt >= 255 ) {
				// --- 大砲の初期化
				for ( int i = 0 ; i < go_Cannon.m_xcnt ; i++ )
					go_Cannon.CannonRest(i) ;
				g_Dispflg[e_player1] = 0 ;
				g_Dispflg[e_player2] = 0 ;
				g_Bmpimage[e_TBanner].bmp_dispflg = 0 ;
				go_TitChaSet.m_fadecnt = 255 ;

				// --- プレイヤーの最後のアニメーションをDetachする
				MV1DetachAnim( g_Loadmodel[e_player1], go_1Player.m_attachno[go_1Player.m_animno] ) ;	// --- アニメーション解除
				MV1DetachAnim( g_Loadmodel[e_player2], go_2Player.m_attachno[go_2Player.m_animno] ) ;	// --- アニメーション解除
				// --- プレイヤーの向く方向を正面にする
				MV1SetRotationXYZ(g_Loadmodel[e_player1], VGet(0.0f, 1.57f * 4 , 0.0f)) ;
				MV1SetRotationXYZ(g_Loadmodel[e_player2], VGet(0.0f, 1.57f * 4 , 0.0f)) ;

				go_Result.InitSet() ;		// --- リザルト処理の初期設定

				// --- 結果発表ドラム音
				PlaySoundMem( g_soundDT[e_Sresult],  DX_PLAYTYPE_LOOP, TRUE ) ;

				// --- カメラポジション
				g_Camerapos	= VGet( 0.0f, 350.0f, -1100.0f ) ;
				SetCameraPositionAndTargetAndUpVec(g_Camerapos, g_Cameratgt,VGet(0.0f,0.0f,1.0f)) ;

				if ( go_Result.m_playercoin )
					g_Modelpos[e_coin] = VGet( -500.0f, 350.0f, 0.0f ) ; // --- １Ｐ側
				else
					g_Modelpos[e_coin] = VGet( 500.0f, 350.0f, 0.0f ) ; // --- ２Ｐ側
		

				// --- 次のシーンへ
				g_SceneNo = e_Result ;
			}
			break ;


		// --- リザルト中 -----------------------------------
		case e_Result :
			g_Bmpimage[e_waku4].bmp_dispflg = 0 ;	// --- 表示無
			go_TitChaSet.m_fadecnt -= 5 ;
			if ( go_TitChaSet.m_fadecnt <= 0 ) {
				go_TitChaSet.m_fadecnt = 0 ;
			}

			go_Result.Action() ;
			break ;


		// --- デフォルト -----------------------------------
		default :
			break ;
	}
}


