
/* ==================================================================================

		リザルトのアクション

+ -----------ファイルの概要----------------------------------------------------------
		リザルトのクラス

+ =================================================================================== */

class ResultAction : BasicObj
{
	public :
		ResultAction() ;
		~ResultAction() ;
		void InitSet() override ;			// --- 初期化
		void Action() override ;			// --- アクション

		void BmpInit() ;					// --- WINLOSE画像の初期設定

		void AnimInit()	;					// --- アニメーションセット		
		void AnimPlay() ;					// --- アニメーション

		void PlayerRotation() ;				// --- 1P回転

		void ResultWinAnim() ;				// --- 勝利アニメーション
		void ResultLoseAnim() ;				// --- 敗北アニメーション

		BOOL m_Resultflg ;					// --- リザルトフラグ
		
		VECTOR m_Rtpos[2] ;					// --- 座標一時格納	要素数 0 : 1P   1 : 2P
		VECTOR m_Rtrotation[2] ;			// --- 回転値 要素数 0 : 1P   1 : 2P

		int m_Rtcount ;						// --- 回転したカウント

		int m_winBmp ;						// --- Win画像要素数
		int m_loseBmp ;						// --- Lose画像要素数
		
		int m_Imagesize ;					// --- サイズ 1 : 加算・ 2 : 減算
		
		int Wplyer ;						// --- 勝利したプレイヤーの要素数
		int Lplyer ;						// --- 敗北したプレイヤーの要素数

		bool m_playercoin ;


		// 要素数 0 : 勝利アニメーション 　 //
		// 要素数 1 : 敗北アニメーション	//
		int m_Rt_AnimData[5] ;					// --- アニメーションデータ
		int m_Rt_Attach[5] ;					// --- アニメーションとの紐づけ
		float m_Rt_Totaltime[5] ;				// --- アニメーションの総時間
		float m_Rt_Playtime[5] ;				// --- アニメーションの経過時間

		// 回転移動で必要な変数 //
		double m_Square[2][3] ;				// --- ２乗			要素数 第二要素数 0 : 1P   1 : 2P
		double m_radius[2] ;				// --- 半径			要素数 0 : 1P   1 : 2P
		double m_angle[2][2] ;				// --- 角度計算		要素数 0 : 1P   1 : 2P

		int scorememo[2] ;					// --- スコア記録



	private :

} ;

