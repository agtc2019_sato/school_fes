
/* ==================================================================================

		ステージのアクション

+ -----------ファイルの概要----------------------------------------------------------
		ステージのクラス

+ =================================================================================== */

class StageAction : BasicObj
{
	public :
		StageAction() ;
		~StageAction() ;

		void InitSet() override ;		// --- 初期セット
		void Action() override ;		// --- アクション

		int		m_Rootflm ;				// --- ルートフレーム情報
		int		m_Attachidx ;			// --- アニメーションの紐づけに使用
		float	m_Anim_totaltime ;		// --- アニメーションの総時間
		float	m_Playtime ;			// --- アニメーションの経過時間

		int		m_Rc_AnimDt[5] ;		// --- レコードアニメーションデータ
		int		m_Rc_Attach[5] ;		// --- レコードアニメーションの紐づけ
		float	m_Rc_Totaltime[5] ;		// --- レコードアニメーション総時間
		float	m_Rc_Playtime[5] ;		// --- レコードアニメーションの経過時間

		BOOL	m_keihoFlg ;			// --- 警報音SEフラグ

	private :
		float m_stgcmrpos ;	// --- カメラポジションX

} ;
