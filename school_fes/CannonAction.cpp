
/* ==================================================================================

		大砲のアクション

+ -----------ファイルの概要----------------------------------------------------------
		大砲のクラス		継承元 → BasicObjクラス

+ =================================================================================== */

#include "Common.h"

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
CannonAction::CannonAction() {
	

}

/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
CannonAction::~CannonAction() {

}

/* ------------------------------------ */
/*										*/
/*			  初期セット				*/
/*										*/
/* ------------------------------------ */
void CannonAction::InitSet() {
	
	m_CA_randflg = 0 ;									// --- ランダムフラグ
	m_CA_Setposi.CA_angle = 0.7f ;						// --- 円の角度
	m_CA_Setposi.CA_radius = 700 ;						// --- 円の半径
	m_CA_Setposi.CA_xp = 0 ;							// --- 大砲の配置x座標
	m_CA_Setposi.CA_yp = 0 ;							// --- 大砲の配置y座標

	m_CenterPosi = VGet( 0.0f , 0.0f , 0.0f ) ;			// --- 中心座標
	
	m_CActionNo = 0 ;									// --- アクションナンバー
	m_ShafulFlg = 0 ;									// --- ポジションシャッフルフラグ

	// --- モデルデータを格納
	for ( m_xcnt = 0 ; m_xcnt < 4  ; m_xcnt++ )
	{
		g_Loadmodel[e_cannon + m_xcnt + 1] = MV1DuplicateModel( g_Loadmodel[e_cannon] ) ;		// --- 大砲のモデルコピー		
			
		g_Loadmodel[e_bullet + m_xcnt + 1] = MV1DuplicateModel( g_Loadmodel[e_bullet] ) ;		// --- 弾のモデルコピー	
	}

	for ( m_xcnt = 0 ; m_xcnt < 5 ; m_xcnt++ )
	{		
		//MV1SetScale( g_Loadmodel[e_cannon + m_xcnt], VGet(0.5f , 0.5f , 0.5f) ) ;			// --- 大砲モデルの大きさ 
		m_CAsizeX[e_cannon + m_xcnt] = 0.0f ;
		m_CAsizeY[e_cannon + m_xcnt] = 0.0f ;
		m_CAsizeZ[e_cannon + m_xcnt] = 0.0f ;

		MV1SetScale( g_Loadmodel[e_cannon + m_xcnt], VGet(m_CAsizeX[e_cannon + m_xcnt] , 
					m_CAsizeY[e_cannon + m_xcnt] , m_CAsizeZ[e_cannon + m_xcnt]) ) ;			// --- 大砲モデルの大きさ 

		m_CA_flg[m_xcnt] = FALSE ;															// --- アニメーションフラグ

		m_CA_bulletflg[m_xcnt] = FALSE ;													// --- 弾の処理フラグ

		g_Dispflg[e_cannon + m_xcnt] = 0 ;													// --- 大砲非表示
	
		g_Dispflg[e_bullet + m_xcnt] = 0 ;													// --- 弾は非表示

		m_smemory = 0 ;																		// --- 秒数を記憶

		m_CA_radian[m_xcnt] = 0 ;															// --- 弾角度

	}
}

/* ------------------------------------ */
/*										*/
/*			 大砲関数まとめ				*/
/*										*/
/* ------------------------------------ */
void CannonAction::Action() {

	switch ( m_CActionNo )
	{
		// --- 大砲ポジションセット
		case 0 :			
			CannonPosition() ;
			
			if ( m_ShafulFlg == 1 )
			{
				CannonShaful() ;
				m_ShafulFlg = 0 ;
			}

			m_CActionNo = 1 ;
			break ;

		case 1 :
			CannonAnim() ;
			
			m_CActionNo = 0 ;
			break ;

		case 2 :

			m_CActionNo = 0 ;
			break ;
	}
}

/* ------------------------------------ */
/*										*/
/*			 大砲アニメーション			*/
/*										*/
/* ------------------------------------ */
void CannonAction::CannonAnim() {
	
	for ( m_xcnt = 0 ; m_xcnt < 5 ; m_xcnt++ )
	{
		if ( (m_CA_flg[m_xcnt] == FALSE) && (m_CA_bulletflg[m_xcnt] == FALSE) )
		{
			// --- アニメーション格納
			m_CA_atack = MV1LoadModel( "Models/Cannon/CannonA.mv1" ) ;
		
			// --- 大砲アニメをアタッチ
			m_CA_attach =MV1AttachAnim( g_Loadmodel[e_cannon + m_xcnt]  , 0 , m_CA_atack )  ;

			// --- 大砲アニメの時間を取得
			m_CA_totaltime[m_xcnt] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_cannon + m_xcnt] , m_CA_attach ) ;

			// --- アニメーションOK
			m_CA_flg[m_xcnt] = TRUE ;

			// --- アニメーションタイムの初期値
			m_CA_playtime[m_xcnt] = 0.0f ;
		}
		else if ( (m_CA_flg[m_xcnt] == TRUE) && (g_Dispflg[e_cannon + m_xcnt] == 1) )
		{

			// --- 大砲の拡大縮小
			if ( m_CAsizeX[e_cannon + m_xcnt] < 0.35f )
			{
				m_CAsizeX[e_cannon + m_xcnt] += 0.03f ;	
				m_CAsizeY[e_cannon + m_xcnt] += 0.03f ;		
				m_CAsizeZ[e_cannon + m_xcnt] += 0.03f ;
			}

			MV1SetScale( g_Loadmodel[e_cannon + m_xcnt], VGet(m_CAsizeX[e_cannon + m_xcnt] 
					, m_CAsizeY[e_cannon + m_xcnt] , m_CAsizeZ[e_cannon + m_xcnt]) ) ;

			// --- アニメーション進行
			m_CA_playtime[m_xcnt] += 0.5f ;

			// --- プレイ時間とトータル時間の比較
			if( m_CA_playtime[m_xcnt] > m_CA_totaltime[m_xcnt] )
			{

				MV1DetachAnim( g_Loadmodel[e_cannon + m_xcnt] , m_CA_attach ) ;
				m_CA_playtime[m_xcnt] = 0.0f ;
				m_CA_flg[m_xcnt] = FALSE ;
			}

			// --- アニメーションを進行させる
			MV1SetAttachAnimTime( g_Loadmodel[e_cannon + m_xcnt] , m_CA_attach , m_CA_playtime[m_xcnt] ) ;
		}
		else
		{
			m_CA_playtime[m_xcnt] = 0.0f ;
		}

		// --- アニメーション38秒後 弾を発射
		if ( (m_CA_playtime[m_xcnt] == 38.0f)  && (g_Dispflg[e_cannon + m_xcnt] == 1) )
		{	
			// --- 効果音音量
			ChangeVolumeSoundMem( 255 , g_soundDT[e_Scannon + m_xcnt] ) ;
			// --- 大砲の効果音
			PlaySoundMem( g_soundDT[e_Scannon + m_xcnt],  DX_PLAYTYPE_BACK, TRUE ) ;
			
			go_Effect.m_cloudFlg[m_xcnt] = TRUE ;
			m_CA_bulletflg[m_xcnt] = TRUE ;		// 弾の処理フラグ 
		}
			
		BulletMove() ;
	}
}

/* ------------------------------------ */
/*										*/
/*			 弾のモーション				*/
/*										*/
/* ------------------------------------ */
void CannonAction::BulletMove() {

	int blimit ;			// --- 弾が飛ぶ範囲

	if ( m_CA_bulletflg[m_xcnt] == TRUE )
	{
		g_Dispflg[e_bullet + m_xcnt] = 1 ;	// --- 弾描画フラグ

		// --- 弾モデルの大きさ 
		MV1SetScale( g_Loadmodel[e_bullet + m_xcnt] , VGet(0.3f,0.3f,0.3f) ) ;

		if ( m_CA_radian[m_xcnt] == 0 )
		{
			m_CA_radian[m_xcnt] = BulletRadian( g_Modelpos[e_cannon + m_xcnt].x , 
			g_Modelpos[e_cannon + m_xcnt].z , 0.0 , 0.0 ) ;			// --- 弾の向き
		}

		// --- 弾が進んでいく				
		g_Modelpos[e_bullet + m_xcnt].x += cos( m_CA_radian[m_xcnt] ) * (float)BULLET_SPD ;			
		g_Modelpos[e_bullet + m_xcnt].z += sin( m_CA_radian[m_xcnt] ) * (float)BULLET_SPD ;

		// --- 飛んでいく範囲
		if ( g_SceneNo == e_Stage1_Square )
		{
			blimit = FIRST_POSI_X + 200 ;
		}

		if ( g_SceneNo == e_Stage2_Circle )
		{
			blimit = FIRST_POSI_Y + 200 ;
		}

		if ( (g_Modelpos[e_bullet + m_xcnt].z < (blimit * -1)) || (g_Modelpos[e_bullet + m_xcnt].z >  blimit) 
				|| (g_Modelpos[e_bullet + m_xcnt].x < (blimit * -1)) || (g_Modelpos[e_bullet + m_xcnt].x > blimit) ) 
		{			
			m_CA_bulletflg[m_xcnt] = FALSE ;			// --- 弾の非表示

			CannonRest(m_xcnt) ;								// --- 値リセット	
		}
	}
	else
	{
		g_Dispflg[e_bullet + m_xcnt] = 0 ;									// --- 弾モデル非表示 
		g_Modelpos[e_bullet + m_xcnt] = g_Modelpos[e_cannon + m_xcnt] ;		// --- 弾の座標を戻す
	}
}

/* ------------------------------------ */
/*										*/
/*			 弾の角度を求める			*/
/*										*/
/* ------------------------------------ */
float CannonAction::BulletRadian( float arg_x1 , float arg_y1 , float arg_x2 , float arg_y2 ){

    float cos_suti ;
	float sin_suti ;
	float trudain   ;
	float reAnsewr ;

	// --- 第一ステージのターゲット座標
	if ( g_SceneNo == e_Stage1_Square )
	{
		switch ( (int)arg_y1 )
		{
			// --- y座標が600の大砲
			case FIRST_POSI_Y :
				// -- 下記の座標に向かって飛んでいく
				arg_y2 = -FIRST_POSI_Y ;
				arg_x2 = arg_x1 ;
				break ;

			// --- y座標が-600の大砲
			case -FIRST_POSI_Y :
				// -- 下記の座標に向かって飛んでいく
				arg_y2 = FIRST_POSI_Y + 50 ;
				arg_x2 = arg_x1 ;
				break ;
		}

		switch ( (int)arg_x1 )
		{
			// --- x座標が700の大砲
			case FIRST_POSI_X :
				// -- 下記の座標に向かって飛んでいく
				arg_y2 = arg_y1 ;
				arg_x2 = -1 * ( FIRST_POSI_X + 50 ) ;
				break ;

			// --- x座標が-700の大砲
			case -FIRST_POSI_X :
				// -- 下記の座標に向かって飛んでいく
				arg_y2 = arg_y1 ;
				arg_x2 = FIRST_POSI_X + 50 ;
				break ;
		}
	}

	cos_suti = arg_x2 - arg_x1 ;		//--- cosの角度
	sin_suti = arg_y2 - arg_y1 ;		//--- sinの角度

	if( cos_suti != 0 )
	{
		trudain =  sin_suti / cos_suti ;		//--- tanの角度
		//--- x座標が大きい場合
		if (arg_x1 < arg_x2) 
		{
			reAnsewr = atanf(trudain) ;
			return reAnsewr ;
		}
		//--- x座標が小さい場合は180度足す
		reAnsewr = atanf(trudain) + g_Pi ;
		return reAnsewr ;
	}

	//--- 同じx座標の場合の処理
	if ( arg_y1 < arg_y2 )
	{
		//--- ターゲットY座標が大きい場合は90度
		reAnsewr  = g_Pi / 2.0f ;
		return reAnsewr ;
	}

	//--- y座標のほうが小さい場合270度	
	reAnsewr = ( g_Pi * -1 ) / 2.0f ;
	return reAnsewr ;

}

/* ------------------------------------ */
/*										*/
/*		大砲の位置を求める				*/
/*										*/
/* ------------------------------------ */
void CannonAction::CannonPosition() {

//	float radium ;							// --- 角度
//	int angle_cnt ;							// --- 角度座標の要数
	int second ;							// --- 秒数

	m_blankcnt = 0 ;
	timer = time( NULL ) ;					// --- 現在時刻を取得
	local = localtime( &timer ) ;			// --- 地方時に変換
	second = local->tm_sec ;				// --- 秒数格納

	switch ( g_SceneNo )
	{
		case e_Stage1_Square :
			FirStagePosi() ;				// --- 第一ステージポジション		
			break ;

		case e_Stage2_Circle :
			SecoStagePosi( ) ;				// --- 第二ステージポジション	
			break ;
	}

	// --- 時間を利用するランダム
	srand( (unsigned)time(NULL) ) ;
	
	// --- 大砲を出す数
	m_cannon_put = rand() % 5 ;

	for ( int icnt = 0 ; icnt < 5 ; icnt++ )
	{
		if ( g_Dispflg[e_cannon + icnt] == 0 )
		{
			m_blank_can[m_blankcnt] = icnt ;		// --- 空きの要素数を格納
			m_blankcnt++ ;							// --- 空きの数
		}
	}


	// --- 4秒ごとに大砲を出す数は空きの大砲オブジェクトをチェック
	if ( (m_blankcnt >= (m_cannon_put + 1)) && ((second % 4) == 0) && (	m_smemory != second) )
	{
		m_smemory = second ;				// --- 秒数を記憶

		// --- 大砲のポジションセット
		switch ( m_cannon_put )
		{
			case 0 :

				if ( g_SceneNo == e_Stage1_Square )
				{
					g_Modelpos[e_cannon + m_blank_can[0]] = VGet( m_CA_xposi[0] , 70.0f , m_CA_yposi[0] ) ;
				}
				else if ( g_SceneNo == e_Stage2_Circle )
				{
					g_Modelpos[e_cannon + m_blank_can[0]] = VGet( m_CA_xposi[0] , 0.0f , m_CA_yposi[0] ) ;
				}
				break ;

			default :

				m_ShafulFlg = 1 ;
				break ;
		}
	}
}

/* ------------------------------------ */
/*										*/
/*		第一ステージのポジション		*/
/*			全パターンを格納			*/
/*										*/
/* ------------------------------------ */
void CannonAction::FirStagePosi( )
{
	// --- 第一ステージのポジション
	for ( int icnt = 0 ; icnt < 16 ; icnt++ )
	{
		if ( (icnt >= 0) && (icnt < 3) )
		{
			// --- 右
			m_CA_xposi[icnt] = -FIRST_POSI_X ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = ( 260 * (float)icnt ) - 240 ;

		}

		if ( (icnt >= 3) && (icnt < 7) )
		{
			// --- 左
			m_CA_xposi[icnt] = FIRST_POSI_X ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = ( 260 * (float)(icnt -3) ) - 400 ;

		}

		if ( (icnt >= 7) && (icnt < 11) )
		{
			// --- 上
			m_CA_xposi[icnt] = ( 280 * (float)(icnt - 7) ) - 400 ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = FIRST_POSI_Y ;

		}

		if ( (icnt >= 11) && (icnt < 16) )
		{
			// --- 下
			m_CA_xposi[icnt] = ( 280 * (float)(icnt - 11) ) - 530 ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = -1 * FIRST_POSI_Y ;
		}

		if ( icnt == 16 )
		{
			// --- 右下
			m_CA_xposi[icnt] = FIRST_POSI_X + 50 ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = -520 ;
		}

		if ( icnt == 17 )
		{
			// --- 左下
			m_CA_xposi[icnt] = -1 * ( FIRST_POSI_X + 50 ) ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = -520 ;
		}

		if ( icnt == 18 )
		{
			// --- 左上
			m_CA_xposi[icnt] = -1 * ( FIRST_POSI_X + 50 ) ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = 520 ;
		}

		if ( icnt == 19 )
		{
			// --- 右上
			m_CA_xposi[icnt] = FIRST_POSI_X + 50 ;

			g_Modelpos[e_cannon + icnt] .y = 50 ;
	
			m_CA_yposi[icnt] = 520 ;
		}
	}	
}

/* ------------------------------------ */
/*										*/
/*		第二ステージのポジション		*/
/*			全パターンを格納			*/
/*										*/
/* ------------------------------------ */
void CannonAction::SecoStagePosi( )
{
	float radium ;

	// --- 一定の角度ずつ分けた座標を取得
	for ( int angle_cnt = 0 ; angle_cnt < 8 ; angle_cnt++ )
	{
		radium = angle_cnt / CANNON_MAX * g_Pi * 2 + ( m_CA_Setposi.CA_angle * angle_cnt ) ;
		m_CA_xposi[angle_cnt] = m_CA_Setposi.CA_radius * cos( radium ) + m_CA_Setposi.CA_xp ;
		m_CA_yposi[angle_cnt] = m_CA_Setposi.CA_radius * sin( radium ) + m_CA_Setposi.CA_yp ;
		m_CA_yposi[angle_cnt] -= 110 ;
	}
}

/* ------------------------------------ */
/*										*/
/*		大砲の位置をシャッフル			*/
/*										*/
/* ------------------------------------ */
void CannonAction::CannonShaful( ) {

	int first_cnt ;
	int second_cnt ;
	int randsuti[8] ;

	// --- 時間を利用するランダム
	srand( (unsigned)time(NULL) ) ;

	while ( m_CA_randflg == 0 )
	{
		for ( first_cnt = 0 ; first_cnt < (m_cannon_put + 1) ; first_cnt++ )
		{
			// --- 位置をランダムで出す
			// --- 第一ステージの場合
			if ( g_SceneNo == e_Stage1_Square )
			{
				randsuti[first_cnt] = rand() % 16 ;
			}
			// --- 第二ステージの場合
			else if ( g_SceneNo == e_Stage2_Circle )
			{
				randsuti[first_cnt] = rand() % 8 ;
			}

			// --- 2回目から
			if ( first_cnt > 0 )
			{
				// --- 同じ位置になっていないかを確認
				for (  second_cnt = 0 ; second_cnt < first_cnt ; second_cnt++ )
				{
					// --- 同じだったらやり直し 違ったら続行
					if ( (randsuti[first_cnt] == randsuti[second_cnt]) )
					{
						first_cnt-- ;			// --- やり直し
						second_cnt += 100 ;		// --- second_cntのfor文から抜ける
					}
					else
					{
						m_CA_randflg = 1 ;
					}
				}
			}
		}
	}

	m_CA_randflg = 0 ;							// --- ランダムフラグ

	// --- モデルポジションのセット
	for ( first_cnt = 0 ; first_cnt < (m_cannon_put + 1) ; first_cnt++ )
	{
		g_Modelpos[e_cannon + m_blank_can[first_cnt]] = VGet( m_CA_xposi[randsuti[first_cnt]] , 40.0f , m_CA_yposi[randsuti[first_cnt]] ) ;

		g_Dispflg[e_cannon + m_blank_can[first_cnt]] = 1 ;

		// --- 弾のポジションの初期値
		g_Modelpos[e_bullet + m_blank_can[first_cnt]] = g_Modelpos[e_cannon + m_blank_can[first_cnt]] ;

		CannonRadian( m_blank_can[first_cnt] ) ;				// --- 大砲の角度
	}
}

/* ------------------------------------ */
/*										*/
/*		大砲の角度を求める				*/
/*										*/
/* ------------------------------------ */
void CannonAction::CannonRadian( int arg_cnt ) {

	// --- 大砲の角度を代入
	switch( g_SceneNo )
	{
		// --- 第一ステージの場合
		case e_Stage1_Square :
			// --- 左側
			if (g_Modelpos[e_cannon + arg_cnt].x == -FIRST_POSI_X )
			{
				MV1SetRotationXYZ( g_Loadmodel[e_cannon + arg_cnt] , VGet(0.0f,(float)-(DX_PI / 2.0f),0.0f) ) ;
				MV1SetRotationXYZ( g_Loadmodel[e_bullet + arg_cnt] , VGet(0.0f,(float)-(DX_PI / 2.0f),0.0f) ) ;
			}
			// --- 右側
			else if ( g_Modelpos[e_cannon + arg_cnt].x == FIRST_POSI_X )
			{
				MV1SetRotationXYZ( g_Loadmodel[e_cannon + arg_cnt] , VGet(0.0f,(float)DX_PI / 2.0f,0.0f) ) ;
				MV1SetRotationXYZ( g_Loadmodel[e_bullet + arg_cnt] , VGet(0.0f,(float)DX_PI / 2.0f,0.0f) ) ;
			}
			// --- 上側
			else if ( g_Modelpos[e_cannon + arg_cnt].z == -FIRST_POSI_Y )
			{
				MV1SetRotationXYZ( g_Loadmodel[e_cannon + arg_cnt] , VGet(0.0f,(float)DX_PI / 1.0f,0.0f) ) ;
				MV1SetRotationXYZ( g_Loadmodel[e_bullet + arg_cnt] , VGet(0.0f,(float)DX_PI / 1.0f,0.0f) ) ;
			}
			// --- 下側
			else if ( g_Modelpos[e_cannon + arg_cnt].z == FIRST_POSI_Y )
			{
				MV1SetRotationXYZ( g_Loadmodel[e_cannon + arg_cnt] , VGet(0.0f, 0.0f,0.0f) ) ;
				MV1SetRotationXYZ( g_Loadmodel[e_bullet + arg_cnt] , VGet(0.0f, 0.0f,0.0f) ) ;
			}
			break ;

		// --- 第二ステージの場合
		case e_Stage2_Circle :
			// --- 大砲から中心座標に向かうベクトルを算出
			m_PosiVec = VSub( m_CenterPosi , g_Modelpos[e_cannon + arg_cnt] ) ;

			// --- atan2 を使用して角度を取得
			m_CA_can_ragian = atan2( m_PosiVec.x, m_PosiVec.z ) ;

			// --- 大砲モデルの向き
			MV1SetRotationXYZ( g_Loadmodel[e_cannon + arg_cnt]  , VGet(0,m_CA_can_ragian + DX_PI_F,0.0f) ) ;
			MV1SetRotationXYZ( g_Loadmodel[e_bullet + arg_cnt]  , VGet(0,m_CA_can_ragian + DX_PI_F,0.0f) ) ;
	
			break ;
	}
}

/* ------------------------------------ */
/*										*/
/*		大砲の設定をリセット			*/
/*										*/
/* ------------------------------------ */
void CannonAction::CannonRest(int arg_xcnt) {
				
	m_CA_flg[arg_xcnt] = FALSE ;											// --- アニメーションフラグ

	m_CA_bulletflg[arg_xcnt] = FALSE ;										// --- 弾の処理フラグ

	g_Dispflg[e_cannon + arg_xcnt] = 0 ;									// --- 大砲描画フラグ

    g_Dispflg[e_bullet + arg_xcnt] = 0 ;									// --- 弾描画フラグ

	g_Modelpos[e_cannon + arg_xcnt] = VGet( -500.0f , 300.0f , -500.0f ) ;	// --- ポジションをリセット

	m_CA_radian[arg_xcnt] = 0 ;												// --- 弾の角度リセット	

	// --- 大砲のサイズの初期化
	m_CAsizeX[e_cannon + arg_xcnt] = 0.0f ;
	m_CAsizeY[e_cannon + arg_xcnt] = 0.0f ;
	m_CAsizeZ[e_cannon + arg_xcnt] = 0.0f ;

	MV1SetScale( g_Loadmodel[e_cannon + arg_xcnt], VGet(m_CAsizeX[e_cannon + arg_xcnt] 
				, m_CAsizeY[e_cannon + arg_xcnt] , m_CAsizeZ[e_cannon + arg_xcnt]) ) ;
}


