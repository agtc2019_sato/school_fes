
/* ==================================================================================

		プレイヤー１クラスの実態部

+ -----------ファイルの概要----------------------------------------------------------

		変数セットや動きの処理等

+ =================================================================================== */
#include "Common.h"


/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
Player1Action::Player1Action()
{
	// --- 初期化
	InitSet() ;
}



/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
Player1Action::~Player1Action()
{}



/* ------------------------------------ */
/*										*/
/*			　初期化処理				*/
/*										*/
/* ------------------------------------ */
void Player1Action::InitSet() {
	m_pos = VGet(0.0f, 0.0f, 0.0f) ;	// --- ポジション
	m_move = VGet( 0.0f,0.0f,0.0f );	// --- 移動値
	m_ActionNo = ep_PlayerBlank ;	// --- アクションナンバー
	m_direction = 0 ;				// --- 方向
	g_Dispflg[e_player1] = 1 ;		// --- 表示フラグ
	m_playerkeydata = 0 ;			// --- キー情報
	m_keytriger = 0 ;				// --- キートリガー
	m_radius = 0 ;					// --- 半径

	m_Width = 200.0f ;		// --- 横幅
	m_Height = 200.0f ;		// --- 縦幅

	m_animno = 0 ;			// --- 待機アニメ
	g_PScoer[P2] = 0 ;		// --- 得点
	m_hitcnt = 0 ;			// --- ヒットカウント
	m_attackflg  = 0 ;		// --- アタックフラグ
	m_stage2cnt = 0 ;		// --- ステージ２のカウント

}



/* ------------------------------------ */
/*										*/
/*			アニメーション設定			*/
/*										*/
/* ------------------------------------ */
int Player1Action::PAnimInit() {
	// フレーム入れ
	m_rootflm = MV1SearchFrame(g_Loadmodel[e_player1], "root") ;

	// アニメーションの読み込み //
	g_Animmodel[P1][ep_Wait] = MV1LoadModel( "Models/Player/RobotA_Wait.mv1" ) ;
	g_Animmodel[P1][ep_Walk] = MV1LoadModel( "Models/Player/RobotA_Walk.mv1" ) ;
	g_Animmodel[P1][ep_Down] = MV1LoadModel( "Models/Player/RobotA_Down.mv1" ) ;
	g_Animmodel[P1][ep_Stan] = MV1LoadModel( "Models/Player/RobotA_Stan.mv1" ) ;
	g_Animmodel[P1][ep_Attack] = MV1LoadModel( "Models/Player/RobotA_AttackItigeki.mv1" ) ;
	for ( int i = 0 ; i < 5 ; i++ ) {
		if ( g_Animmodel[P1][i] == -1 )
			return -1 ;
	}

	// --- 待機アニメの時間を取得
	m_attachno[ep_Wait] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Wait] ) ;
	m_totalanimtime[ep_Wait] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player1], m_attachno[ep_Wait] ) ;
	MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Wait] ) ;

	// --- 歩きアニメの時間を取得
	m_attachno[ep_Walk] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Walk] ) ;
	m_totalanimtime[ep_Walk] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player1], m_attachno[ep_Walk] ) ;
	MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Walk] ) ;

	// --- ダウンアニメの時間を取得
	m_attachno[ep_Down] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Down] ) ;
	m_totalanimtime[ep_Down] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player1], m_attachno[ep_Down] ) ;
	MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Down] ) ;

	// --- スタンアニメの時間を取得
	m_attachno[ep_Stan] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Stan] ) ;
	m_totalanimtime[ep_Stan] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player1], m_attachno[ep_Stan] ) ;
	MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Stan] ) ;

	// --- アタックアニメの時間を取得
	m_attachno[ep_Attack] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Attack] ) ;
	m_totalanimtime[ep_Attack] = MV1GetAttachAnimTotalTime( g_Loadmodel[e_player1], m_attachno[ep_Attack] ) ;
	m_totalanimtime[ep_Attack] = P_ATTIME ;

	MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Attack] ) ;
	m_animno = ep_Attack ;

	return 0 ;
}



/* ------------------------------------ */
/*										*/
/*				アクション				*/
/*										*/
/* ------------------------------------ */
void Player1Action::Action() {
	// --- 回転移動トリガー ０:回転１:移動
	BOOL ctriger = 0 ;


	switch( m_ActionNo ) {

		// --- プレイヤー初期 --- //
		case ep_PlayerBlank :
			printf( "Blank" ) ;
			// --- アタッチを外す
			MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[m_animno] ) ;
			// --- アニメーションタイムの初期化
			m_playtime = 0.0f ;
			// --- 待機アニメーション入れ
			m_attachno[ep_Wait] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Wait] ) ;
			m_attackflg = 0 ;	// --- アタックフラグ降し

			m_ActionNo = ep_PlayerWait ;
			break ;


		// --- 待機(キー押下で移動) --- //
		case ep_PlayerWait :
			// --- アニメ番号入れ
			m_animno = ep_Wait ;
			// --- アニメーション進行
			m_playtime += 1.0f ;
			if ( m_playtime > m_totalanimtime[ep_Wait] )
				m_playtime = 0.0f ;

			// --- キーが押されていたら変更
			if ( m_keytriger ) {
				m_ActionNo = ep_PlayerWalk ;
				MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Wait] ) ;	// --- アタッチを外す
				m_attachno[ep_Walk] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Walk] ) ;
				m_playtime = 0.0f ;
			}

			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Wait], m_playtime ) ;
			break ;


		// --- 歩き処理 --- //
		case ep_PlayerWalk :
			// --- アニメ番号入れ
			m_animno = ep_Walk ;
			// --- アニメーション進行
			m_playtime += 3.0f ;
			if ( m_playtime > m_totalanimtime[ep_Walk] )
				m_playtime = 0.0f ;
			// --- ボタン処理
			ctriger = PlayerMove( ctriger ) ;
			// --- 待機に戻る
			if ( m_keytriger == 0 ) {
				if ( WalkDec() ) {		// --- 減速処理が終わったら待機に戻る
					m_ActionNo = ep_PlayerBlank ;
					MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Walk] ) ;	// --- アタッチを外す
					m_playtime = 0.0f ;
				}
			}
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Walk], m_playtime ) ;
			break ;


		// --- 攻撃 --- //
		case ep_PlayerAttack :
			// --- アニメ番号入れ
			m_animno = ep_Attack ;

			if ( m_playtime == 10.0f )
			{						// --- 攻撃サウンド再生
				PlaySoundMem( g_soundDT[e_Sattack],  DX_PLAYTYPE_BACK, TRUE ) ;
			}

			// --- アニメーション進行
			if ( m_playtime > m_totalanimtime[ep_Attack] ) {
				m_attackflg = 0 ;				// --- フラグ降し
				m_playtime = 0.0f ;				// --- アニメーションタイム消し
				m_ActionNo = ep_PlayerBlank ;	// --- 待ちアクションへ
				MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Attack] ) ;	// --- アタッチを外す
			} else {
				m_playtime += 1.0f ;
			}
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Attack], m_playtime ) ;
			break ;


		// --- スタン --- //
		case ep_PlayerStan :
			// --- アニメ番号入れ
			m_animno = ep_Stan ;
			// --- アニメーション進行
			if ( m_playtime > m_totalanimtime[ep_Stan] ) {
				m_playtime = 0.0f ;				// ---アニメーションタイム消し
				m_ActionNo = ep_PlayerBlank ;	// --- 待ちアクションへ
				MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Stan] ) ;	// --- アタッチを外す
			} else {
				m_playtime += 0.8f ;
				m_move = VGet(0.0f, 0.0f, 0.0f) ;
			}
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Stan], m_playtime ) ;
			break ;


		// --- ダウン --- //
		case ep_PlayerDown :
			// --- アニメ番号入れ
			m_animno = ep_Down ;
			// --- アニメーション進行
			if ( m_playtime > m_totalanimtime[ep_Down] ) {
				m_playtime = 0.0f ;				// ---アニメーションタイム消し
				m_ActionNo = ep_PlayerBlank ;	// --- 待ちアクションへ
				MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Down] ) ;	// --- アタッチを外す
			} else {
				m_playtime += 1.3f ;
				HitBullet() ;	// --- 弾に当たったときの処理
			}
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Down], m_playtime ) ;
			break ;


		// --- ステージ範囲外によるダウン --- //
		case ep_PlayerStageDown :
			// --- アニメ番号入れ
			m_animno = ep_Down ;
			g_Dispflg[e_player1] = 1 ;	// --- 非表示解除
			// --- アニメーション進行
			if ( m_playtime > m_totalanimtime[ep_Down] ) {
				m_playtime = 0.0f ;				// ---アニメーションタイム消し
				m_ActionNo = ep_PlayerBlank ;	// --- 待ちアクションへ
				MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Down] ) ;	// --- アタッチを外す
				m_pos = VGet(0.0f, 0.0f, 0.0f) ;	// --- 初期値戻し
			} else {
				m_playtime += 1.0f ;
				ctriger = 1 ;
			}
			// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
			MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Down], m_playtime ) ;
			break ;

	}

	// --- ヒットチェック
	if ( m_ActionNo < ep_PlayerDown )
		HitCheck(m_animno) ;

	// --- 回転処理
	if ( (g_SceneNo == e_Stage2_Circle) && (m_ActionNo < ep_PlayerDown) )
		Rotation( ) ;

	// --- ポジション移動
	if ( g_SceneNo == e_Stage1_Square ) {
		m_pos.x += m_move.x ;
		m_pos.y = 0.0f ;
		m_pos.z += m_move.z ;
	} else if ( ctriger ) {		// --- レコード回転無し
		m_pos = VAdd(m_pos, m_move) ;
	} else {					// --- レコード回転有り
		m_pos = VAdd(VGet(0.0f, 0.0f, 0.0f), m_rotation) ;
	}

	// --- 座標位置入れ
	g_Modelpos[e_player1] = m_pos ;

}



/* ------------------------------------ */
/*										*/
/*			キー情報格納				*/
/*										*/
/* ------------------------------------ */
int Player1Action::GetKeydata(int arg_key) {
	// --- 上
	if ( arg_key & PAD_INPUT_UP ) {
		m_playerkeydata |= 0x01 ;
		m_keytriger = 1 ;
	} else {
		m_playerkeydata &= 0xfe ;
	}

	// --- 下
	if ( arg_key & PAD_INPUT_DOWN ) {
		m_playerkeydata |= 0x02 ;
		m_keytriger = 1 ;
	} else {
		m_playerkeydata &= 0xfd ;
	}
	
	// --- 左
	if ( arg_key & PAD_INPUT_LEFT ) {
		m_playerkeydata |= 0x04 ;
		m_keytriger = 1 ;
	} else {
		m_playerkeydata &= 0xfb ;
	}

	// --- 右
	if ( arg_key & PAD_INPUT_RIGHT ) {
		m_playerkeydata |= 0x08 ;
		m_keytriger = 1 ;
	} else {
		m_playerkeydata &= 0xf7 ;
	}

	// --- スペースキー10 ○B STARTM 
	if ( arg_key & PAD_INPUT_B ) {
		m_playerkeydata |= 0x20 ;
		m_keytriger = 1 ;
	} else {
		m_playerkeydata &= 0xdf ;
	}

	return ( arg_key ) ;
}



/* ------------------------------------ */
/*										*/
/*				移動処理				*/
/*										*/
/* ------------------------------------ */
int Player1Action::PlayerMove(BOOL arg_ctriger) {
	arg_ctriger = 1 ;	// 回らない

	switch ( m_playerkeydata ) {
		// 上 //
		case 0x01 :
			// --- 方向
			m_direction = e_Up ;
			// --- 通常処理
			m_move.z += 1.7f ;
			if ( m_move.z > PL_SPD )
				m_move.z = PL_SPD ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.z = P_PL_SPD ;
				m_playtime -= 0.5f ;
			}
			m_move.x = 0.0f ;
			break ;


		// 下 //
		case 0x02 :
			// --- 方向
			m_direction = e_Down ;
			// --- 通常処理
			m_move.z += -1.7f ;
			if ( m_move.z < -PL_SPD )
				m_move.z = -PL_SPD ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.z = -P_PL_SPD ;
				m_playtime -= 0.5f ;				
			}
			m_move.x = 0.0f ;
			break ;


		// 左 //
		case 0x04 :
			// --- 方向
			m_direction = e_Left ;
			// --- 通常処理
			m_move.x += -1.7f ;
			if ( m_move.x < -PL_SPD )
				m_move.x = -PL_SPD ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.x = -P_PL_SPD ;
				m_playtime -= 0.5f ;				
			}
			m_move.z = 0.0f ;
			break ;


		// 上左 //
		case 0x05 :
			// --- 方向
			m_direction = e_Up - 0.5f ;
			// --- 通常処理
			m_move.x = -PL_SPD_DI ;
			m_move.z = PL_SPD_DI ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.x = -P_PL_SPD_DI ;
				m_move.z = P_PL_SPD_DI ;
				m_playtime -= 0.5f ;				
			}
			break ;


		// 下左 //
		case 0x06 :
			// --- 方向
			m_direction = e_Down + 0.5f ;
			// --- 通常処理
			m_move.x = -PL_SPD_DI ;
			m_move.z = -PL_SPD_DI ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.x = -P_PL_SPD_DI ;
				m_move.z = -P_PL_SPD_DI ;
				m_playtime -= 0.5f ;				
			}
			break ;


		// 右 //
		case 0x08 :
			// --- 方向
			m_direction = e_Right ;
			// --- 通常処理
			m_move.x += 1.7f ;
			if ( m_move.x > PL_SPD )
				m_move.x = PL_SPD ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.x = P_PL_SPD ;
				m_playtime -= 0.5f ;				
			}
			m_move.z = 0.0f ;
			break ;


		// 上右 //
		case 0x09 :
			// --- 方向
			m_direction = e_Up + 0.5f ;
			// --- 通常処理
			m_move.x = PL_SPD_DI ;
			m_move.z = PL_SPD_DI ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.x = P_PL_SPD_DI ;
				m_move.z = P_PL_SPD_DI ;
				m_playtime -= 0.5f ;				
			}
			break ;

		// 下右 //
		case 0x0a :
			// --- 方向
			m_direction = e_Down - 0.5f ;
			// --- 通常処理
			m_move.x = PL_SPD_DI ;
			m_move.z = -PL_SPD_DI ;
			// --- パネル処理
			if ( go_Panel.m_ptopno[P1] ) {
				m_move.x = P_PL_SPD_DI ;
				m_move.z = -P_PL_SPD_DI ;
				m_playtime -= 0.5f ;				
			}
			break ;


		// それ以外 //
		default :
			// --- レコード回転処理
			if ( g_SceneNo == e_Stage1_Square )
				arg_ctriger = 1 ;
			else
				arg_ctriger = 0 ;
			m_keytriger = 0 ;	// --- 回る
			break ;
	}


	
	// 攻撃 //
	if ( m_playerkeydata & 0x20 ) {
		m_keytriger = 1 ;
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[m_animno] ) ;	// --- アタッチを外す
		m_attachno[ep_Attack] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Attack] ) ;
		m_playtime = 0.0f ;		// ---アニメーションタイム消し
		m_ActionNo = ep_PlayerAttack ;
		// --- スタン時以降は当たらない
		if ( (AP1toAP2HitCheck()) && (go_2Player.m_ActionNo < ep_PlayerStan) ) {
			if ( m_attackflg == 0 ) {
				m_attackflg  = 1 ;
				HitAttack() ;
			}
		}


		m_move = VGet(0.0f, 0.0f, 0.0f) ;
	}


	// --- 高さ変更なし
	m_move.y = 0.0f ;

	// --- モデルの回転
	MV1SetRotationXYZ(g_Loadmodel[e_player1], VGet(0.0f, 1.57f * m_direction, 0.0f)) ;

	return ( arg_ctriger ) ;
}



/* ------------------------------------ */
/*										*/
/*				減速処理				*/
/*										*/
/* ------------------------------------ */
int Player1Action::WalkDec() {
	BOOL return_no = 0 ;

	// --- ｚ軸減速
	if ( m_move.z == 0.0 )
		m_move.z = 0.0f ;
	else if ( m_move.z > 0.0 )
		m_move.z -= 1.0f ;
	else if ( m_move.z < 0.0 )
		m_move.z += 1.0f ;

	// --- ｘ軸減速
	if ( m_move.x == 0.0 )
		m_move.x = 0.0f ;
	else if ( m_move.x > 0.0 )
		m_move.x -= 1.0f ;
	else if ( m_move.x < 0.0 )
		m_move.x += 1.0f ;

	// 0に近くなったら0に戻す //
	if ( (m_move.z < 1.0) && (m_move.z > -1.0) && (m_move.x < 1.0) && (m_move.x > -1.0) ) {
		m_move = VGet(0.0f, 0.0f, 0.0f) ;
		return_no = 1 ;		// --- 正常終了
	}


	return (return_no) ;
}



/* ------------------------------------ */
/*										*/
/*				回転処理				*/
/*										*/
/* ------------------------------------ */
int Player1Action::Rotation( ) {

	/*半径処理*/
	m_Square[0] = pow((double)m_pos.x, 2) ;		// --- ２乗
	m_Square[1] = pow((double)m_pos.z, 2) ;		// --- ２乗
	m_Square[2] = m_Square[0] + m_Square[1] ;	// --- 足し算
	m_radius = sqrt( m_Square[2] ) ;			// --- √計算
	/*角度処理*/
	m_angle[0] = atan2(m_pos.z, m_pos.x) ;
	m_angle[1] = m_angle[0] / (g_Pi / DEFANGLE) ;
	/*外への移動*/
	m_radius += 0.5f ;
	m_angle[1]-- ;
	/**/


	// 代入 //
	if ( m_radius < STG2_RADIUS ) {	// --- radiusが700以下の場合は代入
		m_rotation.x = (float)(cos(m_angle[1] * g_Pi / DEFANGLE) * m_radius) ;
		m_rotation.y = 0.0f ;
		m_rotation.z = (float)(sin(m_angle[1] * g_Pi / DEFANGLE) * m_radius) ;
	} else {
		m_move = VGet(0.0f, 0.0f, 0.0f) ;	// --- 初期値戻し
		m_hitcnt = HITCNT ;					// --- ヒットカウント
		g_PScoer[P2]++ ;					// --- ２Ｐ側への点数追加
		m_ActionNo = ep_PlayerStageDown ;	// --- 吹き飛びアクションへ
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[ep_Walk] ) ;	// --- アタッチを外す
		m_attachno[ep_Down] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Down] ) ;
		m_playtime = 0.0f ;					// ---アニメーションタイム消し
	}


	return 0 ;
}



/* ------------------------------------ */
/*										*/
/*		　ヒットチェック処理			*/
/*										*/
/* ------------------------------------ */
int Player1Action::HitCheck(int arg_anim) {
	int retutn_no = 0 ;

	// --- ヒットチェックまでのラグ
	if ( m_hitcnt > 0 ) {
		m_hitcnt-- ;
		if ( m_hitcnt <= 30 ) {
			if ( (m_hitcnt % 3) == 0 )
				g_Dispflg[e_player1] ^= 1 ;
		} else {
			if ( (m_hitcnt % 10) == 0 )
				g_Dispflg[e_player1] ^= 1 ;
		}
	} else {
		g_Dispflg[e_player1] = 1 ;
		m_hitcnt = 0 ;
	}

	
	// --- 弾との当たり判定
	if ( (m_hitcnt == 0) && (OtoPHitCheck(e_player1)) ) {
		printf( "Bullet" ) ;
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[arg_anim] ) ;	// --- アタッチを外す
		m_attachno[ep_Down] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Down] ) ;
		m_playtime = 0.0f ;		// ---アニメーションタイム消し
		g_PScoer[P2]++ ;		// --- ２Ｐ側への点数追加
		m_ActionNo = ep_PlayerDown ;
	}


	// --- 爆発との当たり判定
	if ( (m_hitcnt == 0) && (BtoPHitCheck(e_player1)) ) {
		printf( "爆発" ) ;
		g_Dispflg[e_player1] = 1 ;
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[arg_anim] ) ;	// --- アタッチを外す
		m_attachno[ep_Down] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Down] ) ;
		m_playtime = 0.0f ;		// ---アニメーションタイム消し
		g_PScoer[P2]++ ;		// --- ２Ｐ側への点数追加
		m_ActionNo = ep_PlayerDown ;
	}


	// --- プレイヤー同士で当たっていた場合
	if ( P1toP2HitCheck() ) {
		m_move.x *= -1 ;
		m_move.z *= -1 ;
	}


	// --- プレイヤーの床チェック
	if ( (g_SceneNo == e_Stage1_Square) && (PlayerSquare(e_player1) == 0) && (m_animno != ep_Down) ) {
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[m_animno] ) ;	// --- アタッチを外す
		m_attachno[ep_Down] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][ep_Down] ) ;
		m_playtime = 0.0f ;					// ---アニメーションタイム消し
		g_PScoer[P2]++ ;					// --- ２Ｐ側への点数追加
		m_move = VGet(0.0f, 0.0f, 0.0f) ;	// --- 初期値戻し
		m_hitcnt = HITCNT ;					// --- ヒットカウント
		m_ActionNo = ep_PlayerStageDown ;	// --- 吹き飛びアクションへ
	}


	return 0 ;
}



/* ------------------------------------ */
/*										*/
/*			攻撃時のアニメ処理			*/
/*										*/
/* ------------------------------------ */
int Player1Action::HitAttack() {
	int retutn_no = 0 ;

	m_playtime = 0.0f ;				// ---アニメーションタイム消し
	// --- プレイヤー２はスタンする
	go_2Player.m_ActionNo = ep_PlayerStan ;
	MV1DetachAnim( g_Loadmodel[e_player2], m_attachno[go_2Player.m_animno] ) ;	// --- アタッチを外す
	go_2Player.m_attachno[ep_Stan] = MV1AttachAnim( g_Loadmodel[e_player2], 0, g_Animmodel[P2][ep_Stan] ) ;
	go_2Player.m_playtime = 0.0f ;	// ---アニメーションタイム消し
	go_Effect.m_stanflg |= 0x02 ;	// --- プレイヤー２スタン画像描画
	
	return (retutn_no) ;
}



/* ------------------------------------ */
/*										*/
/*		　弾に当たったときの処理		*/
/*										*/
/* ------------------------------------ */
int Player1Action::HitBullet() {
	int retutn_no = 0 ;
	// --- その場で止まる
	m_move = VGet(0.0f, 0.0f, 0.0f ) ;

	if ( m_hitcnt <= 0 )
		m_hitcnt = HITCNT ;
	
	return (retutn_no) ;
}



/* ------------------------------------ */
/*										*/
/*	　　　ステージ１から動き			*/
/*										*/
/* ------------------------------------ */
void Player1Action::Stage1move() {

	// --- アニメーション初期化
	if ( m_animno != ep_Walk ) {
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[m_animno] ) ;	// --- アタッチを外す
		m_playtime = 0.0f ;
		m_animno = ep_Walk ;
		m_attachno[ep_Walk] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][m_animno] ) ;
	}

	// --- アニメーション進行
	m_playtime += 2.5f ;
	if ( m_playtime > m_totalanimtime[ep_Walk] )
		m_playtime = 0.0f ;

	// --- ポジション代入
	if ( m_pos.x >= 600.0f ) {
		stanimflg = 1 ;		// --- アニメーションフラグ
		if ( go_Stage.m_Rc_Playtime[0] > 2.0f )
			m_move = VGet(PL_STG2_SPD, 0.0f, 0.0f) ;
	} else {
		m_move = VGet(10.0f, 0.0f, 0.0f) ;
	}
	m_pos = VAdd(m_pos, m_move) ;


	// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Walk], m_playtime ) ;
	// --- 回転値
	MV1SetRotationXYZ(g_Loadmodel[e_player1], VGet(0.0f, 1.57f * e_Right, 0.0f)) ;
	// --- ポジション代入
	g_Modelpos[e_player1] = m_pos ;

}



/* ------------------------------------ */
/*										*/
/*	　　　ステージ２への初期化			*/
/*										*/
/* ------------------------------------ */
void Player1Action::Stage2Init() {
	// --- プレイヤー位置セット
	m_move = VGet(0.0f, 0.0f, 0.0f) ;
	m_pos = VGet(-3300.0f, PL_STG2_Y, 0.0f) ;
	go_2Player.m_move = VGet(0.0f, 0.0f, 0.0f) ;
	go_2Player.m_pos = VGet(-3000.0f, PL_STG2_Y, 0.0f) ;
	g_Modelpos[e_player1] = m_pos ;
	g_Modelpos[e_player2] = go_2Player.m_pos ;
	m_direction = e_Down ;
	go_2Player.m_direction = e_Down ;
}



/* ------------------------------------ */
/*										*/
/*	　　　ステージ２まで動き			*/
/*										*/
/* ------------------------------------ */
void Player1Action::Stage2move() {
	m_stage2cnt++ ;	// --- レコードアニメーション用のカウント

	// --- アニメーション初期化
	if ( m_animno != ep_Wait ) {
		MV1DetachAnim( g_Loadmodel[e_player1], m_attachno[m_animno] ) ;	// --- アタッチを外す
		m_playtime = 0.0f ;
		m_animno = ep_Wait ;
		m_attachno[ep_Wait] = MV1AttachAnim( g_Loadmodel[e_player1], 0, g_Animmodel[P1][m_animno] ) ;
	}

	// --- アニメーション進行
	m_playtime += 1.0f ;
	if ( m_playtime > m_totalanimtime[ep_Wait] )
		m_playtime = 0.0f ;

	// --- ポジション代入
	if ( stanimflg == 1 ) {
		if ( m_stage2cnt <= 101 )
			m_move = VGet(30.0f, 0.0f, 0.0f) ;
		else if ( m_stage2cnt <= 150 )
			m_move = VGet(0.0f, -5.5f, 0.0f) ;
		else
			m_move = VGet(0.0f, -8.0f, 0.0f) ;
		m_pos = VAdd(m_pos, m_move) ;
	}


	// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	MV1SetAttachAnimTime( g_Loadmodel[e_player1], m_attachno[ep_Wait], m_playtime ) ;
	// --- 回転値
	MV1SetRotationXYZ(g_Loadmodel[e_player1], VGet(0.0f, 1.57f * e_Down, 0.0f)) ;
	// --- ポジション代入
	g_Modelpos[e_player1] = m_pos ;

}



