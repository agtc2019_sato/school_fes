
/* ==================================================================================

		エフェクトのアクション

+ -----------ファイルの概要----------------------------------------------------------
		エフェクトのクラス

+ =================================================================================== */

class EffectAction : BasicObj
{
	public :
		EffectAction() ;
		~EffectAction() ;

		void InitSet() override ;		// --- 初期セット
		void Action() override ;		// --- アクション
		void EffectSelect( char ) ;		// --- プレイヤーナンバーチェック
		void EffectDraw() ;				// --- 描画
		void EffectDrawP() ;			// --- プレイヤー前描画

		int m_playerbmp[2] ;
		u_char m_stanflg ;				// --- スタン時の表示フラグ
		BOOL m_cloudFlg[CANNON_MAX]	;	// --- 煙フラグ

	private :
		char m_efflg ;					// --- エフェクト描画フラグ
		int m_E_action ;				// --- エフェクトアニメション格納
		int m_E_attach ;				// --- エフェクトアタッチ
		float m_E_totaltime ;			// --- エフェクトトータルタイム
		float m_E_plytime[5] ;			// --- エフェクトプレイタイム
} ;


