
/* ==================================================================================

		アイテムパネルのアクション

+ -----------ファイルの概要----------------------------------------------------------
		アイテムパネルのアクション

+ =================================================================================== */

class ItemPanelAction : BasicObj
{
	public :
		ItemPanelAction() ;
		~ItemPanelAction() ;

		void InitSet() override ;		// --- 初期セット
		void Action() override ;		// --- アクション
		int RandomPos() ;				// --- ランダムポジション入れ
		int PlayerHit() ;				// --- プレイヤーに当たったとき

		int		m_Rootflm ;			// --- ルートフレーム情報
		int		m_Attachidx ;		// --- アニメーションの紐づけに使用
		float	m_Anim_totaltime ;	// --- アニメーションの総時間
		float	m_Playtime ;		// --- アニメーションの経過時間

		
		double m_radius ;			// --- 半径
		double m_angle ;			// --- 角度計算	０：角度　１：＋ー
		VECTOR m_rotation ;			// --- 回転値入れ

		int m_ptopno[3] ;			// --- プレイヤーのどっちに当たったか
		int m_flgcnt ;				// --- パネル効果時間計算

	private :

} ;

