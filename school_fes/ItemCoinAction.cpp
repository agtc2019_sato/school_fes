
/* ==================================================================================

		アイテムのアクション

+ -----------ファイルの概要----------------------------------------------------------
		アイテムのアクション実装部


+ =================================================================================== */

#include "Common.h"

/* ------------------------------------ */
/*										*/
/*			  コンストラクタ			*/
/*										*/
/* ------------------------------------ */
ItemCoinAction::ItemCoinAction() {

}

/* ------------------------------------ */
/*										*/
/*			  デストラクタ				*/
/*										*/
/* ------------------------------------ */
ItemCoinAction::~ItemCoinAction() {

}

/* ------------------------------------ */
/*										*/
/*			  初期セット				*/
/*										*/
/* ------------------------------------ */
void ItemCoinAction::InitSet()
{
	m_pos = VGet( 500.0f, 50.0f, 0.0f ) ;
	m_move = VGet( 5.0f, 0.0f, 0.0f ) ;

	// --- フレーム情報の格納
	m_Rootflm			= MV1SearchFrame( g_Loadmodel[e_coin], "root" ) ;

	// --- アニメーションの経過時間
	m_Playtime = 0.0f ;
	// --- コイン回転アニメーション読み込み
	m_AnimDt			= MV1LoadModel( "Models/Item/StarA_new.mv1" ) ;

	// --- コインアニメ時間の取得
	m_Attachidx			= MV1AttachAnim( g_Loadmodel[e_coin], 0, m_AnimDt ) ;
	m_Anim_totaltime	= MV1GetAttachAnimTotalTime( g_Loadmodel[e_coin], m_Attachidx ) ;

	m_hitcheck			= 0 ;
	m_spd				= VGet( 0.0f, 2.0f, 0.0f ) ;	// --- バウンド
	m_playercoin[P1]	= 0 ;		// --- 1Pコイン未所持
	m_playercoin[P2]	= 0 ;		// --- 2Pコイン未所持
	m_blinkcnt			= 10 ;		// --- 初期化

	// --- バウンド方向
	m_direction[0]		= -0.05f ;
	m_direction[1]		= 0.05f ;

	m_invincible[P1] = 0 ;
	m_invincible[P2] = 0 ;

	MV1SetScale( g_Loadmodel[e_coin] , VGet(0.7f,0.7f,0.7f) ) ;

}

/* ------------------------------------ */
/*										*/
/*			  アクション				*/
/*										*/
/* ------------------------------------ */
void ItemCoinAction::Action()
{
	// --- 跳ね返り
	if ( ((m_pos.x > 500.0f) && (m_move.x > 1.0)) || (m_pos.x < -500.0f) && (m_move.x < -1.0) ) {
		m_move.x *= -1 ;
	}


	// --- コイン回転アニメーション進行
	if ( (m_playercoin[P1] == 0) && (m_playercoin[P2] == 0) ) {
		m_Playtime += 0.5f ;
		if ( m_Playtime > m_Anim_totaltime )
			m_Playtime = 0.0f ;
	}

	// --- キャラモデルにアタッチされているアニメにアニメ進行時間を与える
	// --- (アニメーションの何番目（何秒後）の動きか同期させる
	MV1SetAttachAnimTime( g_Loadmodel[e_coin], m_Attachidx, m_Playtime ) ;


	// --- コインを取ったときのヒットチェック 
	if ( (CtoPHitCheck() != 0) && (m_hitcheck == 0) &&
		((go_1Player.m_ActionNo < ep_PlayerDown) || ((go_2Player.m_ActionNo < ep_PlayerDown))) && 
		((m_outofrange[P1] != 1) && (m_outofrange[P2] != 1)) ) {
		PHitCheck(CtoPHitCheck()) ;		// --- 1P2Pどっちが取ったか
		// --- コインサウンド再生
		ChangeVolumeSoundMem( 155 , g_soundDT[e_Scoin] ) ;
		PlaySoundMem( g_soundDT[e_Scoin],  DX_PLAYTYPE_BACK, TRUE ) ;
	}
	// --- コイン落とし処理
	LostCoin() ;

	// --- 座標のセット
	if ( ((m_playercoin[P1] != 1) && (m_playercoin[P2] != 1)) &&
		 ((m_outofrange[P1] != 1) && (m_outofrange[P2] != 1))) 
		m_pos = VAdd(m_pos, m_move) ;


	// --- 座標位置入れ
	g_Modelpos[e_coin] = m_pos ;

}



/* ------------------------------------ */
/*										*/
/*		プレイヤーの横に表示する		*/
/*										*/
/* ------------------------------------ */
int ItemCoinAction::PHitCheck(int arg_player)
{
	// --- どっちに当たったか
	if ( arg_player == e_player1 ) {
		m_pos = VGet( -575.0f, 425.0f, 0.0f ) ; // --- １Ｐ側
		m_playercoin[P1] = 1 ;
		m_playercoin[P2] = 0 ;
	} else if ( arg_player == e_player2 ) {
		m_pos = VGet( 575.0f, 425.0f, 0.0f ) ; // --- １Ｐ側
		m_playercoin[P1] = 0 ;
		m_playercoin[P2] = 1 ;
	}

	m_Playtime = 0.0f ;
	MV1SetRotationXYZ( g_Loadmodel[e_coin] , VGet(0.0f,0.0f,0.0f) ) ;

	return 0 ;
}



/* ------------------------------------ */
/*										*/
/*		コインをなくした時の処理		*/
/*										*/
/* ------------------------------------ */
int ItemCoinAction::LostCoin()
{
	int playeracq = 0 ;		
	// --- どちらが持っているか
	if ( m_playercoin[P1] ) {
		playeracq = e_player1 ;
		go_Result.m_playercoin = true ;
	} else if ( m_playercoin[P2] ) {
		playeracq = e_player2 ;
		go_Result.m_playercoin = false ;
	}


	// --- プレイヤー同士
	if ( (P1toP2HitCheck()) && ((m_playercoin[P1]) || (m_playercoin[P2])) ) {
		m_hitcheck = 1 ;	// --- はじけ飛びフラグ
		printf( "プレイヤー同士" ) ;
		m_spd.y = 10.0f ;				// --- 初速
		m_spd.x = 0.0f ;
		m_pos = VAdd(g_Modelpos[playeracq] , VGet(0.0f, 200.0f, 0.0f)) ;
		m_pos = VAdd( m_pos, m_spd ) ;	// --- 下がる
		if ( m_playercoin[P1] )
			m_playercoin[P1] = 0 ;		// --- 保持無し
		else if ( m_playercoin[P2] )
			m_playercoin[P2] = 0 ;		// --- 保持無し
	}

	// --- 一回無敵時に当たったとき
	if ( (m_invincible[P1]) || (m_invincible[P2]) ) {
		m_hitcheck = 1 ;	// --- はじけ飛びフラグ
		m_spd.y = 10.0f ;				// --- 初速
		m_spd.x = 0.0f ;
		m_pos = VAdd(g_Modelpos[playeracq] , VGet(0.0f, 200.0f, 0.0f)) ;
		m_pos = VAdd( m_pos, m_spd ) ;	// --- 下がる
		if ( m_playercoin[P1] ) {
			m_playercoin[P1] = 0 ;		// --- 保持無し
			m_invincible[P1] = 0 ;
			go_1Player.m_hitcnt = HITCNT - 50 ;
			go_Panel.m_ptopno[P1] = 1 ;	// --- 足遅
		} else if ( m_playercoin[P2] ) {
			m_playercoin[P2] = 0 ;		// --- 保持無し
			m_invincible[P2] = 0 ;
			go_2Player.m_hitcnt = HITCNT - 50 ;
			go_Panel.m_ptopno[P2] = 1 ;	// --- 足遅
		}
	}

	// --- 弾け飛び
	if ( m_hitcheck ) {
		m_spd.y += -0.5f ;
		m_spd.x += m_direction[m_blinkcnt % 2] ;
		m_spd.z = 0.0f ;
		m_pos = VAdd( m_pos, m_spd ) ;		// --- 下がる
		if ( m_pos.y <= 50.0f ) {
			m_blinkcnt += 1 ;
			m_hitcheck = 0 ;
			m_spd = VGet(0.0f, 0.0f, 0.0f) ;
		}
	}
	// --- 足遅解除
	if ( go_1Player.m_hitcnt <= 0 ) {
		go_Panel.m_ptopno[P1] = 0 ;	// --- 足遅無し
	} else if ( go_2Player.m_hitcnt <= 0 ) {
		go_Panel.m_ptopno[P2] = 0 ;	// --- 足遅無し
	}


	// --- 範囲外に出たときの処理　コインを持っていて二回以降外に出ていない場合
	if ( (m_playercoin[P1]) && (m_outofrange[P1] != 1) && (go_1Player.m_ActionNo == ep_PlayerStageDown) ) {
		m_outofrange[P1] = 1 ;	// --- 範囲外
		m_spd.x = 0.0f ;
		m_spd.y = 0.5f ;
		m_pos = VGet(0.0f, WINDOW_H / 2, 0.0f) ;	// --- 上からスタート
		m_playercoin[P1] = 0 ;	// --- 保持無し
	}
	if ( (m_playercoin[P2]) && (m_outofrange[P2] != 1) && (go_2Player.m_ActionNo == ep_PlayerStageDown) ) {
		m_outofrange[P2] = 1 ;	// --- 範囲外
		m_spd.x = 0.0f ;
		m_spd.y = 0.5f ;
		m_pos = VGet(0.0f, WINDOW_H / 2, 0.0f) ;	// 上からスタート
		m_playercoin[P2] = 0 ;	// --- 保持無し
	}

	// --- 上から落ちる処理
	if ( (m_outofrange[P1]) || (m_outofrange[P2]) ) {
		m_spd.x = 0.0f ;
		m_spd.y = -2.0f ;
		m_pos = VAdd(m_pos, m_spd) ;	// --- ポジション足しこみ
		m_blinkcnt-- ;
		// --- 取れないときは点滅
		if ( m_blinkcnt < 0 ) {
			g_Dispflg[e_coin] ^= 1 ;
			if ( m_pos.y <= 150.f )
				m_blinkcnt = 5 ;
			else
				m_blinkcnt = 10 ;
		}
		// --- ポジション固定
		if ( m_pos.y <= 50.0f ) {
			m_spd = VGet(0.0f, 0.0f, 0.0f) ;	// --- 初期化
			m_pos.y = 50.0f ;
			g_Dispflg[e_coin] = 1 ;
			if ( m_outofrange[P1] )
				m_outofrange[P1] = 0 ;
			else if ( m_outofrange[P2] )
				m_outofrange[P2] = 0 ;
		}
	}

	return 0 ;
}


