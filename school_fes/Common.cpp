
/* ==================================================================================

		共通での処理、汎用性がある関数や変数の実態宣言部

+ -----------ファイルの概要----------------------------------------------------------

		オブジェクトの実態やグローバル変数等

+ =================================================================================== */

#include <windows.h>
#include <stdio.h>	// --- コンソール用
#include "Common.h"

/* -----------------------------------------------------------------
|
|			オブジェクトの実体宣言
|			: go_
|
+----------------------------------------------------------------- */
ConsoleWindow	g_cWin ;			// --- コンソールウィンドウ
TitleCharaSet	go_TitChaSet ;		// --- タイトルとキャラセレクト
Player1Action	go_1Player ;		// --- プレイヤー１クラス
Player2Action	go_2Player ;		// --- プレイヤー２クラス
CannonAction	go_Cannon ;			// --- 大砲クラス
StageAction		go_Stage ;			// --- ステージクラス
ItemCoinAction	go_Coin ;			// --- アイテム・コイン
ItemPanelAction	go_Panel ;			// --- アイテム・パネル
EnemyAction		go_Enemy ;			// --- エネミークラス
EffectAction	go_Effect ;			// --- エフェクトクラス
ResultAction	go_Result ;			// --- リザルトクラス

/* -----------------------------------------------------------------
|
|			グローバル変数
|			: g_
|
+----------------------------------------------------------------- */
int			g_SceneNo = 0 ;			// --- シーンループの番号
int			g_Timercnt ;			// --- タイマーカウント
SYSTEMTIME	g_LocalTime[2] ;		// --- ローカルの時間

BMPDT		g_Bmpimage[e_BmpEnd] ;	// --- 画像系

VECTOR		g_Camerapos	= VGet( 0.0f, 700.0f, -1000.0f ) ;			// --- カメラポジション
//VECTOR		g_Camerapos	= VGet( 0.0f, 100.0f, -1200.0f ) ;		// --- カメラポジション
VECTOR		g_Cameratgt	= VGet( 0.0f, 0.0f, 0.0f ) ;				// --- カメラ注視点

BOOL		g_Dispflg[e_end] ;			// --- 表示フラグ
VECTOR		g_Modelpos[e_end] ;			// --- モデルポジション
int			g_Loadmodel[e_end] ;		// --- モデルの読み込み
int			g_Animmodel[2][ep_end] ;	// --- アニメーションの読み込み
float		g_Pi = 3.141592f ;			// --- 円周率

int			g_PScoer[2] ;				// --- プレイヤーのスコアとコイン保持

int			g_stflg ;					// --- ステージフラグ
BOOL		g_timeflg ;					// --- 第二ステージタイムフラグ
int			stanimflg ;					// --- ステージアニメフラグ
float		g_titsize ;					// --- タイトル大きさ変更用
int			g_pmflg = 0 ;				// --- タイトル大きさ変更用
int			g_titcnt = 0 ;				// --- タイトル点滅用
int			g_nxtcnt = 0 ;				// --- タイトル点滅用

int			g_soundDT[e_SoundEnd] ;		// --- サウンドデータ

bool		g_readygoflg = false ;		// --- ゴー画像


/* -----------------------------------------------------------------
|
|			グローバル関数の実装部
|
+----------------------------------------------------------------- */

