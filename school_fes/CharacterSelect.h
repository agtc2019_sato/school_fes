
/* ==================================================================================

		タイトル・セレクトの宣言部

+ -----------ファイルの概要----------------------------------------------------------

		タイトル時の表示やセレクト画面での処理

+ =================================================================================== */

// --- 二次元ベクトル
typedef struct {
	int	x ;
	int	y ;
} VECTOR2 ;

// ------------------------- //
//			クラス			 //
// ------------------------- //
class TitleCharaSet
{
	public :
		TitleCharaSet( ) ;
		~TitleCharaSet( ) ;

		void	CharSelInit() ;			// --- セレクト画面初期化
		int		CharSel()  ;			// --- セレクト画面
		void	CharAnim() ;			// --- セレクトされた時のアニメーション

		u_char	m_selectcnt[4] ;		// --- プレイヤーのセレクトカウント
		int		m_sceneno ;				// --- シーン切り替え用
		int		m_fadecnt ;				// --- フェードカウント
		VECTOR2	m_imagepos[8] ;			// --- 画像用座標
		int		m_Pmdldt[4] ;			// --- モデル格納用変数
	
	//private :
		char	m_Decisionflg[2] ;		// --- 決定フラグ
		bool	m_Finalflg[2] ;			// --- 確認フラグ
		int		m_image_W ;				// --- キャラ選択画像幅
		int		m_image_H ;				// --- キャラ選択画像高さ
		int		m_imagenum ;			// --- 画像用配列ナンバー
		int		m_stagetime ;			// --- 選択用タイマー
		bool	m_endtrg ;				// --- 最終決定前のトリガー
		u_char	m_seltrg ;				// --- 十字キー長押し防止用トリガー
		int		m_keyflg[2] ;

		// アニメーション //
		int m_animmodel ;
		int m_attachno[2] ;				// --- アタッチナンバー入れ
		float m_totalanimtime[2] ;		// --- アニメーションのトータルタイム
		float m_playtime[2] ;			// --- アニメ経過時間
		int m_animno ;					// --- アニメーション番号入れ

} ;





