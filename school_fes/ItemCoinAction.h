
/* ==================================================================================

		アイテムコインのアクション

+ -----------ファイルの概要----------------------------------------------------------
		アイテムコインのアクション

+ =================================================================================== */

class ItemCoinAction : BasicObj
{
	public :
		ItemCoinAction() ;
		~ItemCoinAction() ;

		void InitSet() override ;		// --- 初期セット
		void Action() override ;		// --- アクション
		int PHitCheck(int arg_player) ;	// --- プレイヤーとのヒットチェック
		int	LostCoin() ;				// --- プレイヤーがなくしたときの処理
		//int 

		// 変数 //
		int		m_Rootflm ;			// --- ルートフレーム情報
		int		m_Attachidx ;		// --- アニメーションの紐づけに使用
		float	m_Anim_totaltime ;	// --- アニメーションの総時間
		float	m_Playtime ;		// --- アニメーションの経過時間


		BOOL m_hitcheck ;		// --- 当たったか
		VECTOR m_spd ;			// --- コイン移動スピード
		BOOL m_playercoin[2] ;	// --- プレイヤーのどっちが持っているか
		BOOL m_outofrange[2] ;	// --- プレイヤーが範囲外か
		int m_blinkcnt ;		// --- 点滅カウント

		float m_direction[2] ;	// --- コイン弾きの方向値

		int m_invincible[2] ;	// --- 無敵時のフラグ
		
	private :

} ;
