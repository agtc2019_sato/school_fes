
/* ==================================================================================

		コンソールウィンドウ

+ -----------ファイルの概要----------------------------------------------------------
		デバッグ用のコンソール表示

+ =================================================================================== */

#include <windows.h>
#include <stdio.h>	// --- コンソール用

#include "ConsoleWindow.h"

// --- コンストラクタ
ConsoleWindow::ConsoleWindow( )
{
	// --- printfでのデバッグ画できる
	AllocConsole( ) ;
	
	freopen_s( &m_fIn,  "CON", "r", stdin ) ;     // --- 標準入力の割り当て
	freopen_s( &m_fOut, "CON", "w", stdout ) ;    // --- 標準出力の割り当て

	printf( "It succeeded in starting of a console screen.\nA standard input/output can be used.\n\n" ) ;
}

// --- デストラクタ
ConsoleWindow::~ConsoleWindow( )
{
	// --- コンソールの削除
	fclose( m_fIn ) ;
	fclose( m_fOut ) ;
	FreeConsole( ) ;                
}
		
