
/* ==================================================================================

		プレイヤー１クラスの宣言部

+ -----------ファイルの概要----------------------------------------------------------

		ポジションやアクション等

+ =================================================================================== */

// ------------------------- //
//			クラス			 //
// ------------------------- //
class Player1Action : BasicObj
{
	public :
		Player1Action( ) ;
		~Player1Action( ) ;
		void InitSet() override ;			// --- 初期化
		int PAnimInit() ;					// --- アニメーション設定
		void Action() override ;			// --- アクション
		int GetKeydata(int arg_key) ;		// --- キー入力
		int PlayerMove(BOOL arg_ctriger) ;	// --- 移動処理
		int WalkDec() ;						// --- 減速値
		int Rotation() ;					// --- 回転処理
		void Stage1move() ;					// --- ステージ１からの動き
		void Stage2Init() ;					// --- プレイヤーたちの初期位置
		void Stage2move() ;					// --- ステージ２までの動き

		int HitCheck(int arg_anim) ;		// --- なにと当たったか
		int HitBullet() ;					// --- 弾と当たったときの処理
		int HitAttack() ;					// --- 攻撃が当たったときの処理

	
	//private :
		int m_ActionNo ;				// --- アクションナンバー
		VECTOR m_move ;
		VECTOR m_pos ;
		float m_direction ;				// --- 方向
		VECTOR m_rotation ;				// --- 回転値入れ
		u_char m_playerkeydata ;		// --- キー情報
		BOOL m_keytriger ;				// --- キーが押されているかのトリガー

		// 回転移動で必要な変数 //
		double m_Square[3] ;			// --- ２乗
		double m_radius ;				// --- 半径
		double m_angle[2] ;				// --- 角度計算

		// アニメーション //
		int m_rootflm ;					// --- フレーム入れ
		int m_attachno[ep_end] ;		// --- アタッチナンバー入れ
		float m_totalanimtime[ep_end] ;	// --- アニメーションのトータルタイム
		float m_playtime ;				// --- アニメ経過時間
		int m_animno ;					// --- アニメーション番号入れ

		// ヒットチェック
		int m_hitcnt ;					// --- ラグカウント
		bool m_attackflg ;				// --- 一回当たったら時間たつまでは当たらない

		// ステージ移動時のカウント
		int m_stage2cnt ;

} ;





